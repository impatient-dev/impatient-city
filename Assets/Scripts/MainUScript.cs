﻿using System;
using imp.city;
using imp.city.r;
using imp.city.u;
using imp.city.u2;
using imp.city.wl;
using imp.city.world;
using imp.city.world.rail;
using imp.city.world.rail.cars;
using imp.city.wu;
using UnityEngine;

/**Main (and currently only) Unity script.*/
public class MainUScript : MonoBehaviour, AppSceneContainer {
	private AppScene? Scene;
	
	public void Start() {
		Debug.Log("C# script starting.");
		Time.timeScale = 1;
		Application.targetFrameRate = 24;
		
		AppDisplays.Init();

		World w = new();
		WorldListeners wl = new();

		//rail segment 0: a curve ending at origin heading +X
		var seg0 = new BasicRailSeg(new CircularPath2(new Vector2(0, 20), 20, Mathf.PI / 2, Mathf.PI * 3 / 2).Flat(1f));
		WURail.CreateRailSeg(w, seg0);
		//rail segment 1: straight line from origin towards +X
		var seg1 = new BasicRailSeg(new StraightPath2(new Vector2(0, 0), new Vector2(100, 0)).Flat(1f));
		WURail.CreateRailSeg(w, seg1);
		WURail.ConnectRailSegEnds(w, seg0.Path, Dir1E.End(), seg1.Path, Dir1E.Start());
		
		var r = new AppRes();
		Main.R = r;
		var boxTrain = WUTrain.Spawn(w, wl, StandardTraincarDesigns.Switcher,
			new TrackPos(w, seg0.Path, seg0.Path.Geom.Length - 5).Facing(Dir1E.End()));
		WUTrain.Spawn(w, wl, StandardTraincarDesigns.Flatcar,
			new TrackPos(w, seg1.Path, 1).Facing(Dir1E.Start()));
		WUTrain.SpawnAndAttach(w, StandardTraincarDesigns.Boxcar, Dir1S.Front(), boxTrain, Dir1E.End());
		WUTrain.SpawnAndAttach(w, StandardTraincarDesigns.Boxcar, Dir1S.Front(), boxTrain, Dir1E.End());

		Scene = new WorldScene(this, r, w, (scene) => new WorldView2D(scene, Camera.main!));
	}

	public void Update() {
		try {
			Scene!.Update(Time.deltaTime);
		} catch (Exception e) {
			OnError(e, "frame update");
		}
	}

	public void OnGUI() {
		try {
			Scene!.OnUiEvent(Event.current);
		} catch (Exception e) {
			OnError(e, "GUI event processing");
		}
	}

	public void OnApplicationFocus(bool hasFocus) {
		if(Scene == null) // apparently this can be called before Start()
			return;
		try {
			Scene.OnApplicationFocus(hasFocus);
		} catch(Exception e) {
			OnError(e, "focus change");
		}
	}

	private void OnError(Exception e, string during) {
		Debug.LogError($"Fatal error during {during}: {e}");
		Debug.Break();
		Application.Quit();
	}

	public void DestroyAndReplace(AppScene scene) {
		Scene!.Destroy();
		Scene = scene;
	}
}
