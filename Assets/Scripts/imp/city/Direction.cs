﻿namespace imp.city {
	/** Start / end.
	 * Represents a direction or orientation in 1-dimensional space. The E stands for end.*/
	public readonly struct Dir1E {
		public readonly bool IsEnd;
		public bool IsStart => !IsEnd;

		private Dir1E(bool isEnd) {
			IsEnd = isEnd;
		}

		public override string ToString() => IsStart ? "start" : "end";
		
		public static Dir1E operator !(Dir1E it) => new(!it.IsEnd);

		public override bool Equals(object obj) => obj is Dir1E that && this.IsEnd == that.IsEnd;
		public static bool operator ==(Dir1E a, Dir1E b) => a.IsEnd == b.IsEnd;
		public static bool operator !=(Dir1E a, Dir1E b) => a.IsEnd != b.IsEnd;
		public override int GetHashCode() => IsEnd.GetHashCode();

		/**Returns Start or End, depending on the argument.*/
		public static Dir1E End(bool end = true) => new Dir1E(end);
		/**Returns Start or End, depending on the argument.*/
		public static Dir1E Start(bool start = true) => new Dir1E(!start);
	}




	/** Front / back.
	 * Represents a direction in 1-dimensional space. The S stands for side.*/
	public readonly struct Dir1S {
		public readonly bool IsFront;
		public bool IsBack => !IsFront;

		private Dir1S(bool isFront) {
			IsFront = isFront;
		}

		public override string ToString() => IsFront ? "front" : "back";
		
		public static Dir1S operator !(Dir1S it) => new(!it.IsFront);

		public override bool Equals(object obj) => obj is Dir1S that && this.IsFront == that.IsFront;
		public static bool operator ==(Dir1S a, Dir1S b) => a.IsFront == b.IsFront;
		public static bool operator !=(Dir1S a, Dir1S b) => a.IsFront != b.IsFront;
		public override int GetHashCode() => IsFront.GetHashCode();

		/**Returns Front or Back, depending on the argument.*/
		public static Dir1S Front(bool front = true) => new Dir1S(front);
		/**Returns Back or Front, depending on the argument.*/
		public static Dir1S Back(bool back = true) => new Dir1S(!back);
	}




	/**Forward, neutral, or reverse. The enum values can be converted to the ints 1, 0, and -1.*/
	public enum FNR {
		Forward = 1,
		Neutral = 0,
		Reverse = -1,
	}
}