namespace imp.city.destroy {
	public interface Destroyable {
		/**Deletes any Unity GameObject(s), deregisters any listeners, and cleans up any other resources owned by this object.*/
		public void Destroy();
	}
}