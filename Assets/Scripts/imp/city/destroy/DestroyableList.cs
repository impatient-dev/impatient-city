using System.Collections.Generic;

namespace imp.city.destroy {
	
	/**A parent class that maintains a list of things (children, listeners, GameObjects, etc.) that need to be destroyed when this thing is.
	 * This is easier and more reliable (but less efficient) than just manually writing out all necessary steps in the Destroy() method.*/
	public abstract class DestroyableList : Destroyable {
		private readonly List<Destroyable> DestroyList = new();
		
		public void Destroy() {
			foreach(var d in DestroyList)
				d.Destroy();
			_cleanup();
		}

		/**Called when this object is destroyed.
		 * Empty by default, but you can override this if you want to clean certain things up in a way that isn't ideally handled by adding things to the destroy list.*/
		protected virtual void _cleanup() {}

		/**Adds something to the list of things that must be destroyed when this object is, then returns it.*/
		protected D OnDestroy<D>(D d) where D : Destroyable {
			DestroyList.Add(d);
			return d;
		}

		/**Registers a listener, and deregisters it when this object is destroyed.*/
		protected void Register<L>(HashSet<L> listeners, L listener) => OnDestroy(new SetListenerRegistration<L>(listeners, listener));
		/**Registers a listener, and deregisters it when this object is destroyed.*/
		protected void Register<K, L>(Dictionary<K, List<L>> listeners, K key, L listener) => OnDestroy(new MapListListenerRegistration<K,L>(listeners, key, listener));
	}
}