using System.Collections.Generic;
using imp.util;
using static imp.city.destroy.ListenerUtils;

namespace imp.city.destroy {

	public static class ListenerUtils {
		public static void Register<L>(List<L> listeners, L listener) => listeners.Add(listener);
		public static void Deregister<L>(List<L> listeners, L listener) => listeners.Remove(listener);
		
		public static void Register<L>(HashSet<L> listeners, L listener) => listeners.Add(listener);
		public static void Deregister<L>(HashSet<L> listeners, L listener) => listeners.Remove(listener);

		public static void Register<K, L>(Dictionary<K, List<L>> map, K key, L listener) => map.GetOrCreate(key, () => new()).Add(listener);
		public static void Deregister<K, L>(Dictionary<K, List<L>> map, K key, L listener) => map[key].Remove(listener);
	}


	/**A listener registration object that registers the listener in the constructor and deregisters it in the Destroy method.*/
	public class ListListenerRegistration<L> : Destroyable {
		private readonly List<L> Listeners;
		private readonly L Listener;

		public ListListenerRegistration(List<L> listeners, L listener) {
			Listeners = listeners;
			Listener = listener;
			Register(listeners, listener);
		}

		public void Destroy() => Deregister(Listeners, Listener);
	}


	/**A listener registration object that registers the listener in the constructor and deregisters it in the Destroy method.*/
	public class SetListenerRegistration<L> : Destroyable {
		private readonly HashSet<L> Listeners;
		private readonly L Listener;

		public SetListenerRegistration(HashSet<L> listeners, L listener) {
			Listeners = listeners;
			Listener = listener;
			Register(listeners, listener);
		}

		public void Destroy() => Deregister(Listeners, Listener);
	}


	/**A listener registration object that registers the listener in the constructor and deregisters it in the Destroy method.*/
	public class MapListListenerRegistration<K, L> : Destroyable {
		private readonly Dictionary<K, List<L>> Map;
		private readonly K Key;
		private readonly L Listener;

		public MapListListenerRegistration(Dictionary<K, List<L>> map, K key, L listener) {
			Map = map;
			Key = key;
			Listener = listener;
			Register(map, key, listener);
		}

		public void Destroy() => Deregister(Map, Key, Listener);
	}
}