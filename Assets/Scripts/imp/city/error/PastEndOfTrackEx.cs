using System;
using imp.city.world.rail;

namespace imp.city.error {
	public class PastEndOfTrackEx : Exception {
		public readonly RailPath Path;
		public readonly Dir1E End;
		public readonly float DistancePastEnd;

		public PastEndOfTrackEx(RailPath path, Dir1E end, float distancePastEnd) : base(
			$"Went {distancePastEnd} past the end of the track."
		) {
			Path = path;
			End = end;
			DistancePastEnd = distancePastEnd;
		}
	}
}