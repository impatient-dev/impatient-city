using System;
using imp.city.world;
using imp.city.world.rail;

namespace imp.city.error {
	public class RailSegUnmodifiableConnEx : Exception {
		public readonly World Chunk;
		public readonly RailSeg RailSeg;
		public readonly int Path;
		public readonly Dir1E End;

		public RailSegUnmodifiableConnEx(World chunk, RailSeg railSeg, int path, Dir1E end) : base(
			$"Connection cannot be modified for rail segment {railSeg.Id}, path {path}, end {end}"
		) {
			Chunk = chunk;
			RailSeg = railSeg;
			Path = path;
			End = end;
		}
	}
}