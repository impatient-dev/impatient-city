using System.Collections.Generic;
using imp.util;
using UnityEngine;

namespace imp.city.input {
	/**Keeps track of what InputInterests are interested in different things.*/
	public class ActiveInputInterests {
		private readonly Dictionary<KeyCode, HashSet<InputHandler>> KeyHandlers = new();
		private readonly HashSet<InputHandler> MouseHandlers = new();
		private readonly HashSet<InputHandler> ScrollHandlers = new();
		private readonly HashSet<InputHandler> UpdateHandlers = new();

		/**Removes the handler from everything it's currently interested in.*/
		public void OnDisable(InputInterests i) {
			foreach(var key in i.Keys)
				KeyHandlers.Require(key).Remove(i.Handler);
			if(i.Mouse)
				MouseHandlers.Remove(i.Handler);
			if(i.Update)
				UpdateHandlers.Remove(i.Handler);
		}

		/**Adds the handler to everything it's interested in.*/
		public void OnEnable(InputInterests i) {
			foreach(var key in i.Keys)
				KeyHandlers.GetOrCreate(key, () => new HashSet<InputHandler>()).Add(i.Handler);
			if(i.Mouse)
				MouseHandlers.Add(i.Handler);
			if(i.Update)
				UpdateHandlers.Add(i.Handler);
		}

		public void AddKey(KeyCode key, InputHandler handler) =>
			KeyHandlers.GetOrCreate(key, () => new HashSet<InputHandler>()).Add(handler);
		public void RemoveKey(KeyCode key, InputHandler handler) =>
			KeyHandlers.Require(key).Remove(handler);

		public void AddMouse(InputHandler handler) => MouseHandlers.Add(handler);
		public void RemoveMouse(InputHandler handler) => MouseHandlers.Remove(handler);

		public void AddScroll(InputHandler handler) => ScrollHandlers.Add(handler);
		public void RemoveScroll(InputHandler handler) => ScrollHandlers.Remove(handler);

		public void AddUpdate(InputHandler handler) => UpdateHandlers.Add(handler);
		public void RemoveUpdate(InputHandler handler) => UpdateHandlers.Remove(handler);



		public void CallKeyDown(KeyCode key) {
			var handlers = KeyHandlers.Opt(key);
			if(handlers == null)
				return;
			foreach(var h in handlers)
				h.OnKeyDown(key);
		}
		public void CallKeyUp(KeyCode key) {
			var handlers = KeyHandlers.Opt(key);
			if(handlers == null)
				return;
			foreach(var h in handlers)
				h.OnKeyUp(key);
		}

		public void CallMouseDown(int button) {
			foreach(var h in MouseHandlers)
				h.OnMouseDown(button);
		}
		public void CallMouseUp(int button) {
			foreach(var h in MouseHandlers)
				h.OnMouseUp(button);
		}

		public void CallScroll(Vector2 delta) {
			foreach(var h in ScrollHandlers)
				h.OnScroll(delta);
		}
		
		public void CallUpdate(float deltaTime) {
			foreach(var h in UpdateHandlers)
				h.OnUpdate(deltaTime);
		}
	}
}