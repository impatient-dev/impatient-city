using UnityEngine;

namespace imp.city.input {
	/**A subclass of InputManager that tries to work around scrolling issues in the editor.
	 * This shouldn't be used if we're running in the player.*/
	public class EditorInputManager : InputManager {
		/*
		 The first time (in a while) you move the mouse scroll wheel, it will scroll 0.5 units (UP is positive).
		 On subsequent uses of the wheel, it will scroll 3 units (DOWN is positive), then 0.5 units (UP is positive) on the same frame.
		 Note: all scrolling seems to be inverted in the editor compared with the player.
		 
		 The event attributes aren't helpful. isScrollWheel=true, pointerType=Mouse, rawType=scrollWheel, even on the trackpad.
		 
		 In the player, the scrolls are of magnitude 0.5 (even the trackpad; the editor offers finer control?!)
		 But scrolls are mostly in the opposite direction compared to the editor.
		 
		 I've tried a couple solutions, but none of them work consistently with fast and slow scrolling on a mouse and trackpad.
		 For now, in the editor, the first click of the scroll wheel is going to be in the wrong direction.
		 */

		public EditorInputManager() {}

		protected override void OnScroll(Event ev) {
			var delta = ev.delta;
			delta.y = -delta.y; // all scrolling is reversed in the editor
			OnScrollDelta(delta);
		}
	}
}