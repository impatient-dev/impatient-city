using System.Collections.Generic;
using UnityEngine;

namespace imp.city.input {
	/**Specifies what inputs an InputHandler is interested in (i.e. what methods InputManager will call).
	 * Initially, Enabled is true but all other interests are off/false.*/
	public sealed class InputInterests {
		private readonly ActiveInputInterests Mgr;
		public readonly InputHandler Handler;

		public InputInterests(ActiveInputInterests mgr, InputHandler handler) {
			Mgr = mgr;
			Handler = handler;
		}

		public void Destroy() {
			Enabled = false;
		}

		private bool _enabled = true;
		/**Initially true; the handler will not receive any method calls while this is false.
		 * You can still add and remove interests at any time; the new interests will not be triggered until enabled becomes true.*/
		public bool Enabled {
			get => _enabled;
			set {
				if (_enabled == value)
					return;
				_enabled = value;
				if(value)
					Mgr.OnEnable(this);
				else
					Mgr.OnDisable(this);
			}
		}

		private readonly HashSet<KeyCode> _keys = new();
		public IReadOnlyCollection<KeyCode> Keys => _keys;
		
		public void AddKey(KeyCode key) {
			if(_keys.Add(key) && Enabled)
				Mgr.AddKey(key, Handler);
		}
		public void AddKeys(params KeyCode[] keys) {
			foreach(var key in keys)
				AddKey(key);
		}

		public void RemoveKey(KeyCode key) {
			if(_keys.Remove(key) && _enabled)
				Mgr.RemoveKey(key, Handler);
		}
		public void RemoveKeys(params KeyCode[] keys) {
			foreach(var key in keys)
				RemoveKey(key);
		}

		private bool _mouse;
		/**Mouse up/down.*/
		public bool Mouse {
			get => _mouse;
			set {
				if(_mouse == value)
					return;
				_mouse = value;
				if(value)
					Mgr.AddMouse(Handler);
				else
					Mgr.RemoveMouse(Handler);
			}
		}

		private bool _scroll;
		public bool Scroll {
			get => _scroll;
			set {
				if(_scroll == value)
					return;
				_scroll = value;
				if(value)
					Mgr.AddScroll(Handler);
				else
					Mgr.RemoveScroll(Handler);
			}
		}

		private bool _update;
		public bool Update {
			get => _update;
			set {
				if(_update == value)
					return;
				_update = value;
				if(value)
					Mgr.AddUpdate(Handler);
				else
					Mgr.RemoveUpdate(Handler);
			}
		}
	}
}