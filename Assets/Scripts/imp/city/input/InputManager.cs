﻿using UnityEngine;

namespace imp.city.input {

	/**All methods do nothing by default, so you only have to override the ones you want to.*/
	public abstract class InputHandler {
		public virtual void OnKeyDown(KeyCode key) {}
		public virtual void OnKeyUp(KeyCode key) {}
		public virtual void OnMouseDown(int button) {}
		public virtual void OnMouseUp(int button) {}
		
		/**Handles horizontal and/or vertical scrolling.
		 * A normal value for vertical scrolling is 0.5 with a scroll wheel; values smaller than 0.1 happen if scrolling with a trackpad.
		 * Positive Y means we scrolled up (in); negative means down (out).*/
		public virtual void OnScroll(Vector2 delta) {}

		/**The Unity Update() function, called once per frame. deltaTime is in seconds.*/
		public virtual void OnUpdate(float deltaTime) {}
	}

	
	/**A registry that routes input events to the handlers that are interested in them.*/
	public class InputManager {
		private readonly ActiveInputInterests Active = new();

		/**Chooses the appropriate manager based on whether we're in the player or editor.*/
		public static InputManager Create() {
			return Application.isEditor ? new EditorInputManager() : new InputManager();
		}

		protected InputManager() {}

		/**Registers an InputHandler to receive inputs. Use the returned InputInterests to decide what inputs the handler should receive.*/
		public InputInterests Register(InputHandler handler) => new(Active, handler);

		public void OnEvent(Event ev) {
			switch(ev.type) {
				case EventType.KeyDown:
					OnKeyDown(ev.keyCode);
					break;
				case EventType.KeyUp:
					OnKeyUp(ev.keyCode);
					break;
				case EventType.MouseDown:
					OnMouseDown(ev.button);
					break;
				case EventType.MouseUp:
					OnMouseUp(ev.button);
					break;
				case EventType.ScrollWheel:
					OnScroll(ev);
					break;
			}
		}

		// TODO inline these methods

		public void OnKeyDown(KeyCode key) {
			if(key != KeyCode.None)
				Active.CallKeyDown(key);
		}

		public void OnKeyUp(KeyCode key) {
			if(key != KeyCode.None)
				Active.CallKeyUp(key);
		}

		public void OnMouseDown(int button) => Active.CallMouseDown(button);
		public void OnMouseUp(int button) => Active.CallMouseUp(button);

		public void OnUpdate(float deltaTime) => Active.CallUpdate(deltaTime);

		protected virtual void OnScroll(Event ev) => OnScrollDelta(ev.delta);
		protected void OnScrollDelta(Vector2 delta) => Active.CallScroll(delta);
	}
}