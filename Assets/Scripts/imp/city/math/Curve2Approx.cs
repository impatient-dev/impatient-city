using UnityEngine;

namespace imp.city.math {
	/**Utility functions for working with 2D curves.*/
	public static class Curve2Approx {
		/**<summary>This function is used when approximating a curved 2D path with straight segments.
		 * Given a (minimum) radius of the circle, this function returns how long each straight segment has to be
		 * to ensure the difference in heading between any 2 adjacent segments is no more than the provided degree of curvature, in radians.</summary>
		 *
		 * <param name="radius">The minimum, tightest radius of the circular curve.</param>
		 * <param name="radians">The angle we want there to be between each pair of adjacent straight segments, in radians.
		 * (This is equivalent to the central angle.)</param>*/
		// degree_of_curvature_rad / 2 pi = arc length / 2 pi r
		public static float LengthForDegreeOfCurvature(float radius, float radians) => radius * radians;


		/**<summary>This function is used when approximating a curved 2D path with straight segments.
		 * It decides how many straight segments you need to approximate a circular curve.</summary>
		 * <param name="radians">The angle you want there to be between each pair of adjacent straight segments, in radians.</param>
		 * <param name="portion">How much of a circle your curve comprises; 1 (the default) is a whole circle, 0.5 is half of a circle, etc.</param>
		 * <returns>The number of straight segments you need to approximate the (portion of) the circle. This is never less than 1.</returns>*/
		public static int NumSegmentsToApproximateCircularSection(float radians, float portion = 1) {
			var segments =  radians * portion / (2 * Mathf.PI);
			if(segments < 1.5f)
				return 1;
			return Mathf.RoundToInt(segments);
		}


		/**<summary>Returns the number of straight segments you need to approximate a 2D curve.</summary>
		 *
		 * <param name="radius">The minimum (tightest) radius of the curve.</param>
		 * <param name="length">The length of the curve.</param>
		 * <param name="radians">The maximum difference in heading you want there to be between any 2 adjacent segments.
		 * A lower number increases the number of segments that are needed.</param>*/
		public static int NumSegments(float radius, float length, float radians) {
			var segmentLength = LengthForDegreeOfCurvature(radius: radius, radians: radians);
			var segments = length / segmentLength;
			if(segments < 1.5f)
				return 1;
			return Mathf.RoundToInt(segments);
		}
	}
}