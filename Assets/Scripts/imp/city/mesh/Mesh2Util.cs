using UnityEngine;

namespace imp.city.mesh {
	public static class Mesh2Util {
		/**Creates a rectangle centered at the origin.*/
		public static Mesh Rectangle(Vector2 dimensions) {
			var half = dimensions / 2;
			Mesh mesh = new() {
				vertices = new[] { // clockwise
					new Vector3(half.x, half.y),
					new Vector3(half.x, -half.y),
					new Vector3(-half.x, -half.y),
					new Vector3(-half.x, half.y),
				},
				triangles = new[] {
					0, 1, 2,
					2, 3, 0,
				}
			};
			mesh.RecalculateNormals();
			mesh.RecalculateTangents();
			return mesh;
		}


		/**Generates a triangle in the X-Y plane. The bottom of the triangle will be on the X-axis; the point of the triangle will be above on the Y-axis.*/
		public static Mesh TriangleY(float width, float height) {
			var hw = width / 2;
			return new Mesh() {
				vertices = new[] { // clockwise
					new Vector3(-hw, 0),
					new Vector3(0, height),
					new Vector3(hw, 0),
				},
				triangles = new[] {0, 1, 2},
			};
		}
	}
}