using imp.city.math;
using imp.city.world.rail;
using imp.util;
using UnityEngine;

namespace imp.city.mesh {
	public static class Path2Mesh {
		/**<summary>Creates a mesh that approximates a 2D path.
		 * The mesh may have more or fewer vertices, depending on how curved the path is and the radians argument.</summary>
		 *
		 * <param name="radians">If the curve is not straight, it will be approximated by dividing it into straight segments.
		 * We will create as many segments as necessary to ensure the difference in bearing between any 2 adjacent segments is no more than this value.
		 * Lower values create a more accurate approximation with more segments.</param>*/
		public static Mesh Create(Path2 path, float width, float radians) {
			int segments = Curve2Approx.NumSegments(radians: radians, radius: path.MinRadius, length: path.Length);
			var positions = new Vector3[2 * (segments + 1)];

			for(var n = 0; n <= segments; n++) {
				var d = n == segments ? path.Length :
					n / (float)(segments + 1) * path.Length;
				var center = path.PosAt(d);
				var right = path.DirAt(d).RotR();
				positions[2 * n] = center + (width / 2) * right;
				positions[2 * n + 1] = center - (width / 2) * right;
			}
			
			var quadIndices = new int[4 * segments];
			for(var q = 0; q < segments; q++) {
				quadIndices[4 * q] = 2 * q; // bottom-right
				quadIndices[4 * q + 1] = 2 * q + 1; // bottom-left
				quadIndices[4 * q + 2] = 2 * (q + 1) + 1; // top-left
				quadIndices[4 * q + 3] = 2 * (q + 1); // top-right
			}

			var mesh = new Mesh();
			mesh.SetVertices(positions);
			mesh.SetIndices(quadIndices, MeshTopology.Quads, 0);
			return mesh;
		}



		/**<summary>Returns a set of points that approximate a 2D path.
		 * There may be more or fewer points, depending on how curved the path is and the radians argument.</summary>
		 *
		 * <param name="radians">If the curve is not straight, it will be approximated by dividing it into straight segments.
		 * We will create as many segments as necessary to ensure the difference in bearing between any 2 adjacent segments is no more than this value.
		 * Lower values create a more accurate approximation with more segments.</param>*/
		public static Vector2[] Line(Path2 path, float radians) {
			int segments = Curve2Approx.NumSegments(radians: radians, radius: path.MinRadius, length: path.Length);
			var positions = new Vector2[segments + 1];
			for(var n = 0; n <= segments; n++) {
				var d = n == segments ? path.Length :
					n / (float)(segments + 1) * path.Length;
				positions[n] = path.PosAt(d);
			}
			return positions;
		}
	}
}