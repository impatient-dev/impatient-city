using System;
using TMPro;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.city.r {

	/**Different variants (main, bold, italic) of the same font. The main (regular) variant is required; the other variants are optional.*/
	public readonly struct AppFont {
		private readonly TMP_FontAsset?[] _array;

		public AppFont(string main, string? bold = null, string? italic = null) {
			_array = new[] {
				LoadResource<Font>(main).Pro(),
				LoadResourceIf<Font>(bold)?.Pro(),
				LoadResourceIf<Font>(italic)?.Pro(),
			};
		}
		
		public TMP_FontAsset? this[Variant variant] => _array[(int)variant];
		public TMP_FontAsset OrMain(Variant variant) => this[variant] ?? Main;
		
		public TMP_FontAsset Main => this[Variant.Main]!;
		public TMP_FontAsset? Bold => this[Variant.Bold];
		public TMP_FontAsset? Italic => this[Variant.Italic];

		public enum Variant {
			Main = 0,
			Bold = 1,
			Italic = 2,
		}
	}


	public static class AppFontExtensions {
		public static FontStyles ToTmpStyle(this AppFont.Variant variant) {
			switch(variant) {
				case AppFont.Variant.Main:
					return FontStyles.Normal;
				case AppFont.Variant.Bold:
					return FontStyles.Bold;
				case AppFont.Variant.Italic:
					return FontStyles.Italic;
			}
			throw new Exception($"Invalid font variant: {variant}");
		}
	}
}