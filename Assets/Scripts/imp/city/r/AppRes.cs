﻿using imp.city.u;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.city.r {

	/**Various resources required by the app.*/
	public sealed class AppRes {
		public readonly AppColors Color = new();
		public readonly AppFonts Font = new();
		public readonly AppMaterials Mat = new();
		public readonly AppMatProps MP;
		public readonly AppSizes Size = new();

		public readonly AppStyles Style;

		public AppRes() {
			MP = new(Color, Mat);
			Style = new(this);
		}
	}

	public sealed class AppColors {
		public readonly Color Hover = new(0, 0.9f, 0.8f);
		public readonly Color Selected = new(0, 0.6f, 1f);
		public readonly Color TrainCar = Color.yellow;
		
		public readonly ColorPair Ui = Gray(0.8f).Over(Gray(0.3f));
		/**Partially transparent background.*/
		public readonly Color UiPartialBack;
		public readonly ColorPair Btn = Gray(0.9f).Over(Gray(0.2f));
		public readonly ColorPair BtnDisabled = Gray(0.85f).Over(Gray(0.4f));

		public AppColors() {
			UiPartialBack = Ui.Back.Alpha(0.7f);
		}
	}

	public sealed class AppFonts {
		public readonly AppFont Main = new("Fonts/OpenSans-Regular", bold: "Fonts/OpenSans-Bold");
		public readonly AppFont Mono = new("Fonts/Recursive_Mono");
	}

	public sealed class AppMaterials {
		public readonly ColorMat Color = new("Materials/Color");
		public readonly Material Terrain = LoadResource<Material>("Materials/Terrain");
		public readonly Material Rail = LoadResource<Material>("Materials/Rail");
		public readonly Material RailCar = LoadResource<Material>("Materials/RailCar");
		public readonly Material Selected = LoadResource<Material>("Materials/Selected");
		public readonly Material Selecting = LoadResource<Material>("Materials/Selecting");
	}

	public sealed class AppMatProps {
		public readonly MatAndColor Hover, Selected;
		public readonly MatAndColor TrainCar, TrainCarText;
		
		public AppMatProps(AppColors colors, AppMaterials materials) {
			Hover = materials.Color.With(colors.Hover);
			Selected = materials.Color.With(colors.Selected);
			TrainCar = materials.Color.With(colors.TrainCar);
			TrainCarText = materials.Color.With(Color.black);
		}
	}


	/**Sizes used in the UI.*/
	public class AppSizes {
		public readonly int PanelPad = 5;
		public readonly RectOffset PanelMargin;
		
		public AppSizes() {
			PanelMargin = SquareOffset(PanelPad);
		}
	}
}