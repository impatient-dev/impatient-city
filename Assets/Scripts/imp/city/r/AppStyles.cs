using imp.city.ui;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.city.r {
	public class AppStyles {
		public readonly TxtStyle Body, BodyBold;
		public readonly TxtStyle Title;
		/**Describes the adjacent body text, e.g. "size:" before a size.*/
		public readonly TxtStyle Label;

		public readonly BtnStyle Btn;
		public readonly LinkStyle Link;

		public readonly CLinearLayoutCustomizer PanelCPad;
		/**A top-center aligned column with a background and padding.*/


		public AppStyles(AppRes r, float sizeMult = 1) {
			Body = new(r, r.Font.Main, size: 20 * sizeMult, color: r.Color.Ui.Fore);
			Label = Body.WithSize(16 * sizeMult);
			Title = Body.WithSize(28 * sizeMult);

			BodyBold = Body.WithVariant(AppFont.Variant.Bold);

			Btn = new(
				Body.WithVariant(AppFont.Variant.Bold),
				r.Color.Btn,
				Body.WithVariant(AppFont.Variant.Italic),
				r.Color.BtnDisabled,
				margin: SquareOffset(4 * sizeMult),
				padding: SquareOffset(4 * sizeMult)
			);

			Link = LinkStyle.UnderlinedOrItalic(r, Body.Font, Body.Size, new Color(0, 0.7f, 1), Gray(0.7f));

			PanelCPad = new CLinearLayoutAlignmentPaddedCustomizer(TextAnchor.UpperCenter, r.Size.PanelMargin);
		}
	}
}