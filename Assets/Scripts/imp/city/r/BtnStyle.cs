using UnityEngine;

namespace imp.city.r {
	/**A style for buttons.*/
	public readonly struct BtnStyle {
		public readonly TxtStyle Text, TextDisabled;
		public readonly Color Back, BackDisabled;
		public readonly RectOffset Margin, Padding;

		/**The text-style colors are ignored in favor of the color pairs.*/
		public BtnStyle(TxtStyle text, ColorPair color, TxtStyle textDisabled, ColorPair disabled, RectOffset margin, RectOffset padding) {
			Text = text.WithColor(color.Fore);
			TextDisabled = textDisabled.WithColor(disabled.Fore);
			Back = color.Back;
			BackDisabled = disabled.Back;
			Margin = margin;
			Padding = padding;
		}
	}
}