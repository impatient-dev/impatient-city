using imp.city.u;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.city.r {
	/**A Material where the only property is _Color.*/
	public readonly struct ColorMat {
		public readonly Material Mat;
		/**The color property ID for the shader.*/
		public readonly int Prop;

		public ColorMat(string path) {
			Mat = LoadResource<Material>(path);
			Prop = Shader.PropertyToID("_Color");
		}

		/**Creates a MaterialPropertyBlock setting the color of this material.*/
		public MaterialPropertyBlock Block(Color color) {
			var ret = new MaterialPropertyBlock();
			ret.SetColor(Prop, color);
			return ret;
		}

		public MatAndColor With(Color color) => new(this, color);
	}
}