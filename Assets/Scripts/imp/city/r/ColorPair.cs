using UnityEngine;

namespace imp.city.r {
	
	public readonly struct ColorPair {
		public readonly Color Fore, Back;

		public ColorPair(Color fore, Color back) {
			Fore = fore;
			Back = back;
		}
	}


	public static class AppThemeExtensions {
		public static ColorPair Under(this Color back, Color fore) => new(back: back, fore: fore);
		public static ColorPair Over(this Color fore, Color back) => new(back: back, fore: fore);
	}
}