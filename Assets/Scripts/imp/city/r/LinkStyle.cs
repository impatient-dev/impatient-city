using System;
using TMPro;
using UnityEngine;

namespace imp.city.r {
	public class LinkStyle {
		public readonly AppRes R;
		public readonly AppFont Font;
		public readonly AppFont.Variant Variant;
		public readonly float Size;
		public readonly Color Color, ColorDisabled;
		public readonly TMP_FontAsset TmpFont, TmpFontDisabled;
		public readonly FontStyles TmpStyle, TmpStyleDisabled;

		/**For both enabled and disabled, you can either underline or choose a variant, but not both.*/
		public LinkStyle(
			AppRes r,
			AppFont font,
			float size,
			Color color,
			Color disabled,
			bool underline = false,
			bool disabledUnderline = false,
			AppFont.Variant variant = AppFont.Variant.Main,
			AppFont.Variant disabledVariant = AppFont.Variant.Main
		) {
			R = r;
			Font = font;
			Variant = variant;
			Size = size;
			Color = color;
			ColorDisabled = disabled;
			
			if(underline) {
				if(variant != AppFont.Variant.Main)
					throw new ArgumentException("Cannot specify both underline and a variant.");
				TmpStyle = FontStyles.Underline;
				TmpFont = font.Main;
			} else {
				var f = font[variant];
				if(f != null) {
					TmpFont = f;
					TmpStyle = FontStyles.Normal;
				} else {
					TmpFont = font.Main;
					TmpStyle = variant.ToTmpStyle();
				}
			}

			if(disabledUnderline) {
				if(disabledVariant != AppFont.Variant.Main)
					throw new ArgumentException("Cannot specify both underline and a variant for disabled.");
				TmpStyleDisabled = FontStyles.Underline;
				TmpFontDisabled = font.Main;
			} else {
				var f = font[disabledVariant];
				if(f != null) {
					TmpFontDisabled = f;
					TmpStyleDisabled = FontStyles.Normal;
				} else {
					TmpFontDisabled = font.Main;
					TmpStyleDisabled = disabledVariant.ToTmpStyle();
				}
			}
		}

		/**Creates a link style that is underlined when enabled and italic when disabled.*/
		public static LinkStyle UnderlinedOrItalic(AppRes r, AppFont font, float size, Color color, Color disabled) =>
			new(r, font, size, color, disabled, underline: true, disabledVariant: AppFont.Variant.Italic);
	}
	
	
}