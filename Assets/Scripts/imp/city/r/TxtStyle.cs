using TMPro;
using UnityEngine;

namespace imp.city.r {
	
	/**All data necessary to define the appearance (but not content) of some TextMeshPro text.*/
	public class TxtStyle {
		public readonly AppRes R;
		public readonly AppFont Font;
		public readonly TMP_FontAsset TmpFont;
		public readonly float Size;
		public readonly Color Color;
		public readonly AppFont.Variant Variant;
		public readonly FontStyles TmpStyle;

		public TxtStyle(AppRes r, AppFont font, float size, Color color, AppFont.Variant variant = AppFont.Variant.Main) {
			R = r;
			Size = size;
			Color = color;
			Font = font;
			Variant = variant;

			// if we have a font for this variant, use it; otherwise, apply a style to make TextMeshPro generate a bold (etc.) version later.
			var f = font[variant];
			if(f != null) {
				TmpFont = f;
				TmpStyle = FontStyles.Normal;
			} else {
				TmpFont = font.Main;
				TmpStyle = variant.ToTmpStyle();
			}
		}

		public TxtStyle WithSize(float size) => new(r: R, font: Font, size: size, color: Color, variant: Variant);
		public TxtStyle WithColor(Color color) => new(r: R, font: Font, size: Size, color: color, variant: Variant);
		public TxtStyle WithFont(AppFont font, AppFont.Variant variant = AppFont.Variant.Main) =>
			new(R, font, size: Size, color: Color, variant: variant);
		public TxtStyle WithVariant(AppFont.Variant variant) => new(R, Font, size: Size, color: Color, variant: variant);
	}
}