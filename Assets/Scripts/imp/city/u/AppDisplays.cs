using System;
using UnityEngine;

namespace imp.city.u {

	/**If multiple displays are used, we use 1 layer per display to render things correctly.*/
	public static class AppDisplays {
		/**The layer that is used to display things on all displays.*/
		public static int MainLayer { get; private set; }
		private static int[] _displayLayers = null!;
		/**The number of displays that are supported.*/
		public const int COUNT = 8;
		/**The indices used for displays*/
		public const int MIN = 0, MAX = COUNT - 1;

		public static void Init() {
			MainLayer = _layer("Default");
			var disps = new int[COUNT];
			for(var d = 0; d < disps.Length; d++)
				disps[d] = _layer($"Display {d + 1}");
			_displayLayers = disps;
		}

		/**The argument must be in [MIN, MAX]. Returns the bit for the layer that is rendered to this display.*/
		public static int Layer(int display) => _displayLayers[display];

		private static int _layer(string name) {
			var layer = LayerMask.NameToLayer(name);
			if(layer == -1)
				throw new Exception($"There is no Unity layer named {name}");
			return layer;
		}
	}
}