using imp.city.destroy;
using UnityEngine;

namespace imp.city.u {
	/**An object that receives calls from the main MonoBehavior.
	 * A scene can be destroyed and replaced with a different one, e.g. when switching between a world and the main menu.*/
	public interface AppScene : Destroyable {
		/**Called once per frame. The argument is the number of seconds between updates.*/
		public void Update(float deltaTime);
		/**Called for each key press, mouse event, etc.*/
		public void OnUiEvent(Event ev);
		/**Called when this application gains or loses focus.*/
		public void OnApplicationFocus(bool hasFocus);
	}



	public interface AppSceneContainer {
		/**Destroys the current scene and starts rendering a new one.*/
		void DestroyAndReplace(AppScene scene);
	}
}