using System;
using imp.city.destroy;

namespace imp.city.u {

	/**A handler that is notified when the player takes and releases control of objects of a particular type.
	 * This abstract class handles casting and allows you to implement methods that don't take generic objects.
	 * You should probably call Init at the end of your subclass constructor.*/
	public abstract class CastingControlHandler<T> : GenericTypedControlHandler {
		protected readonly WorldScene Scene;

		public CastingControlHandler(WorldScene scene) {
			Scene = scene;
		}

		protected void Register() => Scene.Control.AddTypeHandler(this);

		/**Registers this handler with the ControlManager.
		 * Also checks whether something of the desired type is under control; if it is, calls _onControl().
		 * You should call this at the end of the subclass constructor.*/
		protected void Init() {
			var c = Scene.Control.Controlled;
			if(c != null && c.GetType() == Type)
				_onGenericControl(c);
			Register();
		}

		public virtual void Destroy() {
			Scene.Control.RemoveTypeHandler(this);
		}
		
		public abstract Type Type { get; }

		protected abstract T _cast(object o);
		public abstract void _onControl(T thing);
		public abstract void _onUncontrol(T thing);

		public virtual void _onGenericControl(object thing) => _onControl(_cast(thing));
		public virtual void _onGenericUncontrol(object thing) => _onUncontrol(_cast(thing));
	}



	/**A handler that is notified when the player takes and releases control of objects.
	 * This handler is only interested in objects of a particular type (which there is a getter for),
	 * and will only receive method calls for that particular type, but receives generic objects as arguments and must cast them to the desired type.*/
	public interface GenericTypedControlHandler : Destroyable {
		/**The type of object this handler wants to be notified about.*/
		public Type Type { get; }
		public void _onGenericControl(object thing);
		public void _onGenericUncontrol(object thing);
	}
}