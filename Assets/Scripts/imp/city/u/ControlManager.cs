using System;
using System.Collections.Generic;
using imp.util;

namespace imp.city.u {
	/**Keeps track of which world object is currently being controlled.
	 * The controlled object normally gets a GUI and some code that listens for key presses.*/
	public class ControlManager {
		private object? _controlled;
		private readonly Dictionary<Type, List<GenericTypedControlHandler>> _typedhandlers = new();

		public object? Controlled {
			get => _controlled;
			set {
				if(_controlled == value)
					return;

				var prev = _controlled;
				_controlled = value;
				
				// call the handlers last, since they might check what the selection was set to
				if(prev != null) {
					var uncontrolHandlers = _typedhandlers.Opt(prev.GetType());
					if(uncontrolHandlers != null)
						foreach(var h in uncontrolHandlers)
							h._onGenericUncontrol(prev);
				}
				if(value != null) {
					var controlHandlers = _typedhandlers.Opt(value.GetType());
					if(controlHandlers != null)
						foreach(var h in controlHandlers)
							h._onGenericControl(value);
				}
			}
		}

		public void AddTypeHandler(GenericTypedControlHandler handler) =>
			_typedhandlers.GetOrCreate(handler.Type, () => new()).Add(handler);
		public void RemoveTypeHandler(GenericTypedControlHandler handler) =>
			_typedhandlers.GetValueOrDefault(handler.Type)?.Remove(handler);
	}
}
