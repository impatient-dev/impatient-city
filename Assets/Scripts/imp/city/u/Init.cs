namespace imp.city.u {
	/**Interface for things that need to be initialized after the constructor, to avoid problems where other related constructors may not have finished yet.*/
	public interface Init {
		public void Init();
	}
}