using imp.city.r;
using imp.util;
using TMPro;
using UnityEngine;

namespace imp.city.u {

	/**A material plus a color, which can be applied to text. Also contains a MaterialPropertyBlock with the color, so this can be applied to meshes and lines.*/
	public class MatAndColor {
		public readonly Material Mat;
		public readonly Color Color;
		public readonly MaterialPropertyBlock Block;

		public MatAndColor(ColorMat mat, Color color) {
			Mat = mat.Mat;
			Color = color;
			Block = mat.Block(color);
		}

		public void Apply(LineRenderer line) {
			line.material = Mat;
			// startColor and endColor don't work
			line.SetPropertyBlock(Block);
		}

		public void Apply(MeshRenderer mesh) {
			mesh.material = Mat;
			mesh.SetPropertyBlock(Block);
		}
		
		public void Apply(TextMeshPro text) {
			text.material = Mat;
			text.color = Color;
		}


		public LineRenderer ARLine(GameObject obj, float width, Vector3[] positions) {
			var line = obj.ARLine(width, Mat, positions);
			line.SetPropertyBlock(Block);
			return line;
		}
		public LineRenderer ARLine(GameObject obj, float width, int positionCount) {
			var line = obj.ARLine(width, Mat, positionCount);
			line.startColor = line.endColor = Color;
			return line;
		}

		public MeshRenderer ARMesh(GameObject obj, Mesh mesh) {
			obj.AR<MeshFilter>().mesh = mesh;
			var ret = obj.AR<MeshRenderer>();
			Apply(ret);
			return ret;
		}
	}



	/**A material and a corresponding MaterialPropertyBlock that customizes it.*/
	public readonly struct MatAndProp {
		public readonly Material Mat;
		public readonly MaterialPropertyBlock Prop;

		public MatAndProp(Material mat, MaterialPropertyBlock prop) {
			Mat = mat;
			Prop = prop;
		}

		public void Apply(Renderer renderer) {
			renderer.material = Mat;
			renderer.SetPropertyBlock(Prop);
		}

		public LineRenderer ARLine(GameObject obj, float width, Vector3[] positions) {
			var line = obj.ARLine(width, Mat, positions);
			line.SetPropertyBlock(Prop);
			return line;
		}
		public LineRenderer ARLine(GameObject obj, float width, int positionCount) {
			var line = obj.ARLine(width, Mat, positionCount);
			line.SetPropertyBlock(Prop);
			return line;
		}

		public MeshRenderer ARMesh(GameObject obj, Mesh mesh) {
			obj.AR<MeshFilter>().mesh = mesh;
			var ret = obj.AR<MeshRenderer>();
			Apply(ret);
			return ret;
		}
	}
}