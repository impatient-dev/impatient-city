using imp.city.ui;
using imp.util;
using UnityEngine;
using UnityEngine.EventSystems;

namespace imp.city.u {

	/**Updates the selection and hover based on what the mouse is pointing at and what is clicked on.
	 * This object is passive; someone must call its methods.
	 * This class currently uses only 2D physics and will not work with 3D colliders.*/
	// TODO raycasts don't seem to hit canvas objects, so we should probably stop adding them to the ownership map
	public class SelectionDriver {
		private readonly SelectionManager Selection;
		private readonly Camera Camera;
		private readonly Collider2D[] RaycastHits = new Collider2D[20];

		public SelectionDriver(SelectionManager selection, Camera camera) {
			Selection = selection;
			Camera = camera;
		}


		/**Call this once per frame.*/
		public void Tick(float deltaTime) {
			var center = Camera.ScreenToWorldPoint(Input.mousePosition);
			var radius = Camera.orthographicSize / 8;
			var hits = Physics2D.OverlapCircleNonAlloc(center, radius, RaycastHits);
			RaycastResult best = new(null, float.PositiveInfinity);

			for(var h = 0; h < hits; h++) {
				var hit = RaycastHits[h];
				var obj = hit.gameObject;
				var owner = obj.Owner();
				if(owner == Selection.Selection) {
					continue;
				} else if(owner is WorldEntityWrapper we && we.WorldEntity != null && we.WorldEntity != Selection.Selection) {
					var distance2 = (hit.ClosestPoint(center) - center.xy()).sqrMagnitude;
					if(distance2 < 0.01 && hit.OverlapPoint(center)) {
						Selection.SetHover(we.WorldEntity);
						return;
					} else if(distance2 < best.Distance2) {
						best = new(we.WorldEntity, distance2);
					}
				} else if(owner is CObj && hit.OverlapPoint(center)) { // the mouse is directly over the UI
					Selection.ClearHover();
					return;
				}
			}
			
			Selection.SetHover(best.WorldEntity);
		}


		public void OnMouseDown(int button) {
			if(button != 0 || EventSystem.current.IsPointerOverGameObject())
				return;
			Selection.SetSelection(Selection.Hover);
		}



		private readonly struct RaycastResult {
			public readonly object? WorldEntity;
			/**The square of the distance from the mouse cursor.*/
			public readonly float Distance2;

			public RaycastResult(object? worldEntity, float distance2) {
				WorldEntity = worldEntity;
				Distance2 = distance2;
			}
		}
	}
}