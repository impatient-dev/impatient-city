#nullable enable

using System;
using imp.city.destroy;
using static imp.city.destroy.ListenerUtils;

namespace imp.city.u {

	/**A thing that is notified when anything is selected and hovered.
	 * If you're only interested in selection changes for objects of a particular type (e.g. trains), see CastingSelectionHandler.*/
	public interface SelectionListener {
		public void _onHover(object? worldEntity);
		public void _onSelect(object? worldEntity);
	}




	/**A thing that is notified when objects of a particular type are selected and hovered.
	 * This subclass creates Destroyable objects when selection and hover occur, and destroys them when deselection and unhovering occur.
	 * See the parent classes/interfaces for more details.
	 * Remember to call Init at the end of your subclass constructor.*/
	public abstract class DestroyableTypedSelectionHandler<T> : CastingSelectionHandler<T> {
		private Destroyable? _currentHover, _currentSelect;
		
		public DestroyableTypedSelectionHandler(WorldScene scene) : base(scene) {}
		
		protected abstract Destroyable? _showHover(T thing);
		protected abstract Destroyable? _showSelected(T thing);

		public override void _onHover(T thing) {
			_currentHover = _showHover(thing);
		}
		public override void _onUnhover(T thing) {
			_currentHover?.Destroy();
			_currentHover = null;
		}
		public override void _onSelect(T thing) {
			_currentSelect = _showSelected(thing);
		}
		public override void _onDeselect(T thing) {
			_currentSelect?.Destroy();
			_currentSelect = null;
		}
	}



	/**A handler that is notified when objects of a particular type are selected and hovered.
	 * This abstract class handles casting and allows you to implement methods that don't take generic objects.
	 * You should probably call Init at the end of your subclass constructor.*/
	public abstract class CastingSelectionHandler<T> : GenericTypedSelectionHandler {
		protected readonly WorldScene Scene;
		
		public CastingSelectionHandler(WorldScene scene) {
			Scene = scene;
		}

		/**Registers this handler with the SelectionManager.
		 * Also checks whether something of the desired type is under control; if it is, calls _onControl().
		 * You should call this at the end of the subclass constructor.*/
		protected void Init() {
			var s = Scene.Selection.Selection;
			if(s != null && s.GetType() == Type)
				_onGenericSelect(s);
			var h = Scene.Selection.Hover;
			if(h != null && h.GetType() == Type)
				_onGenericHover(h);
			Register(Scene.Selection.TypedHandlers, Type, this);
		}

		public virtual void Destroy() => Deregister(Scene.Selection.TypedHandlers, Type, this);

		public abstract Type Type { get; }
		
		protected abstract T _cast(object o);
		public abstract void _onHover(T thing);
		public abstract void _onUnhover(T thing);
		public abstract void _onSelect(T thing);
		public abstract void _onDeselect(T thing);

		public virtual void _onGenericHover(object thing) => _onHover(_cast(thing));
		public virtual void _onGenericUnhover(object thing) => _onUnhover(_cast(thing));
		public virtual void _onGenericSelect(object thing) => _onSelect(_cast(thing));
		public virtual void _onGenericDeselect(object thing) => _onDeselect(_cast(thing));
	}



	/**A handler that is notified when objects are selected and hovered.
	 * This handler is only interested in objects of a particular type (which there is a getter for),
	 * and will only receive method calls for that particular type, but receives generic objects as arguments and must cast them to the desired type.
	 * Before a new object is selected/hovered, the old one will be deselected/unhovered first.*/
	public interface GenericTypedSelectionHandler : Destroyable {
		/**The type of object this handler wants to be notified about.*/
		public Type Type { get; }
		public void _onGenericHover(object thing);
		public void _onGenericUnhover(object thing);
		public void _onGenericSelect(object thing);
		public void _onGenericDeselect(object thing);
	}
}