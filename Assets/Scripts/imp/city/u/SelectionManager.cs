using System;
using System.Collections.Generic;
using imp.util;

namespace imp.city.u {

	/**Something that wraps an entity that is part of the world (e.g. a train).
	 * The entity can be null, e.g. for UI elements that may or may not represent a world entity.*/
	public interface WorldEntityWrapper {
		/**The world-object that this object represents / visualizes. For example, if this is a UTrain, this should return the Train.
		 * This can be null, if this type of object supports wrapping world entities but doesn't wrap one in this case.*/
		public object? WorldEntity { get; }
	}



	/**Keeps track of which in-world object is selected, and which one the mouse is hovering over (and could be selected if the user clicks it).
	 * If an object is selected, we will act as if it is no longer hovered, even if the mouse is over it, until it is unselected.
	 * This object contains state, but doesn't do anything unless someone else calls its methods.
	 * Design note: the reason this class exists, instead of just using EventSystem selection, is to support some features in the future:
	 * (1) select a thin object by clicking near it, not just on, where "near" depends on camera zoom
	 * (2) be smart about what object the user is trying to select, when one is larger than another
	 * (3) preferentially select certain kinds of objects when we're in a particular view mode*/
	public class SelectionManager {
		/**_hover is the thing we're actually hovering, while _publicHover is what we tell everyone else is being hovered.
		 * The difference is that _publicHover is null if we're hovering over the selected thing.*/
		private object? _hover, _selection, _publicHover;

		public readonly List<SelectionListener> Listeners = new();
		public readonly Dictionary<Type, List<GenericTypedSelectionHandler>> TypedHandlers = new ();

		public object? Hover => _publicHover;
		public object? Selection => _selection;

		public void ClearHover() => SetHover(null);

		public void SetHover(object? worldEntity) {
			if(_hover == worldEntity)
				return;
			_hover = worldEntity;
			if(worldEntity != null && worldEntity == _selection)
				SetPublicHover(null);
			else
				SetPublicHover(worldEntity);
		}


		private void SetPublicHover(object? worldEntity) {
			if(_publicHover == worldEntity)
				return;

			var prev = _publicHover;
			_publicHover = worldEntity;
			
			// call the handlers last, since they might check what the hover was set to
			if(prev != null) {
				var unhoverHandlers = TypedHandlers.Opt(prev.GetType());
				if(unhoverHandlers != null)
					foreach(var l in unhoverHandlers)
						l._onGenericUnhover(prev);
			}
			if(worldEntity != null) {
				var hoverHandlers = TypedHandlers.Opt(worldEntity.GetType());
				if(hoverHandlers != null)
					foreach(var l in hoverHandlers)
						l._onGenericHover(worldEntity);
			}
			foreach(var l in Listeners)
				l._onHover(worldEntity);
		}


		public void SetSelection(object? worldEntity) {
			if(_selection == worldEntity)
				return;
			
			var prev = _selection;
			_selection = worldEntity;

			// call the handlers last, since they might check what the selection was set to
			if(prev != null) {
				var deselectHandlers = TypedHandlers.Opt(prev.GetType());
				if(deselectHandlers != null)
					foreach(var l in deselectHandlers)
						l._onGenericDeselect(prev);
			}
			if(worldEntity != null) {
				var selectHandlers = TypedHandlers.Opt(worldEntity.GetType());
				if(selectHandlers != null)
					foreach(var h in selectHandlers)
						h._onGenericSelect(worldEntity);
			}
			foreach(var l in Listeners)
				l._onSelect(worldEntity);
		}
	}
}