﻿using System.Collections.Generic;
using imp.util;
using UnityEngine;

namespace imp.city.u {

	/**Maintains an internal map from Unity GameObjects to our C# objects that own/control them.
	 * This is mainly useful for objects that have colliders, allowing us to find the C# object that manages something hit by a raycast etc.
	 * Objects that are registered with this class need to be deregistered when destroyed.
	 * Can only be accessed on the Unity main thread.*/
	public static class UOwners {
		private static readonly Dictionary<GameObject, object> Map = new Dictionary<GameObject, object>();

		public static void Add(GameObject u, object o) {
			Map[u] = o;
		}

		public static object? Get(GameObject u) => Map.Opt(u);

		public static void Remove(GameObject u) => Map.Remove(u);

		public static GameObject OwnedBy(this GameObject u, object o) {
			Add(u, o);
			return u;
		}

		/**Removes this GameObject from the ownership map.*/
		public static GameObject Unowned(this GameObject u) {
			Remove(u);
			return u;
		}

		/**Returns the application object that owns this GameObject, if any.*/
		public static object? Owner(this GameObject u) => Get(u);
	}

}