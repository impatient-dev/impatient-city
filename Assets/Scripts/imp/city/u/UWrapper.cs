using imp.city.destroy;
using imp.util;
using UnityEngine;

namespace imp.city.u {

	public interface Disusable {
		/**Clears / invalidates mutable properties, e.g. by setting floats to NaN and clearing Lists.
		 * Call this when an object should no longer be used, to increase the chance that accidental use will trigger an error.*/
		public void Disuse();
	}


	/**Abstract wrapper for an object that owns/manages a Unity GameObject.*/
	public abstract class UWrapper : Destroyable {
		/**The Unity GameObject.*/
		protected readonly GameObject Obj;

		/**Constructor that wraps a new GameObject.*/
		protected UWrapper(GameObject obj) {
			Obj = obj;
		}

		/**Constructor that wraps a new child GameObject of a parent with the specified name.*/
		protected UWrapper(GameObject parent, string name) {
			Obj = parent.AddChild(name);
		}

		/**Destroys the GameObject. Once this is called, this object cannot be used further.
		 * If you override this method, make sure to call the superclass implementation.*/
		public virtual void Destroy() {
			Obj.Destroy();
		}
	}


	/**Abstract wrapper for an object that owns/manages a Unity GameObject.
	 * This variant allows showing and hiding the object, without destroying it, via public 0-arg methods.
	 * There is no getter telling you whether the object is currently shown/hidden.*/
	public abstract class UWrapperS : UWrapper {

		/**Constructor that wraps a new GameObject.*/
		protected UWrapperS(GameObject obj) : base(obj) { }

		/**Constructor that wraps a new child GameObject of a parent with the specified name.*/
		protected UWrapperS(GameObject parent, string name) : base(parent, name) { }

		/**Makes the GameObject active (visible).
		 * If you override this method, make sure to call the superclass implementation.*/
		public virtual void Show() {
			Obj.SetActive(true);
		}

		/**Makes the GameObject inactive. You can show it again later.
		 * If you override this method, make sure to call the superclass implementation.*/
		public virtual void Hide() {
			Obj.SetActive(false);
		}
	}


	/**Abstract wrapper for an object that owns/manages a Unity GameObject.
	 * This variant allows hiding the object, without destroying it. It also has a protected method to show a hidden object.
	 * Use this variant when you want to support showing/hiding, but showing shouldn't be a public 0-arg method.*/
	public abstract class UWrapperH : UWrapper {

		/**Constructor that wraps a new GameObject.*/
		protected UWrapperH(GameObject obj) : base(obj) { }

		/**Constructor that wraps a new child GameObject of a parent with the specified name.*/
		protected UWrapperH(GameObject parent, string name) : base(parent, name) { }

		/**Makes the GameObject active (visible).*/
		protected void _show() {
			Obj.SetActive(true);
		}

		/**Makes the GameObject inactive. You can show it again later, if the subclass exposes an appropriate method.
		 * If you override this method, make sure to call the superclass implementation.*/
		public virtual void Hide() {
			Obj.SetActive(false);
		}

		/**Shows or hides the object, depending on the argument.*/
		protected virtual void _setVisible(bool visible) {
			if(visible)
				_show();
			else
				Hide();
		}
	}
}