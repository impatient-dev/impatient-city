using System;
using imp.city.input;
using imp.city.r;
using imp.city.wl;
using imp.city.world;
using imp.city.wu;
using UnityEngine;

namespace imp.city.u {
	
	/**A scene that renders a world (as opposed to the main menu or something).*/
	public class WorldScene : AppScene {
		private readonly AppSceneContainer Parent;
		public readonly AppRes R;
		public readonly World W;
		public readonly WorldListeners L = new();
		private WorldView _view;
		
		public readonly InputManager Input = InputManager.Create();
		public readonly ControlManager Control = new();
		public readonly SelectionManager Selection = new();
		public readonly SelectionDriver Driver;


		public WorldScene(AppSceneContainer parent, AppRes r, World w, Func<WorldScene, WorldView> u) {
			Parent = parent;
			R = r;
			W = w;

			Driver = new(Selection, Camera.main!);
			_view = u.Invoke(this);
			_view.Layer = AppDisplays.Layer(AppDisplays.MIN);
			_view.Init();
		}

		public void Destroy() {
			_view.Destroy();
		}

		public void Update(float deltaTime) {
			Input.OnUpdate(deltaTime);
			Driver.Tick(deltaTime);
			WURoot.Tick(W, L, deltaTime);
			L.Time.OnFrame(deltaTime);
		}

		public void OnUiEvent(Event ev) {
			Input.OnEvent(ev);
			if(ev.type == EventType.MouseDown)
				Driver.OnMouseDown(ev.button);
		}

		public void OnApplicationFocus(bool hasFocus) {
			// TODO
		}


		public void Replace(WorldView old, Func<WorldScene, WorldView> nu) {
			if(_view != old)
				throw new Exception($"Wrong world to replace: {old}");
			_view.Destroy();
			_view = nu.Invoke(this);
		}
	}
}