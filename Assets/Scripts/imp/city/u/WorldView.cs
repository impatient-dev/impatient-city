using System;
using System.Collections.Generic;
using imp.city.destroy;
using imp.city.input;
using imp.city.r;
using imp.city.u.handlers;
using imp.city.ui;
using imp.city.wl;
using imp.city.world;
using imp.util;
using UnityEngine;

namespace imp.city.u {
	/**A 2D or 3D view of a virtual world.
	 * This object needs to be initialized immediately after construction.*/
	public abstract class WorldView : Destroyable, Init {
		public WorldScene Scene { get; }
		public abstract int Layer { get; set; }
		public abstract Camera Camera { get; }
		/**A root GameObject that children can be added to.*/
		public abstract GameObject U { get; }
		public readonly WorldUi Ui;
		
		public AppRes R => Scene.R;
		public World W => Scene.W;
		public WorldListeners L => Scene.L;
		public InputManager Input => Scene.Input;
		public SelectionManager Selection => Scene.Selection;
		public ControlManager Control => Scene.Control;

		private List<Init> AfterConstructor = new();

		protected WorldView(WorldScene scene) {
			Scene = scene;
			Ui = new(this);
		}

		public virtual void Destroy() => U.Destroy();

		/**Adds an initialization step that needs to happen immediately after the WorldView and all related objects are constructed.*/
		public void AddInit(Init init) {
			if(AfterConstructor == null)
				throw new Exception("Initialization is already done.");
            AfterConstructor.Add(init);
		}

		public void Init() {
			if(AfterConstructor == null)
				throw new Exception("Initialization is already done.");
			new WorldUiSelectionHandlers(this);
			WorldControlHandlers.Setup(this);
			foreach(var i in AfterConstructor)
				i.Init();
			AfterConstructor = null!;
		}
	}
}