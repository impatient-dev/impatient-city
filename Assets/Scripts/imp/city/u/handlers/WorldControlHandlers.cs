using System;
using imp.city.ui.controls;
using imp.city.world.rail;
using imp.city.world.rail.ctrl;
using imp.city.wu;

namespace imp.city.u.handlers {
	public static class WorldControlHandlers {
		/**Adds a handler that shows the train control GUI when the user takes control of one.*/
		public static void Setup(WorldView view) {
			new TrainCarHandler(view);
		}


		private class TrainCarHandler : CastingControlHandler<TrainCar> {
			private readonly WorldView View;
			public TrainCarHandler(WorldView view) : base(view.Scene) {
				View = view;
				Init();
			}

			public override Type Type => typeof(TrainCar);
			protected override TrainCar _cast(object o) => (TrainCar)o;
			
			public override void _onControl(TrainCar thing) {
				var ctrl = new NotchedTrainControls(thing, 8);
				View.Ui.RemoveControls();
				WUTrainControls.Replace(View.W, View.L, thing.Train, ctrl);
				View.Ui.ShowControls((parent) => new TrainControlUi(parent, View, ctrl));
			}
			
			public override void _onUncontrol(TrainCar thing) {
				View.Ui.RemoveControls();
				WUTrainControls.Replace(View.W, View.L, thing.Train, new NoTrainControls(thing.Train.Controls));
			}
		}
	}
}