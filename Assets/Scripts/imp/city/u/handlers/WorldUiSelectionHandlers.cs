using imp.city.destroy;
using imp.city.ui.sidebar;
using imp.city.world.rail;
using static imp.city.destroy.ListenerUtils;

namespace imp.city.u.handlers {
	/**Shows the appropriate sidebar when things are (de)selected.*/
	public class WorldUiSelectionHandlers : SelectionListener, Destroyable {
		private readonly WorldView View;
		private Destroyable? _currentHover, _currentSelection;

		public WorldUiSelectionHandlers(WorldView view) {
			View = view;
			Register(View.Selection.Listeners, this);
		}

		public void Destroy() {
			_onSelect(null);
			Deregister(View.Selection.Listeners, this);
		}

		public void _onHover(object? worldEntity) {}

		public void _onSelect(object? worldEntity) {
			if(worldEntity == null)
				View.Ui.RemoveSidebar();
			else if(worldEntity is Train train)
				View.Ui.ShowSidebar(parent => new TrainSidebar(parent, View, train));
			else if(worldEntity is TrainCar car)
				View.Ui.ShowSidebar(parent => new TrainCarSidebar(parent, View, car));
			else if(worldEntity is RailPath path)
				View.Ui.ShowSidebar(parent => new RailPathSidebar(parent, View, path));
			else
				View.Ui.RemoveSidebar();
		}
	}
}