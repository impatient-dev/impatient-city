using imp.city.destroy;
using imp.city.world;
using imp.city.world.rail;

namespace imp.city.u.w {
	public abstract class UTracks : Init {
		protected readonly WorldView View;
		private readonly UIdxMap<Destroyable> _segs = new();

		public UTracks(WorldView view) {
			View = view;
			view.AddInit(this);
		}

		public void Init() {
			foreach(var seg in View.W.AllRailSegsWithNulls())
				if(seg != null)
					_segs.PutUnique(seg.Id.Req(), Create(seg));
		}

		protected abstract Destroyable Create(RailSeg seg);
	}
}