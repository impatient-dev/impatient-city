using System;
using imp.city.destroy;
using imp.city.wl;
using imp.city.world;
using imp.city.world.rail;

namespace imp.city.u.w {
	
	/**Displays trains in the world.*/
	public abstract class UTrains : Init, FrameListener, TrainsListener {
		protected readonly WorldView View;
		private readonly UIdxMap<UTrain> _trains = new();

		public UTrains(WorldView view) {
			View = view;
			view.L.Time.Frame.Add(this);
			view.L.Trains.Trains.Add(this);
			view.AddInit(this);
		}

		public void Init() {
			foreach(var train in View.W.AllTrainsWithNulls())
				if(train != null)
					_onAdd(train);
			new CarSelectionHandler(this);
		}

		public void _onAdd(Train train) =>
			_trains.PutUnique(train.Id.Req(), Create(train));
		public void _onRemove(Train train) =>
			_trains.Remove(train.Id.Req()).Destroy();
		public void _onReverse(Train train) {}

		public void _onSplit(Train a, Train b) {
			var aa = _trains.Req(a.Id.Req());
			var bb = aa.Split(b);
			bb.Update();
			_trains.PutUnique(b.Id.Req(), bb);
		}

		protected abstract UTrain Create(Train train);

		public void _onFrame(float deltaTime) {
			foreach(var train in View.W.ActiveTrains())
				_trains.Req(train.Id.Req()).Update();
		}



		private class CarSelectionHandler : DestroyableTypedSelectionHandler<TrainCar> {
			private readonly UTrains Me;

			public CarSelectionHandler(UTrains me) : base(me.View.Scene) {
				Me = me;
				Init();
			}

			public override Type Type => typeof(TrainCar);
			protected override TrainCar _cast(object o) => (TrainCar)o;
			protected override Destroyable? _showHover(TrainCar thing) => Me._trains.Opt(thing.Part.Train.Id)?.CreateCarHoverDecoration(Me.View, thing);
			protected override Destroyable? _showSelected(TrainCar thing) => Me._trains.Opt(thing.Part.Train.Id)?.CreateCarSelectDecoration(Me.View, thing);
		}
	}




	public interface UTrain : Destroyable {
		/**This is called once per frame if the train is active (moving).*/
		public void Update();
		/**Called when parts have been removed from the end of this train and be come a new train.
		 * The argument is the new train.*/
		public UTrain Split(Train split);
		
		public Destroyable? CreateCarHoverDecoration(WorldView v, TrainCar car);
		public Destroyable? CreateCarSelectDecoration(WorldView v, TrainCar car);
	}
}