using UnityEngine;

namespace imp.city.u2 {
	/**Render Depth: Z values for the 2D overhead/map view, used to ensure things correctly appear on top of or under other things.
	 * The depths are all integers; you can fit other things in using +/- 0.5f, etc.*/
	public static class RD {
		/**The sign of Z-direction that goes farther into the background (and is covered up by other items).*/
		public static readonly int BACK = 1;
		/**The sign of Z-direction that goes closer to the camera, appearing on top of background items.*/
		public static readonly int FRONT = -1;
		/**Toward the camera.*/
		public static readonly Vector3 UP = Vector3.back;
		/**Away from the camera.*/
		public static readonly Vector3 DOWN = Vector3.forward;
		
		public static readonly float Camera = 0;
		public static readonly float Train = 4;
		public static readonly float Track = 5;
	}
}