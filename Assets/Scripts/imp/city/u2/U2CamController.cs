using imp.city.input;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.city.u2 {
	/**Pans and zooms the camera.*/
	public class U2CamController : InputHandler {
		/**In half-screens per second.*/
		private static readonly float PAN_SPEED = 2f;
		/**A multiplier for scroll speed. It converts the amount of mouse wheel movement (which can be in the low single digits)
		 * into the amount of change in the natural logarithm of the camera orthographicSize. */
		private static readonly float SCROLL_SPEED = 0.15f;
		
		private readonly Camera Camera;
		private readonly InputInterests Interests;
		public Vector2 Pos { get; private set; }

		private bool PanningUp, PanningDown, PanningLeft, PanningRight;

		public U2CamController(Camera camera, InputManager input, Vector2 pos = new()) {
			Camera = camera;
			camera.orthographic = true;
			camera.orthographicSize = 100;
			camera.backgroundColor = Gray(0.7f);
			camera.gameObject
				.LocalPos(pos.Augment(RD.Camera))
				.LocalRot(Quaternion.LookRotation(Vector3.forward, Vector3.up));
			
			Interests = input.Register(new MyHandler(this));
			Interests.Scroll = Interests.Update = true;
			Interests.AddKeys(KeyCode.W, KeyCode.S, KeyCode.A, KeyCode.D, KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.LeftArrow, KeyCode.RightArrow);
		}


		private class MyHandler : InputHandler {
			private readonly U2CamController It;

			public MyHandler(U2CamController it) { It = it; }
			
			public override void OnUpdate(float deltaTime) {
				int dx = 0, dy = 0;
				if(It.PanningLeft != It.PanningRight)
					dx = It.PanningRight ? 1 : -1;
				if(It.PanningUp != It.PanningDown)
					dy = It.PanningUp ? 1 : -1;
				if(dx == 0 && dy == 0)
					return;
				var movement = new Vector2(dx, dy) * It.Camera.orthographicSize * deltaTime * PAN_SPEED;
				It.Camera.transform.localPosition += movement.Augment();
			}

			private void SetPanning(KeyCode key, bool isPanning) {
				switch(key) {
					case KeyCode.W:
					case KeyCode.UpArrow:
						It.PanningUp = isPanning;
						break;
					case KeyCode.S:
					case KeyCode.DownArrow:
						It.PanningDown = isPanning;
						break;
					case KeyCode.A:
					case KeyCode.LeftArrow:
						It.PanningLeft = isPanning;
						break;
					case KeyCode.D:
					case KeyCode.RightArrow:
						It.PanningRight = isPanning;
						break;
				}
			}

			public override void OnKeyDown(KeyCode key) => SetPanning(key, true);
			public override void OnKeyUp(KeyCode key) => SetPanning(key, false);

			public override void OnScroll(Vector2 delta) {
				var f = It.Camera.orthographicSize;
				f = Mathf.Log(f);
				f -= delta.y * SCROLL_SPEED;
				f = Mathf.Exp(f);
				f = Mathf.Clamp(f, 2, 10_000);
				It.Camera.orthographicSize = f;
			}
		}
	}
}