using imp.city.u;
using imp.util;
using UnityEngine;

namespace imp.city.u2 {
	/**Displays a rectangle made of lines.*/
	public class U2WireframeBox : UWrapper {
		public U2WireframeBox(GameObject parent, Vector2 size, float z, MatAndColor mat, float thickness) : base(parent, "WireBox") {
			var half = size / 2;
			// the ends of each line need to be extended by half the thickness to avoid the corners looking ugly
			var hh = new Vector3(thickness / 2, 0, 0);
			var hv = new Vector3(0, thickness / 2, 0);
			
			Vector3 topRight = new(half.x, half.y, z), topLeft = new(-half.x, half.y, z), bottomRight = new(half.x, -half.y, z), bottomLeft = new(-half.x, -half.y, z);
			mat.ARLine(Obj.AddChild("top"), thickness, new[] { topLeft - hh, topRight + hh });
			mat.ARLine(Obj.AddChild("right"), thickness, new[] { topRight + hv, bottomRight - hv });
			mat.ARLine(Obj.AddChild("bottom"), thickness, new[] { bottomRight + hh, bottomLeft - hh });
			mat.ARLine(Obj.AddChild("left"), thickness, new[] { bottomLeft - hv, topLeft + hv });
		}
	}
}