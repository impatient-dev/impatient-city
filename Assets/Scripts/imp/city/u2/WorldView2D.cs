using imp.city.u;
using imp.city.u2.rail;
using imp.city.u2.train;
using UnityEngine;

namespace imp.city.u2 {
	
	/**Renders the world in 2D, from overhead.
	 * The coordinate system is +X = right, +Y = up, +Z = away from the camera.*/
	public class WorldView2D : WorldView {
		public override GameObject U { get; } = new("2D World");
		public override int Layer { get => U.layer; set => U.layer = value; }
		public override Camera Camera { get; }

		private readonly U2Tracks Tracks;
		private readonly U2Trains Trains;

		public WorldView2D(WorldScene scene, Camera camera, Vector2 cameraPos = new()) : base(scene) {
			Camera = camera;
			new U2CamController(camera, scene.Input, cameraPos);

			Tracks = new(this);
			Trains = new(this);
		}
	}
}