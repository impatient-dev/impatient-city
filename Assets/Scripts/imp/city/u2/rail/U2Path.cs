using System;
using imp.city.destroy;
using imp.city.mesh;
using imp.city.u;
using imp.city.world.geom;
using imp.city.world.rail;
using imp.util;
using UnityEngine;

namespace imp.city.u2.rail {
	/**Renders a path in 2D.*/
	public interface U2Path : Destroyable, WorldEntityWrapper {
		public RailPath Path { get; }

		public static U2Path For(RailPath path, GameObject parent, float width, float z, MatAndColor mat) {
			if(path.Geom.Horizontal.IsCurved)
				return new U2CurvedPath(path, parent, width, z, mat);
			return new U2StraightPath(path, parent, width, z, mat);
		}
	}


	/**Renders a straight path using LineRenderer.*/
	public class U2StraightPath : UWrapper, U2Path {
		public RailPath Path { get; }
		public object WorldEntity => Path;

		public U2StraightPath(RailPath path, GameObject parent, float width, float z, MatAndColor mat): base(parent, "Straight") {
			Path = path;
			Obj.OwnedBy(this);
			var positions2d = new[] {
				Path.Geom.Horizontal.PosAt(0),
				Path.Geom.Horizontal.PosAt(Path.Geom.Length),
			};
			var positions = new[] { positions2d[0].Augment(z), positions2d[1].Augment(z) };
			mat.ARLine(Obj, width, positions);
			Obj.AREdgeCollider2d(positions2d, RailGeom.StandardGauge).AsTrigger2();
		}

		public override void Destroy() {
			Obj.Unowned();
			base.Destroy();
		}
	}


	/**Renders a curved path using a mesh.
	 * LineRenderer doesn't work well for this, because the ends don't "point" straight, so there are gaps between this and the adjacent paths.*/
	public class U2CurvedPath : UWrapper, U2Path {
		/**We represent a curved path by dividing it into straight segments, where the difference in angle between 2 straight sections
		 * (which equals the degree of curvature) is the provided number of radians.*/
		private const float DEGREE_OF_CURVATURE_RAD = Mathf.Deg2Rad * 2;
		
		public RailPath Path { get; }
		public object WorldEntity => Path;

		public U2CurvedPath(RailPath path, GameObject parent, float width, float z, MatAndColor mat) : base(parent, $"Curve") {
			if(width <= 0)
				throw new ArgumentException($"mesh-line width={width}");
			Path = path;
			Obj.OwnedBy(this);

			Obj.LocalPos(new Vector3(0, 0, z));
			mat.ARMesh(Obj, Path2Mesh.Create(Path.Geom.Horizontal, width: width, radians: DEGREE_OF_CURVATURE_RAD));
			Obj.AREdgeCollider2d(Path2Mesh.Line(Path.Geom.Horizontal, radians: DEGREE_OF_CURVATURE_RAD), RailGeom.StandardGauge).AsTrigger2();
		}

		public override void Destroy() {
			Obj.Unowned();
			base.Destroy();
		}
	}
}