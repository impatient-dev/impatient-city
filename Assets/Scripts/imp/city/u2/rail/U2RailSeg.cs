using imp.city.destroy;
using imp.city.u;
using imp.city.world.rail;
using UnityEngine;
using static imp.util.ImpatientUtil;

namespace imp.city.u2.rail {
	public interface U2RailSeg : Destroyable {
		public static U2RailSeg For(RailSeg seg, GameObject parent, float lineWidth, float z, MatAndColor mat) {
			if(seg is BasicRailSeg bseg)
				return new U2BasicRailSeg(parent, bseg, mat, lineWidth, z);
			UnexpectedType(seg);
			return UnreachableR<U2RailSeg>();
		}
	}
	
	
	
	public class U2BasicRailSeg : U2RailSeg {
		private readonly U2Path UPath;
		
		public U2BasicRailSeg(GameObject parent, BasicRailSeg seg, MatAndColor mat, float width, float z) {
			UPath = U2Path.For(seg.Path, parent, width, z, mat);
		}
		
		public void Destroy() => UPath.Destroy();
	}
}