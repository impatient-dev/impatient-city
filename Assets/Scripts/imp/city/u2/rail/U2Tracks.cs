using imp.city.destroy;
using imp.city.u;
using imp.city.u.w;
using imp.city.world.geom;
using imp.city.world.rail;
using UnityEngine;

namespace imp.city.u2.rail {
	
	/**Renders all tracks in the world.*/
	public class U2Tracks : UTracks {
		private readonly float WIDTH = RailGeom.StandardGauge * 1.25f;
		private readonly MatAndColor TrackMat;
		
		public U2Tracks(WorldView view) : base(view) {
			TrackMat = view.R.Mat.Color.With(Color.black);
		}
		
		protected override Destroyable Create(RailSeg seg) => U2RailSeg.For(seg, View.U, WIDTH, RD.Track, TrackMat);
	}
}