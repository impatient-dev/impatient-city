using System;
using imp.city.destroy;
using imp.city.mesh;
using imp.city.u;
using imp.city.world.rail;
using TMPro;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.city.u2.train {
	/**Represents a simple, non-articulated train car with 2 bogies.*/
	public class U2BasicTrainCar : UWrapper, WorldEntityWrapper {
		private readonly TrainCar Car;
		private readonly TextMeshPro Text;
		public object WorldEntity => Car;

		
		public U2BasicTrainCar(GameObject parent, TrainCar car, MatAndColor mat, MatAndColor text) : base(parent, $"Car {car.Part.CarIdx1}") {
			if(car.Part.PositionCount != 2)
				throw new ArgumentException($"Train part has {car.Part.PositionCount} positions/bogies");
			Car = car;

			var textObj = Obj.AddChild("Text").LocalPos(.1f * RD.UP);
			Text = textObj.AR<TextMeshPro>();
			text.Apply(Text);
			Text.alignment = TextAlignmentOptions.Center;
			Text.overflowMode = TextOverflowModes.Ellipsis;
			var rect = textObj.Require<RectTransform>();
			rect.sizeDelta = VisibleSize(Car.Part);
			Text.enableAutoSizing = true;
			Text.text = car.Design.TypeStr;

			Update();
			var mesh = Mesh2Util.Rectangle(VisibleSize(Car.Part));
			mat.ARMesh(Obj, mesh);
			
			Obj.AR<BoxCollider2D>().AsTrigger(SelectableSize(Car.Part).Augment(1));
			Obj.OwnedBy(this);
		}

		public override void Destroy() {
			Obj.Unowned();
			base.Destroy();
		}

		/**Needs to be called every frame if the train is moving.*/
		public void Update() {
			var front = Car.Part.Pos(0).Pos.Pos3d().xz();
			var rear = Car.Part.Pos(1).Pos.Pos3d().xz();
			var center = ((front + rear) * 0.5f).Augment(RD.Train);
			Obj.transform.SetPositionAndRotation(center, RotX(front - rear));
		}
		
		private static Vector2 VisibleSize(TrainPart part) => new(part.Design.LengthExcludingCouplers, part.Design.Width);
		private static Vector2 SelectableSize(TrainPart part) => new(part.Design.TotalLength, part.Design.Width);
		private Destroyable WireBox(MatAndColor mat) => new U2WireframeBox(Obj, VisibleSize(Car.Part), 0.5f * RD.FRONT, mat, Car.Part.Design.Width / 10f);

		public Destroyable CreateHoverDecoration(WorldView v) => WireBox(v.R.MP.Hover);
		public Destroyable CreateSelectDecoration(WorldView v) => WireBox(v.R.MP.Selected);
	}
}