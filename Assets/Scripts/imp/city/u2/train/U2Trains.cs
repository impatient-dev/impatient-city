using System.Collections.Generic;
using imp.city.destroy;
using imp.city.u;
using imp.city.u.w;
using imp.city.world.rail;
using static imp.util.ImpatientUtil;

namespace imp.city.u2.train {
	public class U2Trains : UTrains {
		
		public U2Trains(WorldView view) : base(view) {}

		protected override UTrain Create(Train train) => new U2Train(View, train, View.R.MP.TrainCar, View.R.MP.TrainCarText);
	}


	public class U2Train : UWrapper, UTrain {
		private readonly WorldView View;
		private readonly Train Train;
		private readonly List<U2BasicTrainCar> Cars;
		private MatAndColor Mat, Text;

		public U2Train(WorldView view, Train train, MatAndColor mat, MatAndColor text) : base(view.U, $"Train {train.Id}") {
			View = view;
			Train = train;
			Mat = mat;
			Text = text;
			Cars = new(train.Parts.Parts);
			foreach(var part in train.Parts.PartsList)
				Cars.Add(new U2BasicTrainCar(Obj, part.Cars[0], mat, text));
		}

		private U2Train(WorldView view, Train train, MatAndColor mat, MatAndColor text, List<U2BasicTrainCar> cars) : base(view.U, $"Train {train.Id}") {
			CheckArg(cars.IsNotEmpty(), "No cars in train.");
			View = view;
			Train = train;
			Mat = mat;
			Text = text;
			Cars = cars;
		}

		public void Update() {
			foreach(var car in Cars)
				car.Update();
		}

		public Destroyable CreateCarHoverDecoration(WorldView v, TrainCar car) => Cars[car.IdxInTrain].CreateHoverDecoration(v);
		public Destroyable CreateCarSelectDecoration(WorldView v, TrainCar car) => Cars[car.IdxInTrain].CreateSelectDecoration(v);

		public UTrain Split(Train split) => new U2Train(View, split, Mat, Text, Cars.SplitFrom(Train.Parts.Cars));
	}
}