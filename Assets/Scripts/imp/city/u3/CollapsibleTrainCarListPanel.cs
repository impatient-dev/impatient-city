using imp.city.ui;
using imp.city.world.rail;
using static imp.city.Main;

namespace imp.city.u3 {
	public class CollapsibleTrainCarListPanel {
		private static bool DefaultOpen = false;
		private readonly CBtn bShow;
		private readonly CLinearLayout _listHolder;

		public CollapsibleTrainCarListPanel(CParent parent, Train train, OnClick onClick) {
			var style = R.Style;
			var root = parent.CColL("Train Car List");

			var btnRow = root.CRow("Collapse / Expand");
			btnRow.CText("Car Count", style.Title, $"{train.Parts.Cars} Cars");
			bShow = btnRow.CBtn("Show / Hide", style.Btn, ToggleOpen);
			
			_listHolder = root.CColL("List");
			foreach(var part in train.Parts.PartsList) {
				foreach(var car in part.Cars) {
					_listHolder.CLink($"Car {car.IdxInTrain}", style.Link, () => onClick.OnClick(car), $"{car.IdxInTrain + 1} {car.Design.TypeStr}");
				}
			}

			SetOpen(DefaultOpen);
		}

		private void SetOpen(bool open) {
			_listHolder.Active(open);
			bShow.Text(open ? "Collapse" : "Expand");
			DefaultOpen = open;
		}
		private void ToggleOpen() => SetOpen(!_listHolder.Active);



		public interface OnClick {
			public void OnClick(TrainCar car);
		}
	}
}