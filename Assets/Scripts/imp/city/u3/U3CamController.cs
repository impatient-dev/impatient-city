﻿using UnityEngine;

namespace imp.city.u3 {

	public class U3CamController {
		private static readonly float PAN_SPEED = 10;
		private bool Forward, Backward, Left, Right, Up, Down;
		private Vector3 LastMousePos = Vector3.zero;
		private bool MouseRotating;
		
		private readonly Camera Camera;

		public U3CamController() {
			Camera = Camera.main!;
			Camera.transform.position = new Vector3(0, 15, -10);
			Camera.transform.LookAt(Vector3.zero);
		}

		public void OnMouseDown(int button) {
			if (button == 2) {
				MouseRotating = true;
				LastMousePos = Input.mousePosition;
			}
		}

		public void OnMouseUp(int button) {
			if(button == 2)
				MouseRotating = false;
		}

		public void OnKeyDown(KeyCode code) {
			switch (code) {
				case KeyCode.W:
					Forward = true;
					break;
				case KeyCode.S:
					Backward = true;
					break;
				case KeyCode.A:
					Left = true;
					break;
				case KeyCode.D:
					Right = true;
					break;
				case KeyCode.E:
					Up = true;
					break;
				case KeyCode.C:
				case KeyCode.Q:
					Down = true;
					break;
			}
		}
		
		public void OnKeyUp(KeyCode code) {
			switch (code) {
				case KeyCode.W:
					Forward = false;
					break;
				case KeyCode.S:
					Backward = false;
					break;
				case KeyCode.A:
					Left = false;
					break;
				case KeyCode.D:
					Right = false;
					break;
				case KeyCode.E:
					Up = false;
					break;
				case KeyCode.C:
				case KeyCode.Q:
					Down = false;
					break;
			}
		}
		
		public void Update() {
			var s = PAN_SPEED * Time.deltaTime;
			var pan = new Vector3(DirF(s, Right, Left), DirF(s, Up, Down), DirF(s, Forward, Backward));
			var transform = Camera.transform;
			transform.Translate(pan);

			if (MouseRotating) {
				//let the dragging mouse rotate the camera
				var pos = Input.mousePosition;
				var rotX = LastMousePos.y - pos.y;
				var rotY = pos.x - LastMousePos.x;
				transform.Rotate(rotX, rotY, 0);
				LastMousePos = pos;
				
				//fix the rotation so the camera isn't tilted: the right/left vector must be in the X-Z plane
				var right = Camera.transform.right;
				var newRight = Vector3.ProjectOnPlane(right, Vector3.up);
				var applyRotation = Quaternion.FromToRotation(right, newRight);
				transform.rotation = applyRotation * Camera.transform.rotation;
			}
		}

		private static float DirF(float magnitude, bool positive, bool negative) {
			if (positive)
				return negative ? 0 : magnitude;
			if (negative)
				return -magnitude;
			return 0f;
		}
	}

}