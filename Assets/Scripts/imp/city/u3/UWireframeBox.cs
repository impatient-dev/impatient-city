﻿using imp.city.r;
using imp.util;
using UnityEngine;

namespace imp.city.u3 {

	/**Maintains a set of 3D lines that form a box. Useful for highlighting a selected object.
	 * This object is initially inactive and missing important data; you must call Show to initially make this display something,
	 * then can set the attributes (like Size) to change its appearance later.*/
	public class UWireframeBox {
		private readonly GameObject _root;
		private readonly LineRenderer[] _lineRenderers;
		private Vector3 _size;
		private Material _material;
		private float _thickness;

		public UWireframeBox(AppRes r) {
			_root = new GameObject("WireframeBox");
			_root.SetActive(false);
			_lineRenderers = new LineRenderer[12];
			_material = r.Mat.Selecting;
			for (var c = 0; c < _lineRenderers.Length; c++) {
				var lr = _root.AddChild("Line" + c).AR<LineRenderer>();
				lr.material = _material;
				lr.useWorldSpace = false; //use parent object transform
				lr.numCapVertices = 1;
				_lineRenderers[c] = lr;
			}
		}

		/**You can set the position and rotation, or set the parent transform.*/
		public Transform Transform => _root.transform;

		public void Destroy() {
			Object.Destroy(_root);
		}

		/**Resets this wireframe to the state it was in when first constructed, including being inactive and having no parent.
		 * You can reuse this wireframe by calling Show() again.*/
		public void HideForReuse() {
			_root.SetActive(false);
			_root.transform.DetachAndReset();
		}
		

		/**Sets the material and vertices, and makes this object active. You may want to set the transform's position and/or parent afterward.*/
		public void Show(Vector3 size, Material material, float thickness) {
			Size = size;

			foreach(var lr in _lineRenderers) {
				lr.material = material;
				lr.startWidth = thickness;
				lr.endWidth = thickness;
			}
			
			_material = material;
			_thickness = thickness;
			_root.SetActive(true);
		}


		public Vector3 Size {
			get => _size;
			
			set {
				var size = value;
				var hx = size.x / 2;
				var hy = size.y / 2;
				var hz = size.z / 2;
				var i = 0;
			
				//top quad: +Y
				_lineRenderers[i++].SetPositions(new[] {new Vector3(-hx, hy, hz), new Vector3(hx, hy, hz)});
				_lineRenderers[i++].SetPositions(new[] {new Vector3(hx, hy, hz), new Vector3(hx, hy, -hz)});
				_lineRenderers[i++].SetPositions(new[] {new Vector3(hx, hy, -hz), new Vector3(-hx, hy, -hz)});
				_lineRenderers[i++].SetPositions(new[] {new Vector3(-hx, hy, -hz), new Vector3(-hx, hy, hz)});
			
				//bottom quad: -Y
				_lineRenderers[i++].SetPositions(new[] {new Vector3(-hx, -hy, hz), new Vector3(hx, -hy, hz)});
				_lineRenderers[i++].SetPositions(new[] {new Vector3(hx, -hy, hz), new Vector3(hx, -hy, -hz)});
				_lineRenderers[i++].SetPositions(new[] {new Vector3(hx, -hy, -hz), new Vector3(-hx, -hy, -hz)});
				_lineRenderers[i++].SetPositions(new[] {new Vector3(-hx, -hy, -hz), new Vector3(-hx, -hy, hz)});
			
				//remainder of the left: -X
				_lineRenderers[i++].SetPositions(new[] {new Vector3(-hx, -hy, -hz), new Vector3(-hx, hy, -hz)});
				_lineRenderers[i++].SetPositions(new[] {new Vector3(-hx, -hy, hz), new Vector3(-hx, hy, hz)});
			
				//remainder of the left: +X
				_lineRenderers[i++].SetPositions(new[] {new Vector3(hx, -hy, -hz), new Vector3(hx, hy, -hz)});
				_lineRenderers[i++].SetPositions(new[] {new Vector3(hx, -hy, hz), new Vector3(hx, hy, hz)});

				_size = size;
			}
		}


		public Material Material {
			get => _material;
			
			set {
				foreach(var lr in _lineRenderers)
					lr.material = value;
				_material = value;
			}
		}

		public float Thickness {
			get => _thickness;
			
			set {
				foreach (var lr in _lineRenderers) {
					lr.startWidth = value;
					lr.endWidth = value;
				}
				_thickness = value;
			}
		}
	}

}