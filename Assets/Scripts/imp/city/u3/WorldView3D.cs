﻿using imp.city.u;
using imp.city.u3.rail;
using imp.city.u3.train;
using UnityEngine;

namespace imp.city.u3 {

	public class WorldView3D : WorldView {
		public override GameObject U { get; } = new("3D World");
		public override int Layer { get => U.layer; set => U.layer = value; }
		public override Camera Camera { get; }
		
		private readonly U3Tracks Tracks;
		private readonly U3Trains Trains;
		
		public WorldView3D(WorldScene scene) : base(scene) {
			Camera = Camera.main!;
			Tracks = new(this);
			Trains = new(this);
		}
	}
}