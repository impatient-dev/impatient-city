﻿using imp.city.destroy;
using imp.city.r;
using imp.city.u;
using imp.city.u3.util;
using imp.city.world.geom;
using imp.city.world.rail;
using imp.util;
using UnityEngine;

namespace imp.city.u3.rail {

	/**Renders a basic rail segment. Currently just renders ties; we'll put rails on top sometime.*/
	public class U3BasicRailSeg : Destroyable {
		private readonly GameObject U;

		public U3BasicRailSeg(BasicRailSeg segment, AppRes r, GameObject parent) {
			float gauge = RailGeom.StandardGauge;
			U = parent.AddChild("RailSegBasic").OwnedBy(this);
			
			//calculate how many ties we'll use
			var path = segment.Path;
			var idealTieSpacing = gauge / 3;
			var lengthContainingTies = path.Geom.Length - idealTieSpacing; //exclude 1 spacing - 1/2 a spacing at each end of the path
			var tieCount = Mathf.RoundToInt(lengthContainingTies / idealTieSpacing);
			var tieSpacing = lengthContainingTies / tieCount;

			//create ties
			var tieMesh = Mesh3Util.BoxMesh(new Vector3(gauge / 8f, gauge / 16f, gauge * 1.25f)); //oriented so +X faces forward, towards the end of the path
			for (var t = 0; t < tieCount; t++) {
				var dist = idealTieSpacing / 2f + t * tieSpacing;
				var tieObj = U.AddChild("tie");
				tieObj.AR<MeshFilter>().mesh = tieMesh;
				tieObj.AR<MeshRenderer>().material = r.Mat.Rail;
				tieObj.transform.position = path.Geom.PosAt(dist) - new Vector3(0, -gauge / 8f, 0);
				tieObj.transform.right = path.Geom.DirAt(dist);
			}

			//create a set of selectable boxes that basically cover the path
			var selectableWidth = gauge * 2;
			var selectableHeight = gauge * 2;
			var selectableDivisions = ChooseNumDivisionsForSelection(path.Geom, gauge);
			var selectableLength = path.Geom.Length / selectableDivisions;
			for (var c = 0; c < selectableDivisions; c++) {
				var boxStart = path.Geom.PosAt(c * selectableLength);
				var boxEnd = path.Geom.PosAt(c * selectableLength);
				var boxMid = (boxStart + boxEnd) * 0.5f;
				var selectableObj = U.AddChild("sel");
				
				//TODO
			}
		}


		/// Decides how many pieces we should divide a path into when creating selectable boxes to represent the path.
		private static int ChooseNumDivisionsForSelection(PathHV path, float gauge) {
			var hRadius = path.Horizontal.MinRadius;
			var vRadius = path.Vertical.MinRadius;
			var divisionLength = Mathf.Min(hRadius, vRadius) * gauge / RailGeom.StandardGauge / 3; //std gauge: 1/3 the radius
			return Mathf.CeilToInt(path.Length / divisionLength);
		}
		
		public void Destroy() => U.Destroy();
	}
}