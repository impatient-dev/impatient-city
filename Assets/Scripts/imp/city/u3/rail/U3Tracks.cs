using imp.city.destroy;
using imp.city.u;
using imp.city.u.w;
using imp.city.world.rail;
using static imp.util.ImpatientUtil;

namespace imp.city.u3.rail {
	/**Renders all tracks in the world.*/
	public class U3Tracks : UTracks {
		
		public U3Tracks(WorldView view) : base(view) {
		}
		
		protected override Destroyable Create(RailSeg seg) {
			if(seg is BasicRailSeg bseg)
				return new U3BasicRailSeg(bseg, View.R, View.U);
			UnexpectedType(seg);
			return UnreachableR<Destroyable>();
		}
	}
}