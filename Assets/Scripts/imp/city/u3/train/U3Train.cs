using System;
using System.Collections.Generic;
using imp.city.destroy;
using imp.city.r;
using imp.city.u;
using imp.city.u.w;
using imp.city.world;
using imp.city.world.rail;

namespace imp.city.u3.train {
	public class U3Train : UTrain {
		public readonly Train Train;
		private readonly List<UTrainPart> _parts;

		public U3Train(World chunk, AppRes r, Train train) {
			Train = train;
			_parts = new(train.Parts.Parts);
			for(var p = 0; p < train.Parts.Parts; p++)
				_parts.Add(new UTrainPart(chunk, r, train, p));
		}

		public void Update() {
			foreach(var part in _parts)
				part.Update();
		}

		public void Destroy() {
			foreach(var part in _parts)
				part.Destroy();
		}

		public Destroyable? CreateCarHoverDecoration(WorldView v, TrainCar car) => null;
		public Destroyable? CreateCarSelectDecoration(WorldView v, TrainCar car) => null;

		public UTrain Split(Train split) => throw new NotImplementedException();
	}
}