using imp.city.u;
using imp.city.u.w;
using imp.city.world.rail;

namespace imp.city.u3.train {
	public class U3Trains : UTrains {
		public U3Trains(WorldView view) : base(view) {}

		protected override u.w.UTrain Create(Train train) => new U3Train(View.W, View.R, train);
	}
}