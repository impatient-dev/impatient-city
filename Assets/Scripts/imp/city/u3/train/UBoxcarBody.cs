﻿using System;
using imp.city.r;
using imp.city.u;
using imp.city.world.rail;
using imp.city.world.rail.cars;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;


namespace imp.city.u3.train {
	
	/**Renders the "body" part of a boxcar, excluding the bogies.*/
	public class UBoxcarBody : UWrapper {
		public UBoxcarBody(GameObject parent, TrainPart part, BoxcarDesign design, AppRes r) : base(parent.AddChild("Boxcar")) {
			throw new NotImplementedException();
			// Obj.AR<MeshFilter>().mesh = BoxMesh(part.Design.SelectionBox);
			// Obj.AR<MeshRenderer>().material = r.Mat.RailCar;
		}
	}
}