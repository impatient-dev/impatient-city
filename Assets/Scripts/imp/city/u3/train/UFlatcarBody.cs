﻿using System;
using imp.city.r;
using imp.city.u;
using imp.city.world.rail.cars;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;


namespace imp.city.u3.train {
	
	/**Renders the "body" part of a flatcar, excluding the bogies.*/
	public class UFlatcarBody : UWrapper {
		public UFlatcarBody(GameObject parent, FlatcarDesign design, AppRes r) : base(parent.AddChild("Flatcar")) {
			throw new NotImplementedException();
			// Obj.AR<MeshFilter>().mesh = BoxMesh(design.SelectionBox());
			// Obj.AR<MeshRenderer>().material = r.Mat.RailCar;
		}
	}
}