﻿using System;
using imp.city.r;
using imp.city.u;
using imp.city.world;
using imp.city.world.rail;
using imp.util;
using UnityEngine;

namespace imp.city.u3.train {

	public class UTrainPart {
		public readonly Train Train;
		public readonly TrainPart Part;
		public readonly World Chunk;
		
		public readonly GameObject Obj;

		public UTrainPart(World chunk, AppRes r, Train train, int carIndex) {
			Train = train;
			Part = train.Parts.Part(carIndex);
			Chunk = chunk;
			if(Part.PositionCount != 2)
				throw new Exception($"Train part has {Part.PositionCount} positions/bogies");
			Obj = new GameObject("TrainPart").OwnedBy(this);

			throw new NotImplementedException();
			// if(Part.Design is BoxcarDesign bd)
			// 	new UBoxcarBody(Obj, bd, r);
			// else if(Part.Design is FlatcarDesign fd)
			// 	new UFlatcarBody(Obj, fd, r);
			// else
			// 	UnexpectedType(Part.Design);
			//
			// var collider = Obj.AR<BoxCollider>();
			// collider.size = Part.Design.SelectionBox;
			// collider.isTrigger = true;
		}

		public void Update() {
			var frontBogiePos = Part.Pos(0).Pos.Pos3d();
			var rearBogiePos = Part.Pos(1).Pos.Pos3d();
			var centerPos = (frontBogiePos + rearBogiePos) * 0.5f + new Vector3(0, Part.Design.Height / 2, 0);
			Obj.transform.SetPositionAndRotation(centerPos, Quaternion.LookRotation(frontBogiePos - rearBogiePos, Vector3.up));
		}

		public void Destroy() {
			Obj.Destroy();
		}
	}

}