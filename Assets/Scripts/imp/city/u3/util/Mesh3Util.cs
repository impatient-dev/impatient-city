﻿using UnityEngine;

namespace imp.city.u3.util {

	public static class Mesh3Util {
		/// Creates a box centered around the origin, with the provided length/width/height.
		/// Different faces (e.g. top vs left) do not share vertices, because then they would share/blend normals.
		public static Mesh BoxMesh(Vector3 size) {
			float hx = size.x / 2, hy = size.y / 2, hz = size.z / 2; //half dimensions
			float bottom = hz, top = -hz;
			var mesh = new Mesh();
			
			mesh.vertices = new[] { //6 rectangles, clockwise from the direction they face
				//front (+Z)
				new Vector3(hx, hy, bottom),
				new Vector3(-hx, hy, bottom),
				new Vector3(-hx, -hy, bottom),
				new Vector3(hx, -hy, bottom),
				//back (-Z)
				new Vector3(hx, hy, top),
				new Vector3(hx, -hy, top),
				new Vector3(-hx, -hy, top),
				new Vector3(-hx, hy, top),
				//left (-X)
				new Vector3(-hx, hy, bottom),
				new Vector3(-hx, hy, top),
				new Vector3(-hx, -hy, top),
				new Vector3(-hx, -hy, bottom),
				//right (+X)
				new Vector3(hx, hy, bottom),
				new Vector3(hx, -hy, bottom),
				new Vector3(hx, -hy, top),
				new Vector3(hx, hy, top),
				//top (+Y)
				new Vector3(hx, hy, bottom),
				new Vector3(hx, hy, top),
				new Vector3(-hx, hy, top),
				new Vector3(-hx, hy, bottom),
				//bottom (-Y)
				new Vector3(-hx, -hy, bottom),
				new Vector3(-hx, -hy, top),
				new Vector3(hx, -hy, top),
				new Vector3(hx, -hy, bottom),
			};
			
			var triangles = new int[6 * 6];
			for (var face = 0; face < 6; face++) {
				triangles[6 * face + 0] = 4 * face + 0;
				triangles[6 * face + 1] = 4 * face + 1;
				triangles[6 * face + 2] = 4 * face + 2;
				triangles[6 * face + 3] = 4 * face + 2;
				triangles[6 * face + 4] = 4 * face + 3;
				triangles[6 * face + 5] = 4 * face + 0;
			}
			
			mesh.triangles = triangles;
			mesh.RecalculateNormals();
			return mesh;
		}
	}

}