using static imp.city.Main;
using static imp.city.ui.CUI;

namespace imp.city.ui {
	/**A place to put higher-level UI creation code, unlike CUI which creates or customizes one thing at a time.*/
	public static class AppUi {
		/**Creates the app sidebar, with a vertical layout in a scroll view.*/
		public static CLinearLayout AppSidebarV(this CCell cell) => cell
			.RemoveAnyLayout()
			.Back(R.Color.Ui.Back)
			.CScrollFull(CScrollVertical)
			.CCol("Col", CLeNone, R.Style.PanelCPad);
	}
}