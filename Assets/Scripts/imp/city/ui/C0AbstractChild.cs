using System;
using imp.util;
using UnityEngine;

namespace imp.city.ui {

	/**A Canvas GameObject wrapper that knows about its parent and notifies the parent when the child is created and destroyed.
	 It also exposes the GameObject (U), and destroys it when we are destroyed.*/
	public abstract class CAbstractChild : CChild {
		protected readonly CParent Parent;
		public GameObject U { get; }
		public CPosType PosType { get; protected set; }

		protected CAbstractChild(CParent parent, GameObject u, CPosType posType) {
			if(posType == CPosType.Layout && !parent.HasLayout)
				throw new Exception($"Child {u.name} expects to be positioned by a layout group, but parent {parent.Name} has none.");
			else if(posType == CPosType.Rect && parent.HasLayout)
				throw new Exception($"Child {u.name} expects to position itself via RectTransform, but the parent {parent.Name} has a layout group.");
			
			Parent = parent;
			U = u;
			PosType = posType;
			parent.Add(this);
		}

		/**Constructor that creates a new GameObject with the provided name.*/
		protected CAbstractChild(CParent parent, string name, CPosType posType) : this(parent, new GameObject(name), posType) {}

		public virtual void Destroy() {
			Parent.PreDestroy(this);
			U.Destroy();
		}
		public virtual void DestroyWithParent() {}
		public virtual void DestroyFromParent() => U.Destroy();
	}
}