using UnityEngine.UI;

namespace imp.city.ui {
	
	/**A GameObject wrapper that allows you to add and remove an Image component to display a color or other background.*/
	public interface CBackground : CObj {
		public Image? BackImage { get; }
		/**Returns the current background, enabling it if necessary, or creating a new one if there is not current image.*/
		public Image GetOrCreateBackImage();
		/**Disables or removes the image component if it exists.
		 * Disabling is safer; if the object chooses to destroy the component, any attempt to recreate it during the same frame will fail
		 * (because destruction is delayed until the end of the frame).*/
		public void ClearBackImage();
	}
}