using imp.city.r;
using imp.util;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace imp.city.ui {
	public class CBtn : CAbstractChild, CDisable, CActive {
		private readonly BtnStyle Style;
		private readonly Button Btn;
		private readonly Image Background;
		private readonly TextMeshProUGUI Pro;
		private readonly GameObject _inner;

		public CBtn(CParent parent, string name, BtnStyle style, UnityAction onClick, string text) : base(parent, name, CPosType.Layout) {
			Style = style;

			// 3 objects feels excessive, but the margin / padding don't work without this.
			// When the Button and Text are combined, AR<text> fails, probably because the Canvas Renderer component is added by the background Image.
			U.WHorizontal(style.Margin);
			_inner = U.AddChild("Btn").WHorizontal(style.Padding);
			var t = _inner.AddChild("Txt");
			
			Background = _inner.ARBackground(style.Back);
			Btn = _inner.AR<Button>();
			Btn.onClick.AddListener(onClick);
			
			Pro = t.AR<TextMeshProUGUI>();
			Pro.material = style.Text.R.Mat.Color.Mat; // TODO
			Pro.fontSize = style.Text.Size;
			Pro.text = text;

			_setAppearance(true);
		}

		private void _setAppearance(bool enabled) {
			if(enabled) {
				Pro.font = Style.Text.TmpFont;
				Pro.color = Style.Text.Color;
				Pro.fontStyle = Style.Text.TmpStyle;
				Background.color = Style.Back;
			} else {
				Pro.font = Style.TextDisabled.TmpFont;
				Pro.color = Style.TextDisabled.Color;
				Pro.fontStyle = Style.TextDisabled.TmpStyle;
				Background.color = Style.BackDisabled;
			}
		}

		public bool Enabled {
			set {
				Btn.enabled = value;
				_setAppearance(value);
			}
		}

		public bool Active { set => U.SetActive(value); }

		public CBtn Text(string str) {
			Pro.text = str;
			return this;
		}
	}
}