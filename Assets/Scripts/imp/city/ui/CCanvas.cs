using System.Collections.Generic;
using imp.util;
using UnityEngine;
using UnityEngine.UI;

namespace imp.city.ui {
	public class CCanvas : CParent, CBackground {
		public GameObject U { get; }
		private readonly List<CChild> Children = new();
		public bool HasLayout => false;
		public CPosType PosType => CPosType.Rect;
		public string Name => U.name;

		public CCanvas(string name, CRectCustomizer rect) {
			U = new GameObject(name);
			rect.Customize(U.AR<RectTransform>());
			U.WCanvas();
		}

		public CCanvas(string name, CRectCustomizer rect, GameObject parent) : this(name, rect) {
			U.transform.SetParent(parent.transform, worldPositionStays: true);
		}

		public override string ToString() => $"Canvas({U.name})";

		public void Add(CChild child) {
			child.U.transform.SetParent(U.transform, worldPositionStays: true);
			Children.Add(child);
		}

		public void PreDestroy(CChild child) => Children.Remove(child);

		public void Destroy() {
			foreach(var child in Children)
				child.DestroyWithParent();
			U.Destroy();
		}

		public void Clear() {
			foreach(var child in Children)
				child.DestroyFromParent();
			Children.Clear();
		}

		public Image? BackImage => U.GetComponent<Image>()?.DisabledToNull();
		public Image GetOrCreateBackImage() => U.GetOrCreateComponent<Image>().Enabled(true);
		public void ClearBackImage() => U.GetComponent<Image>()?.Enabled(false);
	}
}