using System;
using System.Collections.Generic;
using imp.city.destroy;
using imp.util;
using UnityEngine;
using UnityEngine.UI;

namespace imp.city.ui {

	/**A part (specified via the RectOffset) of the parent that is available to be filled by some part of the UI.
	 * The intended use case is for containing part of the UI that needs to by dynamically replaced by content that may have different requirements.
	 * If you provide this cell to some content, the content can add children, add a background image, and add a layout group;
	 * you can reset this cell to undo all those changes, and can then reuse this cell for a different piece of content.
	 * If you're writing a piece of content that goes into one of these cells, you generally shouldn't destroy or reset the cell.*/
	public class CCell : CParent, CChild, CBackground, COnDestroy {
		public GameObject U { get; private set; }
		public readonly CParent Parent;
		private readonly List<CChild> Children = new();
		
		public LayoutGroup? Layout { get; private set; }
		public bool HasLayout => Layout != null;
		private readonly CRectCustomizer RectCustomizer;
		private Destroyable? OnDestroy;
		
		public CPosType PosType => CPosType.Rect;
		public string Name => U.name;
		
		public CCell(CParent parent, string name, CRectCustomizer rectCustomizer) {
			if(parent.HasLayout)
				throw new Exception($"Cell {name} can't be the child of parent {parent} because it has a layout.");
			Parent = parent;
			RectCustomizer = rectCustomizer;
			U = new GameObject(name);
			parent.Add(this);
			rectCustomizer.Customize(U.AR<RectTransform>());
		}

		public override string ToString() => $"Cell({U.name})";

		public void Add(CChild child) {
			child.U.transform.SetParent(U.transform, worldPositionStays: true);
			Children.Add(child);
		}

		public void PreDestroy(CChild child) => Children.Remove(child);

		public void Destroy() {
			Parent.PreDestroy(this);
			foreach(var child in Children)
				child.DestroyWithParent();
			U.Destroy();
			OnDestroy?.Destroy();
		}

		public void DestroyWithParent() {
			foreach(var child in Children)
				child.DestroyWithParent();
			OnDestroy?.Destroy();
		}

		public void DestroyFromParent() {
			foreach(var child in Children)
				child.DestroyWithParent();
			U.Destroy();
			OnDestroy?.Destroy();
		}

		public void Clear() {
			foreach(var child in Children)
				child.DestroyFromParent();
			Children.Clear();
		}

		public Image? BackImage => U.GetComponent<Image>()?.DisabledToNull();
		public Image GetOrCreateBackImage() => U.ProvideEnabledImage();
		public void ClearBackImage() => U.GetComponent<Image>()?.Enabled(false);

		public void SetOnDestroy(Destroyable d) {
			if(OnDestroy != null)
				throw new Exception(this + " already has an OnDestroy object set.");
			OnDestroy = d;
		}

		/**Removes all children and disables any background Image, and destroys and forgets anything provided to SetOnDestroy.
		 * However, this does not remove the layout - because destruction is delayed until later in the frame, you would not be able to add any layout on the same frame.
		 * After this method called, this cell is ready to be taken over by a new piece of UI code, and that code is expected to immediately call RemoveAnyLayout or ApplyLayout.*/
		public CCell ResetExceptLayout() {
			Clear();
			ClearBackImage();
			OnDestroy?.Destroy();
			OnDestroy = null;
			return this;
		}

		/**This should only be called when a new owner is taking over this cell, usually immediately after a Reset.*/
		public CCell RemoveAnyLayout() {
			Layout?.Destroy();
			Layout = null;
			return this;
		}

		/**This should only be called when a new owner is taking over this cell, usually immediately after a Reset.
		 * If the layout conflicts with our current layout, this function will transparently destroy and recreate the object
		 * (since destroying the current layout won't finish until the end of the frame).
		 * Otherwise, it will customize the existing layout.
		 * Because of the possible object recreation, there must not be a background image when this is called - this function cannot transparently recreate it.*/
		public void ApplyLayout<L>(CLinearLayoutCustomizer customizer) where L : HorizontalOrVerticalLayoutGroup {
			if(Layout == null) {
				L layout = U.AR<L>();
				customizer.Customize(layout);
				Layout = layout;
			} else if(Layout is L l) {
				customizer.Customize(l);
			} else {
				// we have a conflicting layout, so we have to destroy and recreate this cell
				if(BackImage != null)
					throw new Exception($"We need to destroy and recreate cell {U.name}, but this would destroy the background image.");
				var u2 = new GameObject(U.name);
				var layout = u2.AR<L>();
				RectCustomizer.Customize(u2.AR<RectTransform>());
				customizer.Customize(layout);
				Parent.PreDestroy(this);
				U = u2;
				Layout = layout;
				Parent.Add(this);
			}
		}
	}
}