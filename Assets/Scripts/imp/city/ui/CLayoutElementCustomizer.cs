using imp.util;
using UnityEngine;
using UnityEngine.UI;

namespace imp.city.ui {
	public interface CLayoutElementCustomizer {
		/**Applies any customization related to the LayoutElement component.
		 * This method might be called on a reused object, so all implementations must delete, reset, or customize the component.*/
		public void Customize(GameObject u);
		/**Applies customization related to the LayoutElement component in cases where the object is new and isn't being reused.
		 * LayoutElement will not be present. By default does the same thing as Customize.*/
		public void CustomizeFresh(GameObject u) => Customize(u);
	}


	public class CLayoutElementNoneCustomizer : CLayoutElementCustomizer {
		public void Customize(GameObject u) => u.DestroyComponentIfExists<LayoutElement>();
		public void CustomizeFresh(GameObject u) {}
	}


	public class CLayoutElementBasicCustomizer : CLayoutElementCustomizer {
		public readonly float FlexWidth, FlexHeight;

		public CLayoutElementBasicCustomizer(float flexWidth, float flexHeight) {
			FlexWidth = flexWidth;
			FlexHeight = flexHeight;
		}
		public void Customize(GameObject u) {
			var le = u.GetOrCreateComponent<LayoutElement>();
			le.flexibleWidth = FlexWidth;
			le.flexibleHeight = FlexHeight;
		}
	}
}