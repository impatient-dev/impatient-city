using imp.util;
using UnityEngine;

namespace imp.city.ui {
	public interface CRectCustomizer {
		/**Sets anchors, offsets, etc.
		 * Warning: the Unity UI system is a massive jerk and tends to overwrite your offsets with random garbage when you do anything.
		 * So customizing the RectOffset (which includes setting offsets) needs to be done last.
		 * In particular, offsets will be overwritten when an image or layout group is added, or when a parent transform is set.*/
		public void Customize(RectTransform rect);
	}


	public class CRCAnchors : CRectCustomizer {
		public static readonly CRCAnchors Full = new(x0: 0, y0: 0, x1: 1, y1: 1);
		public readonly float X0, Y0, X1, Y1;

		public CRCAnchors(float x0, float y0, float x1, float y1) {
			X0 = x0;
			Y0 = y0;
			X1 = x1;
			Y1 = y1;
		}
		public void Customize(RectTransform rect) {
			rect.Anchors(x0: X0, y0: Y0, x1: X1, y1: Y1).NoOffsets();
		}
	}
}