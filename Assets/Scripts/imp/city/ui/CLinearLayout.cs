using System;
using System.Collections.Generic;
using imp.util;
using UnityEngine;
using UnityEngine.UI;

namespace imp.city.ui {
	/**A container with a horizontal or vertical layout group.*/
	public class CLinearLayout : CParent, CChild, CActive, CBackground {
		public GameObject U { get; }
		public readonly HorizontalOrVerticalLayoutGroup Layout;
		private CParent Parent;
		private List<CChild> Children = new();
		
		public CPosType PosType => CPosType.Layout;
		public bool HasLayout => true;
		public string Name => U.name;
		public bool Active {
			get => U.activeSelf;
			set => U.SetActive(value);
		}
		
		private CLinearLayout(CParent parent, GameObject u, HorizontalOrVerticalLayoutGroup layout) {
			Parent = parent;
			U = u;
			Layout = layout;
			Parent.Add(this);
		}
		
		public override string ToString() => $"LinearLayout({U.name})";

		public void Add(CChild child) {
			child.U.transform.SetParent(U.transform, worldPositionStays: true);
			Children.Add(child);
		}

		public void PreDestroy(CChild child) => Children.Remove(child);

		public void Destroy() {
			Parent.PreDestroy(this);
			foreach(var child in Children)
				child.DestroyWithParent();
			U.Destroy();
		}

		public void DestroyWithParent() {
			foreach(var child in Children)
				child.DestroyWithParent();
		}

		public void DestroyFromParent() {
			foreach(var child in Children)
				child.DestroyWithParent();
			U.Destroy();
		}

		public void Clear() {
			foreach(var child in Children)
				child.DestroyFromParent();
			Children.Clear();
		}

		public Image? BackImage => U.GetComponent<Image>()?.DisabledToNull();
		public Image GetOrCreateBackImage() => U.ProvideEnabledImage();
		public void ClearBackImage() => U.GetComponent<Image>()?.Enabled(false);


		/**Creates a row or column as a child of a parent that also has a layout.*/
		public static CLinearLayout UnderLayout<L>(CParent parent, string name, CLayoutElementCustomizer elementCustomizer, CLinearLayoutCustomizer layoutCustomizer) where L : HorizontalOrVerticalLayoutGroup {
			if(!parent.HasLayout)
				throw new Exception($"Parent {parent} has no layout group, so child {name} must customize its RectTransform.");
			var u = new GameObject(name);
			elementCustomizer.Customize(u);
			var layout = u.AR<L>();
			layoutCustomizer.Customize(layout);
			return new CLinearLayout(parent, u, layout);
		}

		/**Creates a row or column as a child of a parent that does not have a layout.*/
		public static CLinearLayout WithRect<L>(CParent parent, string name, CRectCustomizer rect, CLinearLayoutCustomizer layoutCustomizer) where L : HorizontalOrVerticalLayoutGroup {
			if(parent.HasLayout)
				throw new Exception($"Parent has a layout group, so child {name} cannot customize its RectTransform.");
			var u = new GameObject(name);
			rect.Customize(u.AR<RectTransform>());
			var layout = u.AR<L>();
			layoutCustomizer.Customize(layout);
			return new CLinearLayout(parent, u, layout);
		}
	}
}