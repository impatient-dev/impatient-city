using imp.util;
using UnityEngine;
using UnityEngine.UI;

namespace imp.city.ui {
	public interface CLinearLayoutCustomizer {
		public void Customize(HorizontalOrVerticalLayoutGroup layout);
	}


	public class CLinearLayoutAlignmentCustomizer : CLinearLayoutCustomizer {
		private readonly TextAnchor Align;

		public CLinearLayoutAlignmentCustomizer(TextAnchor align) { Align = align; }

		public void Customize(HorizontalOrVerticalLayoutGroup layout) => layout.Set(Align);
	}



	public class CLinearLayoutAlignmentPaddedCustomizer : CLinearLayoutCustomizer {
		private readonly TextAnchor Align;
		private readonly RectOffset Padding;

		public CLinearLayoutAlignmentPaddedCustomizer(TextAnchor align, RectOffset padding) {
			Align = align;
			Padding = padding;
		}

		public void Customize(HorizontalOrVerticalLayoutGroup layout) => layout.Set(Align).padding = Padding;
	}
}