using imp.city.r;
using imp.util;
using TMPro;
using UnityEngine.Events;
using UnityEngine.UI;

namespace imp.city.ui {
	public class CLink : CAbstractChild, CActive, CDisable {
		private readonly TextMeshProUGUI Pro;
		private readonly Button Btn;
		private readonly LinkStyle Style;

		public CLink(CParent parent, string name, CLayoutElementCustomizer elementCustom, LinkStyle style, UnityAction onClick, string text) : base(parent, name, CPosType.Layout) {
			Style = style;
			
			elementCustom.CustomizeFresh(U);
			Btn = U.AR<Button>();
			Btn.onClick.AddListener(onClick);

			Pro = U.AR<TextMeshProUGUI>();
			Pro.material = style.R.Mat.Color.Mat;
			Pro.fontSize = style.Size;
			Pro.text = text;
			_setAppearance(true);
		}

		public bool Active { set => U.SetActive(value); }

		public bool Enabled {
			set {
				Btn.enabled = value;
				_setAppearance(value);
			}
		}

		private void _setAppearance(bool enabled) {
			if(enabled) {
				Pro.font = Style.TmpFont;
				Pro.color = Style.Color;
				Pro.fontStyle = Style.TmpStyle;
			} else {
				Pro.font = Style.TmpFontDisabled;
				Pro.color = Style.ColorDisabled;
				Pro.fontStyle = Style.TmpStyleDisabled;
			}
		}

		public CLink Text(string str) {
			Pro.text = str;
			return this;
		}
	}
}