using imp.city.destroy;
using UnityEngine;

namespace imp.city.ui {
	public interface CObj {
		/**Describes how this element is positioned inside its parent.
		 * (Not how any of our child objects are positioned.)*/
		public CPosType PosType { get; }
	}

	/**A canvas element wrapper that provides access to the raw GameObject. If possible, you should use methods or properties instead of accessing this directly.*/
	public interface ICRaw : CObj {
		public GameObject U { get; }
	}

	/**Describes how a Canvas UI element is positioned (and sized).*/
	public enum CPosType {
		/**The position and size are set by directly manipulating the RectTransform.
		 * This could mean that we're filling some fraction of the parent (or screen if there's no parent),
		 * or it could mean manual pixel sizes and positions are used.*/
		Rect = 0,
		/**A parent layout group controls our size and position; we just need to have component(s) that provide min/preferred/flexible sizes.*/
		Layout = 1,
	}



	/**A Canvas GameObject wrapper that can have arbitrary child objects added and removed.*/
	public interface CParent : CObj {
		/**Whether this parent has a layout group applied.
		 * If it does, it will position children without further input from them; if it doesn't, children must customize their own RectTransform's.*/
		public bool HasLayout { get; }
		public string Name { get; }
		/**Adds a child to this parent.
		 * The parent should update the child's transform to attach it to the parent.
		 * The parent will be responsible for notifying the child when the parent is destroyed, in case the child needs to clean up resources.*/
		public void Add(CChild child);
		/**Notifies the parent that a child is about to be destroyed.
		 * This must be called when a child is being Destroy'd, and must not be called for the other destroy variants where the parent is already aware of the destruction
		 * (to avoid concurrent modifications to the parent's list of children).*/
		public void PreDestroy(CChild child);
		/**Destroys all children.*/
		public void Clear();
	}


	/**A Canvas GameObject wrapper that does not exist at the top level, but is instead the child of some other UI object.*/
	public interface CChild : CObj {
		/**The parent needs access to this in order to attach us as a child.
		 * In general, other code should prefer to use other methods instead of accessing this directly.*/
		public GameObject U { get; }
		/**Tells the child that its parent has asked it to destroy itself. The child must not notify the parent about the destruction.*/
		public void DestroyFromParent();
		/**Tells the child that its parent is being destroyed.
		 * The child must not notify the parent about this, and may not need to do anything at all if there are no resources that need to be cleaned up.*/
		public void DestroyWithParent();
	}


	public interface COnDestroy : CObj {
		/**Notifies this Canvas object wrapper that when it is destroyed, the provided object must also be destroyed.
		 * This cannot be called a 2nd time, unless the wrapper provides a way to reset this (cells do provide Reset methods).*/
		public void SetOnDestroy(Destroyable d);
	}


	public interface CActive : CObj {
		/**An inactive object is invisible and does not take up any space in the UI.*/
		bool Active { set; }
	}

	public interface CDisable : CObj {
		/**A disabled object cannot be interacted with and displays a different visual style.*/
		bool Enabled { set; }
	}
}