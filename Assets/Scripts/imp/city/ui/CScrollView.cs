#nullable enable

using System;
using imp.util;
using UnityEngine;
using UnityEngine.UI;
using static imp.util.ImpatientUtil;

namespace imp.city.ui {

	public readonly struct CScrollSettings {
		public readonly Bar Horizontal, Vertical;

		public CScrollSettings(Bar horizontal, Bar vertical) {
			Horizontal = horizontal;
			Vertical = vertical;
		}

		public enum Bar {
			never = 0,
			auto = 1,
			always = 2,
		}
	}
	
	
	/**A UI component that manages a ScrollView and horizontal and/or vertical scrollbars.
	 * This view must have exactly one child.
	 * This object will invasively manage the child's layout, manipulating the child's RectTransform and adding a ContentSizeFitter.
	 * However, the child is allowed and encouraged to have a layout group applied.
	 * The parent of this scrollview should probably be a cell, with no layout group. <br/><br/>
	 *
	 * The recommended way to use this component is to enable the vertical scrollbar only, and make the child a top-aligned column.
	 * Then this component will make its child fill the available space horizontally.
	 * (By using ContentSizeFitter "unconstrained", which isn't documented to work this way but apparently does.)
	 * If you enable both the horizontal and vertical scrollbars, the width and height of the child will be set to preferred.
	 * It will not fill the parent scroll view, and anything centered in the child will not be centered in the parent. <br/><br/>
	 * 
	 * Implementation note: the Unity ScrollView is very tricky to create programmatically.
	 * It took a lot of experimenting, and comparing my programmatic view to a working view generated in the editor, to figure out what seems to be necessary.
	 * Sometimes it's unclear why particular steps are needed.*/
	public class CScrollView : CAbstractChild, CParent {
		private readonly CScrollSettings _settings;
		private readonly ScrollRect _scrollRect;
		private readonly GameObject _viewport;
		private CChild? _content;
		
		public string Name => U.name;
		public bool HasLayout => true; // tell the child it doesn't need to (and shouldn't) customize the RectTransform

		public CScrollView(CParent parent, CRectCustomizer rect, in CScrollSettings settings) : this(parent, "Scroll View", rect, settings) {}

		public CScrollView(CParent parent, string name, CRectCustomizer rect, in CScrollSettings settings) : base(parent, name, CPosType.Rect) {
			_settings = settings;
			var rt = U.AR<RectTransform>();
			_scrollRect = U.AR<ScrollRect>();
			_scrollRect.movementType = ScrollRect.MovementType.Clamped;
			_scrollRect.inertia = false;
			_scrollRect.scrollSensitivity = 1.5f;
			
			_viewport = U.AddChild("Viewport");
			_viewport.AR<Image>();
			_viewport.AR<Mask>().showMaskGraphic = false; // don't show whatever default color is in the viewport Image
			_scrollRect.viewport = _viewport.GetOrCreateComponent<RectTransform>().Anchors(x0: 0, y0: 0, x1: 1, y1: 1).NoOffsets();
			// content is set later, when a child is added

			_scrollRect.horizontal = settings.Horizontal != CScrollSettings.Bar.never;
			_scrollRect.horizontalScrollbar = CreateScrollbar(U, Scrollbar.Direction.LeftToRight);
			_scrollRect.horizontalScrollbarVisibility = ToVisibility(settings.Horizontal);

			_scrollRect.vertical = settings.Vertical != CScrollSettings.Bar.never;
			_scrollRect.verticalScrollbar = CreateScrollbar(U, Scrollbar.Direction.BottomToTop);
			_scrollRect.verticalScrollbarVisibility = ToVisibility(settings.Vertical);
			
			rect.Customize(rt);
		}

		public override string ToString() => $"ScrollView({U.name} content={_content is not null})";

		public override void Destroy() {
			_content?.DestroyWithParent();
			_content = null;
			base.Destroy();
		}
		public override void DestroyWithParent() {
			_content?.DestroyWithParent();
			_content = null;
			base.DestroyWithParent();
		}
		public override void DestroyFromParent() {
			_content?.DestroyWithParent();
			_content = null;
			base.DestroyFromParent();
		}

		public void Clear() {
			// _scrollRect.content = null; // compiler warning about nullability
			_content?.DestroyFromParent();
			_content = null;
		}

		public void Add(CChild child) {
			if(_content != null)
				throw new Exception($"Scroll view '{Name}' already has a child: {_content}");
			child.U.transform.SetParent(_viewport.transform, worldPositionStays: true);
			var rect = child.U.GetOrCreateComponent<RectTransform>();
			_scrollRect.content = rect;
			child.U.GetOrCreateComponent<ContentSizeFitter>().Set(horizontal: ToFitMode(_settings.Horizontal), vertical: ToFitMode(_settings.Vertical));
			rect.Pivot(0, 1) // I have no idea why this is necessary
				.Anchors(x0: 0, x1: 1, y0: 0, y1: 1).NoOffsets();
			_content = child;
		}

		public void PreDestroy(CChild child) {
			if(child != _content)
				throw new Exception($"Wrong child is being destroyed: {child} instead of {_content}.");
			child.U.transform.SetParent(null);
			_content = null;
		}



		private static Scrollbar CreateScrollbar(GameObject scrollView, Scrollbar.Direction direction) {
			bool vertical = IsVertical(direction);
			var track = scrollView.AddChild($"Scrollbar " + (vertical ? "Vertical" : "Horizontal"));
			track.AR<Image>().color = Color.white;
			var scrollbar = track.AR<Scrollbar>();
			scrollbar.direction = direction;

			var handle = track.AddChild("Handle");
			handle.AR<Image>(); // should be managed by the ColorBlock, I think

			var handleColors = new ColorBlock(); // TODO
			handleColors.normalColor = Color.black;
			handleColors.selectedColor = Color.blue;
			handleColors.highlightedColor = Color.yellow;
			handleColors.pressedColor = Color.red;
			handleColors.disabledColor = Color.magenta;
			
			// Y and height don't matter for vertical, and X and width don't matter for horizontal - they're controlled by the Scrollbar component
			int p = vertical ? 1 : 0;
			track.GetOrCreateComponent<RectTransform>().Anchors(x0: 1, x1: 1, y0: 0, y1: 0).Pivot(p, p).NoOffsets().Size(20, 20);
			
			scrollbar.handleRect = handle.GetOrCreateComponent<RectTransform>().NoOffsets();
			scrollbar.transition = Selectable.Transition.ColorTint;
			scrollbar.colors = handleColors;

			return scrollbar;
		}


		/**If the scrollbar for this direction isn't going to be enabled, we'll use the fit mode Unconstrained, which seems to actually keep the content in the bounds of the scrollview.*/
		private static ContentSizeFitter.FitMode ToFitMode(CScrollSettings.Bar bar) =>
			bar == CScrollSettings.Bar.never ? ContentSizeFitter.FitMode.Unconstrained : ContentSizeFitter.FitMode.PreferredSize;
		
		private static ScrollRect.ScrollbarVisibility ToVisibility(CScrollSettings.Bar bar) {
			switch(bar) {
				case CScrollSettings.Bar.always:
					return ScrollRect.ScrollbarVisibility.Permanent;
				case CScrollSettings.Bar.auto:
				case CScrollSettings.Bar.never:
					return ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;
				default:
					return UnexpectedR<ScrollRect.ScrollbarVisibility>($"Unexpected scrollbar enum: {bar}");
			}
		}
		
		private static bool IsVertical(Scrollbar.Direction dir) {
			switch(dir) {
				case Scrollbar.Direction.BottomToTop:
				case Scrollbar.Direction.TopToBottom:
					return true;
				case Scrollbar.Direction.LeftToRight:
					case Scrollbar.Direction.RightToLeft:
					return false;
				default:
					return UnexpectedR<bool>($"Unexpected scrollbar direction: {dir}");
			}
		}
	}
}