using imp.city.r;
using imp.util;
using TMPro;

namespace imp.city.ui {
	/**Text, implemented with TextMeshPro.*/
	public class CText : CAbstractChild, CActive {
		private readonly TextMeshProUGUI Pro;

		public CText(CParent parent, string name, CLayoutElementCustomizer elementCustom, TxtStyle style, string text) : base(parent, name, CPosType.Layout) {
			elementCustom.CustomizeFresh(U);
			Pro = U.AR<TextMeshProUGUI>();
			Style(style).Text(text);
		}

		public bool Active { set => U.SetActive(value); }

		public CText Style(TxtStyle style) {
			Pro.material = style.R.Mat.Color.Mat;
			Pro.font = style.TmpFont;
			Pro.color = style.Color;
			Pro.fontSize = style.Size;
			Pro.fontStyle = style.TmpStyle;
			return this;
		}
		
		public CText Text(string str) {
			Pro.text = str;
			return this;
		}
		public string text => Pro.text;
		
		public CText Centered() {
			Pro.alignment = TextAlignmentOptions.Center;
			return this;
		}
		public CText Left() {
			Pro.alignment = TextAlignmentOptions.Left;
			return this;
		}
		public CText Right() {
			Pro.alignment = TextAlignmentOptions.Right;
			return this;
		}
	}
}