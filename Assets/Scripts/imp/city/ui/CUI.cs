using imp.city.destroy;
using imp.city.r;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace imp.city.ui {
	/**Creates and customizes canvas UI elements.
	 * In general, when creating a child, either the RectTransform is customized (if the parent has no layout),
	 * or layout element customization is used (if the parent has a layout), but not both.*/
	public static class CUI {
		/**Creates a cell container. The parent must not have a layout.*/
		public static CCell CCell(this CParent parent, string name, CRectCustomizer rect) =>
			new(parent, name, rect);
		/**Creates a cell container that takes up part of its parent. The X and Y coordinates are in [0,1]. The parent must not have a layout.*/
		public static CCell CCellRect(this CParent parent, string name, float x0, float y0, float x1, float y1) =>
			new(parent, name, new CRCAnchors(x0: x0, y0: y0, x1: x1, y1: y1));

		/**Creates a scroll view that uses RectTransform anchors to fill the entire parent. The parent must not have a layout.*/
		public static CScrollView CScrollFull(this CParent parent, string name, in CScrollSettings settings) =>
			new(parent, name, CRCAnchors.Full, in settings);
		/**Creates a scroll view that uses RectTransform anchors to fill the entire parent. The parent must not have a layout.*/
		public static CScrollView CScrollFull(this CParent parent, in CScrollSettings settings) =>
			new(parent, "Scroll View", CRCAnchors.Full, in settings);

		/**Creates an object with a linear layout (row/column) that is positioned via RectTransform (the parent must have no layout).*/
		public static CLinearLayout CLinear<L>(this CParent parent, string name, CRectCustomizer rect, CLinearLayoutCustomizer layout) where L : HorizontalOrVerticalLayoutGroup =>
			CLinearLayout.WithRect<L>(parent, name, rect, layout);
		/**Creates an object with a linear layout (row/column) that is the child of a parent with a layout.*/
		public static CLinearLayout CLinear<L>(this CParent parent, string name, CLayoutElementCustomizer elementCustomizer, CLinearLayoutCustomizer layoutCustomizer) where L : HorizontalOrVerticalLayoutGroup =>
			CLinearLayout.UnderLayout<L>(parent, name, elementCustomizer, layoutCustomizer);
		
		/**Creates an object with a VerticalLayoutGroup that is the child of a parent with a layout.*/
		public static CLinearLayout CCol(this CParent parent, string name, CLayoutElementCustomizer elementCustom, CLinearLayoutCustomizer layoutCustom) =>
			CLinear<VerticalLayoutGroup>(parent, name, elementCustom, layoutCustom);

		/**Creates a column (VerticalLayoutGroup) that aligns its content top-center. The parent must have a layout.*/
		public static CLinearLayout CColC(this CParent parent, string name = "Col") =>
			CLinearLayout.UnderLayout<VerticalLayoutGroup>(parent, name, CLeNone, CLinearTopCenter);
		/**Creates a column (VerticalLayoutGroup) that aligns its content top-center. The parent must NOT have a layout.*/
		public static CLinearLayout CColC(this CParent parent, string name, CRectCustomizer rect) =>
			CLinearLayout.WithRect<VerticalLayoutGroup>(parent, name, rect, CLinearTopCenter);

		/**Creates a column (VerticalLayoutGroup) that aligns its content top-left. The parent must have a layout.*/
		public static CLinearLayout CColL(this CParent parent, string name) =>
			CLinearLayout.UnderLayout<VerticalLayoutGroup>(parent, name, CLeNone, CLinearTopLeft);
		/**Creates a column (VerticalLayoutGroup) that aligns its content top-left. The parent must NOT have a layout.*/
		public static CLinearLayout CColL(this CParent parent, string name, CRectCustomizer rect) =>
			CLinearLayout.WithRect<VerticalLayoutGroup>(parent, name, rect, CLinearTopLeft);

		/**Creates a column (VerticalLayoutGroup) that aligns its content top-right. The parent must have a layout.*/
		public static CLinearLayout CColR(this CParent parent, string name) =>
			CLinearLayout.UnderLayout<VerticalLayoutGroup>(parent, name, CLeNone, CLinearTopRight);
		/**Creates a column (VerticalLayoutGroup) that aligns its content top-right. The parent must NOT have a layout.*/
		public static CLinearLayout CColR(this CParent parent, string name, CRectCustomizer rect) =>
			CLinearLayout.WithRect<VerticalLayoutGroup>(parent, name, rect, CLinearTopRight);
		
		/**Creates a column that aligns its content top-center and has a flexible width.
		 * Any parent with a layout group will be forced to expand.*/
		public static CLinearLayout CColWC(this CParent parent, string name = "Col") =>
			CLinearLayout.UnderLayout<VerticalLayoutGroup>(parent, name, CLeFlexW, CLinearTopCenter);
		/**Creates a column that aligns its content top-left and has a flexible width.
		 * Any parent with a layout group will be forced to expand.*/
		public static CLinearLayout CColWL(this CParent parent, string name = "Col") =>
			CLinearLayout.UnderLayout<VerticalLayoutGroup>(parent, name, CLeFlexW, CLinearTopLeft);

		/**Creates a row that centers its content.*/
		public static CLinearLayout CRow(this CParent parent, string name) =>
			CLinearLayout.UnderLayout<HorizontalLayoutGroup>(parent, name, CLeNone, CLinearCenter);

		/**Creates a row that left-aligns its content and has a flexible width. Any parents with layout groups will be forced to expand.*/
		public static CLinearLayout CRowWL(this CParent parent, string name) =>
			CLinearLayout.UnderLayout<HorizontalLayoutGroup>(parent, name, CLeFlexW, CLinearLeft);


		public static CText CText(this CParent parent, string name, CLayoutElementCustomizer elementCustom, TxtStyle style, string text = "") =>
			new(parent, name, elementCustom, style, text);
		public static CText CText(this CParent parent, string name, TxtStyle style, string text = "") =>
			parent.CText(name, CLeNone, style, text);

		public static CLink CLink(this CParent parent, string name, CLayoutElementCustomizer elementCustom, LinkStyle style, UnityAction onClick, string text = "") =>
			new(parent, name, elementCustom, style, onClick, text);
		public static CLink CLink(this CParent parent, string name, LinkStyle style, UnityAction onClick, string text = "") =>
			parent.CLink(name, CLeNone, style, onClick, text);

		public static CBtn CBtn(this CParent parent, string name, BtnStyle style, UnityAction onClick, string text = "") =>
			new(parent, name, style, onClick, text);




		// CUSTOMIZE EXISTING OBJECTS
		
		/**Marks this object active or inactive. Inactive objects are invisible and take no space in the layout.*/
		public static C Active<C>(this C obj, bool active = true) where C : CActive {
			obj.Active = active;
			return obj;
		}
		/**Marks this object inactive. Inactive objects are invisible and take no space in the layout.*/
		public static C Inactive<C>(this C obj) where C : CActive {
			obj.Active = false;
			return obj;
		}

		/**Marks this object enabled or disabled. Disabled objects are still visible, but they can't be interacted with.*/
		public static C Enabled<C>(this C obj, bool enabled = true) where C : CDisable {
			obj.Enabled = enabled;
			return obj;
		}
		/**Marks this object disabled. Disabled objects are still visible, but they can't be interacted with.*/
		public static C Disabled<C>(this C obj) where C : CDisable => obj.Enabled(false);

		/**Destroys all children.*/
		public static C Cleared<C>(this C parent) where C : CParent {
			parent.Clear();
			return parent;
		}

		/**Tells the canvas element wrapper that when it is destroyed, this object should also be.
		 * This cannot be called twice (unless the wrapper provides a way to reset this, which cells do).*/
		public static C OnDestroy<C>(this C obj, Destroyable d) where C : COnDestroy {
			obj.SetOnDestroy(d);
			return obj;
		}

		/**Sets the background color.*/
		public static C Back<C>(this C element, Color color) where C : CBackground {
			element.GetOrCreateBackImage().color = color;
			return element;
		}
		/**Removes the Image component for the background, if it exists.*/
		public static C ClearBack<C>(this C element) where C : CBackground {
			element.ClearBackImage();
			return element;
		}
		
		public static CCell AsCol(this CCell cell, CLinearLayoutCustomizer custom) {
			cell.ApplyLayout<VerticalLayoutGroup>(custom);
			return cell;
		}
		/**Makes the cell a column aligned to the top-center.*/
		public static CCell AsColC(this CCell cell) => cell.AsCol(CLinearTopCenter);
		/**Makes the cell a column aligned to the top-left.*/
		public static CCell AsColL(this CCell cell) => cell.AsCol(CLinearTopLeft);
		/**Makes the cell a column aligned to the top-right.*/
		public static CCell AsColR(this CCell cell) => cell.AsCol(CLinearTopRight);
		
		public static CCell AsRow(this CCell cell, CLinearLayoutCustomizer custom) {
			cell.ApplyLayout<HorizontalLayoutGroup>(custom);
			return cell;
		}
		/**Makes the cell a row aligned to the center.*/
		public static CCell AsRowC(this CCell cell) => cell.AsRow(CLinearCenter);
		/**Makes the cell a row aligned to the center-left.*/
		public static CCell AsRowL(this CCell cell) => cell.AsRow(CLinearLeft);
		/**Makes the cell a row aligned to the center-right.*/
		public static CCell AsRowR(this CCell cell) => cell.AsRow(CLinearTopRight);
		
		
		
		// LAYOUT CUSTOMIZERS

		public static readonly CLinearLayoutCustomizer CLinearCenter = new CLinearLayoutAlignmentCustomizer(TextAnchor.MiddleCenter);
		public static readonly CLinearLayoutCustomizer CLinearLeft = new CLinearLayoutAlignmentCustomizer(TextAnchor.MiddleLeft);
		public static readonly CLinearLayoutCustomizer CLinearTopCenter = new CLinearLayoutAlignmentCustomizer(TextAnchor.UpperCenter);
		public static readonly CLinearLayoutCustomizer CLinearTopLeft = new CLinearLayoutAlignmentCustomizer(TextAnchor.UpperLeft);
		public static readonly CLinearLayoutCustomizer CLinearTopRight = new CLinearLayoutAlignmentCustomizer(TextAnchor.UpperRight);
		public static readonly CLinearLayoutCustomizer CLinearBottomCenter = new CLinearLayoutAlignmentCustomizer(TextAnchor.LowerCenter);
		public static readonly CLinearLayoutCustomizer CLinearBottomLeft = new CLinearLayoutAlignmentCustomizer(TextAnchor.LowerLeft);
		public static readonly CLinearLayoutCustomizer CLinearBottomRight = new CLinearLayoutAlignmentCustomizer(TextAnchor.LowerRight);

		public static readonly CLayoutElementCustomizer CLeNone = new CLayoutElementNoneCustomizer();
		public static readonly CLayoutElementCustomizer CLeFlexW = new CLayoutElementBasicCustomizer(1, 0);

		/**Scroll horizontally and/or vertically; hide scrollbars when not needed.
		 * See the warning on CScrollView for why this configuration may not work well.*/
		public static readonly CScrollSettings CScrollAuto = new(horizontal: CScrollSettings.Bar.auto, vertical: CScrollSettings.Bar.auto);
		/**Scroll vertically only; hide scrollbar when not needed.
		 * Works well with a top-aligned column.*/
		public static readonly CScrollSettings CScrollVertical = new(horizontal: CScrollSettings.Bar.never, vertical: CScrollSettings.Bar.auto);
	}
}