using System;
using imp.city.destroy;
using imp.city.u;

namespace imp.city.ui {
	/**Displays a GUI (an overlay canvas) giving info and controls for the world. Currently it shows a sidebar and controls for the currently-controlled train.*/
	public class WorldUi : Destroyable {
		private readonly WorldView View;

		private readonly CCanvas Canvas;
		private readonly CCell _control, _sidebar;

		public WorldUi(WorldView view) {
			View = view;
			Canvas = new CCanvas("World UI", CRCAnchors.Full);
			_control = Canvas.CCellRect("Controls", x0: 0, x1: 0.3f, y0: 0, y1: 0.3f);
			_sidebar = Canvas.CCellRect("Sidebar", x0: 0.7f, x1: 1, y0: 0, y1: 1);
		}

		public void Destroy() => Canvas.Destroy();

		public void RemoveSidebar() {
			_sidebar.ResetExceptLayout();
		}

		/**The GameObject arg is the parent that the sidebar should be created under.*/
		public void ShowSidebar(Action<CCell> f) {
			_sidebar.ResetExceptLayout();
			f.Invoke(_sidebar);
		}

		public void RemoveControls() => _control.ResetExceptLayout();
		
		public void ShowControls(Action<CCell> f) {
			_control.ResetExceptLayout();
			f.Invoke(_control);
		}
	}
}