using imp.city.destroy;
using imp.city.r;
using imp.city.u;
using imp.city.wl;
using imp.city.world.rail;
using imp.city.world.rail.ctrl;
using imp.city.wu;
using UnityEngine;
using static imp.city.ui.CUI;
using static imp.util.ImpatientUtil;

namespace imp.city.ui.controls {

	public class TrainControlUi : DestroyableList, TrainControlsListener {
		private readonly WorldView View;
		private NotchedTrainControls Ctrl;
		private readonly TrainCar Car;

		private readonly CBtn bReverserRev, bReverserNeutral, bReverserFwd;
		private readonly CText tThrottleNotch;
		private readonly CBtn bThrottle0, bThrottle1, bThrottleUp, bThrottleDown;
		private readonly CText tFastBrake;
		private readonly CBtn bFastBrake0, bFastBrake1, bFastBrakeUp, bFastBrakeDown;
		private readonly CLinearLayout _throttleRow;

		public TrainControlUi(CCell cell, WorldView view, NotchedTrainControls controls) {
			View = view;
			Ctrl = controls;
			Car = Ctrl.FromCar!;

			var style = view.R.Style;
			var root = cell.OnDestroy(this).AsRow(CLinearBottomLeft).CColC("Train Controls").Back(View.R.Color.UiPartialBack);

			var reverserRow = root.CRow("Reverser");
			bReverserRev = CreateReverserButton(style, reverserRow, "R", TrainReverserState.Reverse);
			bReverserNeutral = CreateReverserButton(style, reverserRow, "N", TrainReverserState.Neutral);
			bReverserFwd = CreateReverserButton(style, reverserRow, "F", TrainReverserState.Forward);

			_throttleRow = root.CRow("Throttle");
			_throttleRow.Active(Ctrl.Reverser is TrainReverserState.Forward or TrainReverserState.Reverse);
			_throttleRow.CText("Label", style.Label, "throttle");
			bThrottle0 = _throttleRow.CBtn("0", style.Btn, () => SetThrottleNotch(0), "0");
			bThrottleDown = _throttleRow.CBtn("-", style.Btn, () => AddThrottleNotch(-1), "-");
			bThrottleUp = _throttleRow.CBtn("+", style.Btn, () => AddThrottleNotch(1), "+");
			bThrottle1 = _throttleRow.CBtn("M", style.Btn, () => SetThrottleNotch(Ctrl.MaxThrottleNotch), "M");
			tThrottleNotch = _throttleRow.CText("Text", style.Body);

			var brakeRow = root.CRow("Brakes");
			brakeRow.CText("Label", style.Label, "brake");
			bFastBrake0 = brakeRow.CBtn("0", style.Btn, () => SetBrake(0), "0");
			bFastBrakeDown = brakeRow.CBtn("-", style.Btn, () => AddBrake(-0.1f), "-");
			bFastBrakeUp = brakeRow.CBtn("+", style.Btn, () => AddBrake(0.1f), "+");
			bFastBrake1 = brakeRow.CBtn("M", style.Btn, () => SetBrake(1), "M");
			tFastBrake = brakeRow.CText("Text", style.Body);
			
			_onTrainControlInputChange(Ctrl.FromCar.Train);
			Register(view.L.Trains.TrainControls, this);
		}

		public void _onTrainControlInputChange(Train train) {
			if(train != Ctrl.FromCar.Train)
				return;

			bReverserRev.Enabled(Ctrl.Reverser != TrainReverserState.Reverse);
			bReverserNeutral.Enabled(Ctrl.Reverser != TrainReverserState.Neutral);
			bReverserFwd.Enabled(Ctrl.Reverser != TrainReverserState.Forward);

			if(Ctrl.Reverser != TrainReverserState.Forward && Ctrl.Reverser != TrainReverserState.Reverse) {
				_throttleRow.Active(false);
			} else {
				_throttleRow.Active(true);
				bThrottle0.Enabled(Ctrl.ThrottleNotch > 0);
				bThrottleDown.Enabled(Ctrl.ThrottleNotch > 0);
				bThrottleUp.Enabled(Ctrl.ThrottleNotch < Ctrl.MaxThrottleNotch);
				bThrottle1.Enabled(Ctrl.ThrottleNotch < Ctrl.MaxThrottleNotch);
				tThrottleNotch.Text($"Notch {Ctrl.ThrottleNotch}");
			}

			bFastBrake0.Enabled(Ctrl.FastBrake > 0);
			bFastBrakeDown.Enabled(Ctrl.FastBrake > 0);
			bFastBrake1.Enabled(Ctrl.FastBrake < 1);
			bFastBrakeUp.Enabled(Ctrl.FastBrake < 1);
			tFastBrake.Text(FmtPercent(Ctrl.FastBrake));
		}

		public void _onTrainControlsReplaced(Train train) {}

		private void SetThrottleNotch(int notch) {
			if(notch == Ctrl.ThrottleNotch || notch < 0 || notch > Ctrl.MaxThrottleNotch)
				return;
			WUTrainControls.SetThrottleNotch(View.W, View.L, Ctrl, notch);
		}

		private void AddThrottleNotch(int delta) => SetThrottleNotch(Ctrl.ThrottleNotch + delta);

		private void SetBrake(float brake) {
			var b = Mathf.Clamp01(brake);
			if(Ctrl.FastBrake == b)
				return;
			WUTrainControls.SetFastBrake(View.W, View.L, Ctrl, b);
		}

		private void AddBrake(float delta) => SetBrake(Ctrl.FastBrake + delta);


		private CBtn CreateReverserButton(AppStyles style, CParent parent, string str, TrainReverserState reverser) =>
			parent.CBtn(str, style.Btn, () => WUTrainControls.SetReverser(View.W, View.L, Ctrl, reverser), str);
	}
}