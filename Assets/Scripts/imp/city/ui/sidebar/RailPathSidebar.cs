using imp.city.u;
using imp.city.world.rail;
using static imp.city.Main;

namespace imp.city.ui.sidebar {
	public class RailPathSidebar {
		private readonly RailPath Path;
		private readonly WorldView View;

		public RailPathSidebar(CCell cell, WorldView view, RailPath path) {
			Path = path;
			View = view;
			var style = R.Style;

			var root = cell.AppSidebarV();
			root.CText("Title", style.Title, Title(path));
			root.CText("Length", style.Body, $"{path.Geom.Length} m");

			if(path.Geom.Horizontal.IsCurved) {
				var row = root.CRowWL("Curve Radius");
				row.CText("Label", style.Label, "Radius: ");
				row.CText("Radius", style.Body, $"{path.Geom.Horizontal.MinRadius} m");
			}
		}

		private static string Title(RailPath path) =>
			path.Geom.Horizontal.IsCurved ? "Curved Track" : "Straight Track";
	}
}