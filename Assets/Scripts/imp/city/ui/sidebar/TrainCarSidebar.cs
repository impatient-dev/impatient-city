using System;
using imp.city.destroy;
using imp.city.u;
using imp.city.wl;
using imp.city.world.rail;
using imp.city.world.rail.cars;
using imp.city.wu;
using static imp.util.ImpatientUtil;

namespace imp.city.ui.sidebar {
	public class TrainCarSidebar : Destroyable, TrainsListener {
		private readonly TrainCar Car;
		private readonly WorldView View;
		private readonly CText tUnderControl;
		private readonly CBtn bControl;
		private readonly CLink lFront, lRear;
		private readonly CBtn bDecoupleFront, bDecoupleRear;
		private readonly CarControlHandler _controlHandler;

		public TrainCarSidebar(CCell cell, WorldView view, TrainCar car) {
			Car = car;
			View = view;
			var style = view.R.Style;

			var root = cell.OnDestroy(this).AppSidebarV();
			root.CText("Label" ,style.Label, "train car");
			root.CText("Title", style.Title, $"#{car.IdxInTrain + 1}: {Description(car.Part.Design)}");
			root.CLink("Train", style.Link, () => view.Selection.SetSelection(car.Train),
				$"Train: {car.Train.Parts.Cars} cars, {car.Train.Performance.Length} m");
			tUnderControl = root.CText("Under Control", style.BodyBold, "Under Your Control");
			bControl = root.CBtn("Control Button", style.Btn, _toggleControl);

			root.CText("Front Label", style.Label, "To the front:");
			lFront = root.CLink("Front Link", style.Link, () => SelectNextCar(Dir1E.Start()));
			bDecoupleFront = root.CBtn("Decouple Front", style.Btn, () => Decouple(Dir1E.Start()), "Decouple");
			root.CText("Rear Label", style.Label, "To the rear:");
			lRear = root.CLink("Rear Link", style.Link, () => SelectNextCar(Dir1E.End()));
			bDecoupleRear = root.CBtn("Decouple Rear", style.Btn, () => Decouple(Dir1E.End()), "Decouple");

			_showFrontAndRear();
			_showWhetherUnderControl();
			
			_controlHandler = new(this);
			view.L.Trains.Trains.Add(this);
		}

		public void Destroy() {
			_controlHandler.Destroy();
			View.L.Trains.Trains.Remove(this);
		}

		private void _showFrontAndRear() {
			var front = Car.NextInPartOrder(Dir1E.Start());
			if(front == null) {
				lFront.Text("end of train").Disabled();
				bDecoupleFront.Inactive();
			} else {
				lFront.Text(front.Design.TypeStr).Enabled();
				bDecoupleFront.Active();
			}
			
			var rear = Car.NextInPartOrder(Dir1E.End());
			if(rear == null) {
				lRear.Text("end of train").Disabled();
				bDecoupleRear.Inactive();
			} else {
				lRear.Text(rear.Design.TypeStr).Enabled();
				bDecoupleRear.Active();
			}
		}

		private void _showWhetherUnderControl() {
			var ctrl = View.Control.Controlled == Car;
			if(ctrl) {
				bControl.Text("Release Control");
				tUnderControl.Active();
			} else {
				bControl.Text("Take Control");
				tUnderControl.Inactive();
			}
		}

		private void _toggleControl() {
			var ctrl = View.Control.Controlled == Car;
			View.Control.Controlled = ctrl ? null : Car;
		}

		private void SelectNextCar(Dir1E end) {
			var car = Car.NextInPartOrder(end);
			if(car != null)
				View.Selection.SetSelection(car);
		}

		private void Decouple(Dir1E end) {
			var next = Car.NextInPartOrder(end);
			if(next != null)
				WUTrain.SplitParts(View.W, View.L, Car.Part, next.Part);
		}


		private static string Description(TrainPartDesign design) {
			if(design is TrainCarPart car)
				return car.CarDesign.TypeStr;
			return UnexpectedTypeR<string>(design);
		}


		public void _onAdd(Train train) {}
		public void _onRemove(Train train) {}
		public void _onReverse(Train train) { _showFrontAndRear(); }
		public void _onSplit(Train a, Train b) {
			if(Car.Train == a || Car.Train == b)
				_showFrontAndRear();
		}

		private class CarControlHandler : CastingControlHandler<TrainCar> {
			private readonly TrainCarSidebar Me;
			
			public override Type Type => typeof(TrainCar);
			protected override TrainCar _cast(object o) => (TrainCar)o;

			public CarControlHandler(TrainCarSidebar me) : base(me.View.Scene) {
				Me = me;
				Init();
			}

			public override void _onControl(TrainCar thing) => Me._showWhetherUnderControl();
			public override void _onUncontrol(TrainCar thing) => Me._showWhetherUnderControl();
		}
	}
}