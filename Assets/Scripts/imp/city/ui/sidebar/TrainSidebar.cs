using imp.city.destroy;
using imp.city.u;
using imp.city.u3;
using imp.city.wl;
using imp.city.world.rail;
using imp.util;
using static imp.city.Main;

namespace imp.city.ui.sidebar {
	public class TrainSidebar : Destroyable, UiFrequent, CollapsibleTrainCarListPanel.OnClick {
		private readonly Train Train;
		private readonly WorldView View;
		private readonly CText tSpeed;

		public TrainSidebar(CCell cell, WorldView view, Train train) {
			Train = train;
			View = view;
			var style = R.Style;

			var root = cell.OnDestroy(this).AppSidebarV();
			root.CText("Title", style.Title, "Train");
			root.CText("Subtitle", style.Body, $"{train.Parts.Cars} cars; {train.Performance.Length:n0} m");
			tSpeed = root.CText("Speed", style.Body);
			new CollapsibleTrainCarListPanel(root, train, this);
			
			view.L.Time.UiFrequent.Add(this);
			UpdateSpeed();
		}

		public void Destroy() {
			View.L.Time.UiFrequent.Remove(this);
		}

		public void _uiFrequent() => UpdateSpeed();


		private void UpdateSpeed() {
			tSpeed.Text($"{Train.Velocity.Abs():n1} m/s");
		}

		public void OnClick(TrainCar car) {
			View.Selection.SetSelection(car);
		}
	}
}