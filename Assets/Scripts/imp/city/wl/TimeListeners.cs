using System.Collections.Generic;
using imp.util;

namespace imp.city.wl {
	public class TimeListeners {
		public readonly HashSet<FrameListener> Frame = new();
		public readonly List<UiFrequent> UiFrequent = new();
		private int _uiFrequentIdx = 0;

		public void OnFrame(float deltaTime) {
			foreach(var l in Frame)
				l._onFrame(deltaTime);
			
			// run 1 UiFrequent update per frame
			if(UiFrequent.IsNotEmpty()) {
				if(_uiFrequentIdx >= UiFrequent.Count)
					_uiFrequentIdx = 0;
				UiFrequent[_uiFrequentIdx++]._uiFrequent();
			}
		}
	}



	public interface FrameListener {
		/**Called once per frame. The argument is the number of seconds between frames.*/
		public void _onFrame(float deltaTime);
	}

	public interface UiFrequent {
		/**Called less often than every frame, but generally at least once a second.
		 * Intended for UI elements that want to update their display frequently to reflect changes in the world.*/
		public void _uiFrequent();
	}
}