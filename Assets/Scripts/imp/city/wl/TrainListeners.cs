using System.Collections.Generic;
using imp.city.world.rail;

namespace imp.city.wl {
	public class TrainListeners {
		public readonly HashSet<TrainsListener> Trains = new();
		public readonly HashSet<TrainControlsListener> TrainControls = new();


		public void OnAdd(Train train) {
			foreach(var l in Trains)
				l._onAdd(train);
		}

		public void OnRemove(Train train) {
			foreach(var l in Trains)
				l._onRemove(train);
		}

		public void OnReverse(Train train) {
			foreach(var l in Trains)
				l._onReverse(train);
		}

		public void OnSplit(Train a, Train b) {
			foreach(var l in Trains)
				l._onSplit(a, b);
		}

		/**Call this when a controls input changes, e.g. throttle or brakes.*/
		public void OnControlsInputChange(Train train) {
			foreach(var l in TrainControls)
				l._onTrainControlInputChange(train);
		}
		
		public void OnControlsReplaced(Train train) {
			foreach(var l in TrainControls)
				l._onTrainControlsReplaced(train);
		}
	}


	/**Listens for trains being created and destroyed.*/
	public interface TrainsListener {
		public void _onAdd(Train train);
		public void _onRemove(Train train);
		public void _onReverse(Train train);
		/**Called when a train is split into 2 trains. "a" is the original train and contains parts from the front; "b" contains parts split off from the rear.*/
		public void _onSplit(Train a, Train b);
	}

	/**Listens for changes being made to train controls (throttle, brakes, etc.).*/
	public interface TrainControlsListener {
		/**Called when some control input changes (e.g. throttle, brakes).*/
		public void _onTrainControlInputChange(Train train);
		/**Called when the TrainControls object is replaced for a train (e.g. because someone has taken or relinquished control of it).*/
		public void _onTrainControlsReplaced(Train train);
	}
}