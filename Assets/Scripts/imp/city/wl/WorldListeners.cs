namespace imp.city.wl {
	/**A place for UI and visualization code to register to be notified when things change in the world.*/
	public class WorldListeners {
		public readonly TimeListeners Time = new();
		public readonly TrainListeners Trains = new();
	}
}