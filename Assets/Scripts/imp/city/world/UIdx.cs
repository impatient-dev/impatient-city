using System;
using System.Collections.Generic;
using imp.util;

namespace imp.city.world {
	/**An ID / index that is used to link in-game concepts and objects to the UI / Unity objects that visualize them.
	 * This struct represents an actual valid ID / index; see UIdxOpt for the optional variant.
	 * The constructor defaults to -1 (which throws an exception) to prevent you from accidentally using a default, uninitialized instance.*/
	public readonly struct UIdx {
		public readonly int Value;

		public UIdx(int value = -1) {
			if(value < 0)
				throw new ArgumentException($"{value} is not a valid UIdx");
			Value = value;
		}

		public override string ToString() => Value.ToString();
		public UIdxOpt Opt() => new(Value);
	}


	/**An ID / index that is used to link in-game concepts and objects to the UI / Unity objects that visualize them.
	 * This struct represents an optional ID / index that may or may not be present.*/
	public readonly struct UIdxOpt {
		public static readonly UIdxOpt None = new(-1);
		public readonly int Value;

		public UIdxOpt(int value = -1) {
			Value = value;
		}

		public override string ToString() => IsAbsent ? "None" : Value.ToString();
		public bool IsPresent => Value >= 0;
		public bool IsAbsent => Value < 0;
		public UIdx Req() => new(Value);
	}
	
	
	
	
	/**Efficiently maps UIdx IDs/indices to values.
	 * The lookup is intended to be much more lightweight than what happens in a Dictionary: we mainly just look at an index in a List.
	 * This object does not shrink when items are removed. But the Add method does make an effort to reuse IDs / indexes that are no longer in use.*/
	public class UIdxMap <T> where T : class {
		private const int MAX_TRIES_TO_REUSE_INDICES = 3;
		private const int MIN_SIZE_TO_CONSIDER_REUSE = 2 * MAX_TRIES_TO_REUSE_INDICES;
		private readonly List<T?> _list = new();

		public T? Opt(UIdx i) => i.Value >= 0 && i.Value < _list.Count ? _list[i.Value] : null;
		public T? Opt(UIdxOpt i) => i.IsAbsent ? null : Opt(i.Req());
		public T Req(UIdx i) => Opt(i) ?? throw new Exception($"No value at index {i.Value}");
		public T Req(UIdxOpt i) => Req(i.Req());

		/**Adds an item to this map, and returns the ID it was assigned.*/
		public UIdx Add(T item) => Add(item, ThreadLocalRandom.Current);

		/**Adds an item to this map, and returns the ID it was assigned.*/
		public UIdx Add(T item, Random rand) {
			if(_list.Count >= MIN_SIZE_TO_CONSIDER_REUSE) {
				for(var t = 0; t < MAX_TRIES_TO_REUSE_INDICES; t++) {
					var i = rand.Next(_list.Count - 1);
					if(_list[i] == null) {
						_list[i] = item;
						return new UIdx(i);
					}
				}
			}
			
			_list.Add(item);
			return new UIdx(_list.Count - 1);
		}

		/**Returns the removed object. This method will throw if the item isn't present.*/
		public T Remove(UIdx i) {
			var ret = Req(i);
			_list[i.Value] = null;
			return ret;
		}
		/**Returns the removed object, if any.*/
		public T? RemoveOpt(UIdx i) {
			var ret = Opt(i);
			_list[i.Value] = null;
			return ret;
		}
		/**Returns the removed object, if any.*/
		public T? RemoveOpt(UIdxOpt i) {
			return i.IsAbsent ? null : RemoveOpt(i.Req());
		}
		/**Removes the preexisting value for this id and inserts a new one.
		 * There must be a value with this id. */
		public void Replace(UIdx i, T value) {
			_list[i.Value] = value;
		}
		
		/**Puts a value with a specific ID. Throws if the ID is already in use.*/
		public void PutUnique(UIdx i, T value) {
			if(_list.Count <= i.Value) {
				while(_list.Count < i.Value)
					_list.Add(null);
				_list.Add(value);
			} else {
				if(_list[i.Value] != null)
					throw new Exception($"ID {i} already in use by {_list[i.Value]}");
				_list[i.Value] = value;
			}
		}

		/**Returns a read-only list of all items. This method doesn't create a new list and can be called very frequently.
		 * However, the returned list may have null items in it.*/
		public IReadOnlyList<T?> AllWithNulls => _list;
	}
}