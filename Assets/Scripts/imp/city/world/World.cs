﻿using System;
using System.Collections.Generic;
using imp.city.wl;
using imp.city.world.rail;
using imp.city.wu;
using UnityEngine;

namespace imp.city.world {

	/**Currently contains all railroads and trains.
	 * Intended to contain everything in a virtual world, but not UI state or listeners.
	 * Ideally, it should be possible to save the game by serializing this object.*/
	public class World {
		private readonly UIdxMap<RailSeg> _railSegments = new();
		private readonly UIdxMap<Train> _trains = new();
		private readonly List<Train> _activeTrains = new();

		public override string ToString() => "Chunk";

		public void Add(RailSeg segment) {
			if(segment.Id.IsPresent)
				throw new ArgumentException($"Rail segment already has an ID: {segment.Id}.");
			segment.Id = _railSegments.Add(segment).Opt();
		}

		public void Add(Train train) {
			if(train.Id.IsPresent)
				throw new Exception("Cannot add a train that already has an ID");
			train.Id = _trains.Add(train).Opt();
			if(train.ShouldBeActive()) {
				_activeTrains.Add(train);
				train.Active = true;
			}
		}

		public void Remove(Train train) {
			if(train.Id.IsAbsent)
				throw new Exception("Cannot remove a train with no ID");
			_trains.Remove(train.Id.Req());
			if(train.Active) {
				_activeTrains.Remove(train);
				train.Active = false;
			}
		}

		/**Removes the old train and assigns its ID to the new train.*/
		public void Replace(Train old, Train nu) {
			if(old.Id.IsAbsent)
				throw new Exception("Cannot replace a train with no ID");
			if(nu.Id.IsPresent)
				throw new Exception("Replacement train already has an ID");
			var id = old.Id.Req();
			_trains.Replace(id, nu);
			old.Id = UIdxOpt.None;
			nu.Id = id.Opt();
			if(old.Active) {
				_activeTrains.Remove(old);
				old.Active = false;
			}
			if(nu.ShouldBeActive()) {
				_activeTrains.Add(nu);
				nu.Active = true;
			}
		}


		/**Adds to our list of active trains and sets the active flag on the train. This train must not currently be active.
		 * Currently, trains are marked inactive during TickActiveTrains; there is no method to mark an active train inactive.*/
		public void MarkActive(Train train) {
			Debug.Assert(!train.Active);
			_activeTrains.Add(train);
			train.Active = true;
		}


		public RailSeg RailSeg(UIdx id) => _railSegments.Req(id);
		public IReadOnlyList<RailSeg?> AllRailSegsWithNulls() => _railSegments.AllWithNulls;

		public IReadOnlyList<Train?> AllTrainsWithNulls() => _trains.AllWithNulls;
		public IReadOnlyList<Train> ActiveTrains() => _activeTrains;

		/**Calls WUTrain.Tick. Trains that shouldn't be active will be removed instead.*/
		public void TickActiveTrains(float deltaTime, WorldListeners wl) {
			for(var i = 0; i < _activeTrains.Count; i++) {
				var train = _activeTrains[i];
				if(!train.ShouldBeActive()) {
					_activeTrains.RemoveAt(i);
					train.Active = false;
					i--;
				} else {
					WUTrain.Tick(this, wl, train, deltaTime);
				}
			}
		}
	}
}