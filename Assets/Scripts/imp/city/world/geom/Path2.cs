﻿using imp.city.world.geom;
using imp.util;
using UnityEngine;

namespace imp.city.world.rail {
	
	/**A 2D path that a railroad/road/etc can follow: a line or curve. (Elevation is handled separately.)
	 * A vehicle/etc. can move along a path in the forward direction (from distance = 0 to distance = Length), or in the reverse direction.
	 * Distances outside [0, Length] are invalid, but paths should extrapolate for invalid positions instead of failing.*/
	public interface Path2 {
		float Length { get; }
		/**Whether the direction changes along this path.*/
		bool IsCurved { get; }
		/**The minimum (tightest) radius of curvature on this path. Infinity for a straight path.*/
		float MinRadius { get; }
		/**The location at some distance from the path start.*/
		Vector2 PosAt(float distance);
		/**The direction (derivative/tangent) of the path at some distance from the start. This is a normalized vector.*/
		Vector2 DirAt(float distance);
	}


	public static class Path2Extensions {
		public static PathHV Flat(this Path2 horizontal, float altitude = 0f) =>
			new PathHV(horizontal, new FlatPathV(altitude, horizontal.Length));
	}
	
	

	/// A straight line between 2 points.
	public sealed class StraightPath2 : Path2 {
		public readonly Vector2 Start, End;
		public readonly Vector2 Direction;
		public float Length { get; }
		public bool IsCurved => false;
		public float MinRadius => float.PositiveInfinity;
		public Vector2 PosAt(float distance) => Start + Direction * distance;
		public Vector2 DirAt(float distance) => Direction;

		public StraightPath2(Vector2 start, Vector2 end) {
			Start = start;
			End = end;
			Direction = (end - start).normalized;
			Length = (end - start).magnitude;
		}
	}



	/// A section of a circle. (Depending on the angles used, you can form a complete circle, or even a spiral that revolves around the center multiple times.)
	public sealed class CircularPath2 : Path2 {
		public readonly Vector2 Center;
		public readonly float Radius;
		/// The angle in radians where the circle starts and ends. 0 is east (1,0); pi/2 is north (0,1), etc.
		/// The start angle is less than the end for a counter-clockwise circle, and greater for a clockwise one.
		public readonly float StartAngle, EndAngle;
		
		public float Length { get; }
		public bool IsCurved => true;
		public float MinRadius => Radius;
		public bool IsClockwise => StartAngle > EndAngle;

		public CircularPath2(Vector2 center, float radius, float startAngle, float endAngle) {
			Center = center;
			Radius = radius;
			StartAngle = startAngle;
			EndAngle = endAngle;
			Length = (radius * 2 * Mathf.PI) * (Mathf.Abs(endAngle - startAngle) / 2 / Mathf.PI); //circumference * # of full revolutions
		}

		private float AngleByDistance(float distance) => StartAngle + (distance / Length) * (EndAngle - StartAngle);

		public Vector2 PosAt(float distance) {
			var angle = AngleByDistance(distance);
			return new Vector2(Center.x + Radius * Mathf.Cos(angle), Center.y + Radius * Mathf.Sin(angle));
		}
		public Vector2 DirAt(float distance) {
			var towardCenter = (PosAt(distance) - Center).normalized;
			return towardCenter.RotL(!IsClockwise);
		}
	}
}