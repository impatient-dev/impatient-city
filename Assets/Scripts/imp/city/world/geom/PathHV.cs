﻿using System;
using imp.city.world.rail;
using imp.util;
using UnityEngine;
using static imp.util.ImpatientUtil;


namespace imp.city.world.geom {

	/**A path in 3D space, consisting of separate horizontal and vertical paths of the same length.*/
	public readonly struct PathHV {
		public readonly Path2 Horizontal;
		public readonly PathV Vertical;
		public float Length => Horizontal.Length;

		public PathHV(Path2 horizontal, PathV vertical) {
			if(RelativeDifference(horizontal.Length, vertical.Length) > 0.01f)
				throw new ArgumentException($"Length mismatch: horizontal ${horizontal.Length}, vertical ${vertical.Length}.");
			Horizontal = horizontal;
			Vertical = vertical;
		}

		public Vector3 PosAt(float distance) => Horizontal.PosAt(distance).AugShift(Vertical.PosAt(distance));

		/**The derivative. Warning: this may not be a unit vector.*/
		public Vector3 DirAt(float distance) {
			// TODO this is probably wrong
			return Horizontal.DirAt(distance).AugShift(Vertical.SlopeAt(distance));
		}
	}
}