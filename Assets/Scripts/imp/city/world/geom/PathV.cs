﻿namespace imp.city.world.rail {

	/// Defines the vertical geometry of a train track: the height at various points. Similar to Path2, but 1-dimensional.
	public interface PathV {
		/// Uniquely identifies this path within its chunk. Initially 0 (invalid).
		uint Id { get; set; }
		/// False if this path is linear as a function of distance.
		bool IsCurved { get; }
		float MinRadius { get; }
		float Length { get; }
		float PosAt(float distance);
		float SlopeAt(float distance);
	}


	/// A path of constant height.
	public sealed class FlatPathV : PathV {
		public uint Id { get; set; } = 0;
		public bool IsCurved => false;
		public float Length { get; }
		public readonly float Altitude;
		public float MinRadius => float.PositiveInfinity;
		
		public FlatPathV(float altitude, float length) {
			Altitude = altitude;
			Length = length;
		}

		public float PosAt(float distance) => Altitude;
		public float SlopeAt(float distance) => 0;
	}
	
}