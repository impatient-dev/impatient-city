﻿namespace imp.city.world.geom {

	/**Basic numbers and functions related to railroad geometry.*/
	public static class RailGeom {
		/**How far apart the centers of the rails are from each other.*/
		public const float StandardGauge = 1.435f;
		public const float Gravity = 9.81f;
		
		/**How far out from the car a coupler extends. The separation between 2 cars is double this.*/
		public static float CouplerLen(float gauge = StandardGauge) => 0.6f * gauge / StandardGauge;
		/**How much distance 2 coupled cars are separated by - double the coupler length.*/
		public static float CarSep(float gauge = StandardGauge) => 2 * CouplerLen(gauge);
		
		/**The largest separation that is allowed between 2 bogies. (Determines the max length of a train car.)*/
		public static float MaxBogieSep(float gauge = StandardGauge) => 30 * gauge / StandardGauge;

		/**Apparently standard-gauge railroad cars are often limited to around 10 feet 6 inches in width.*/
		public static float MaxWidth(float gauge = StandardGauge) => 3.2f * gauge / StandardGauge;


		public static float NormalWheelDiameter(float gauge = StandardGauge) => 1 * gauge;

		public static float BogieLength(float gauge = StandardGauge) => BogieLengthFromWheelDiameter(NormalWheelDiameter(gauge));
		/**Determines the length (front to back) of a normal 2-axle bogie.*/
		public static float BogieLengthFromWheelDiameter(float diameter) => 2.7f * diameter;
	}

}