using imp.city.world.geom;
using imp.util;

namespace imp.city.world.rail {
	/**A path a train can take through some rail segment.
	 * This class should eventually have a member that gives electrification, rail gauge, weight limit, etc. and is shared between paths.*/
	public sealed class RailPath {
		public readonly PathHV Geom;
		public readonly RailSeg Seg;
		/**The index of this path in its segment. TODO use*/
		public readonly int Idx;
		/**What the start and end of this path connect to, if anything. Initially nothing (invalid).*/
		public RailPathConn StartConn = RailPathConn.Invalid(), EndConn = RailPathConn.Invalid();

		public RailPath(PathHV geom, RailSeg seg, int idx) {
			Geom = geom;
			Seg = seg;
			Idx = idx;
		}

		public override string ToString() => $"RailPath({this.HexHash()} len={Geom.Length} {Seg})";

		public ref RailPathConn Conn(Dir1E end) {
			// C# doesn't like the 1-line version of this function
			if(end.IsStart)
				return ref StartConn;
			return ref EndConn;
		}
	}
}