using System;
using UnityEngine;

namespace imp.city.world.rail {

	/**Describes what path the start/end of a path is connected to. The connection should be mutual, unless this connects to nothing (null).*/
	public readonly struct RailPathConn {
		public readonly World? Chunk;
		public readonly RailPath? Path;
		/**What end of the other path we are connected to.*/
		public readonly Dir1E End;

		private RailPathConn(World? chunk, RailPath? path, Dir1E end) {
			Chunk = chunk;
			Path = path;
			End = end;
			if((chunk == null) != (path == null))
				throw new ArgumentException($"chunk = {chunk} but path = {path}");
		}

		public static RailPathConn Valid(World chunk, RailPath path, Dir1E end) {
			Debug.Assert(chunk != null && path != null);
			return new RailPathConn(chunk, path, end);
		}

		public static RailPathConn Invalid() => new(null, null, Dir1E.Start());

		/**If false, this is a connection to nothing.*/
		public bool IsValid => Chunk != null;
		public override string ToString() => $"RailPathConn(seg={Path?.Seg} path={Path} {End})";
	}
}