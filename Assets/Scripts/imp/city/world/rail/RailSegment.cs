﻿using imp.city.world.geom;
using UnityEngine;

namespace imp.city.world.rail {

	/**A rail segment: 1 or more rail paths that intersect.
	 * A normal piece of track has only 1 path; multi-path segments are switches and crossings.
	 * Two trains can only collide if they occupy the same segment.*/
	public abstract class RailSeg {
		/**Uniquely identifies this segment within its chunk. Initially unset.*/
		public UIdxOpt Id = UIdxOpt.None;
		/**How many distinct paths a train can follow through this segment - always at least 1.*/
		public readonly int PathCount;

		protected RailSeg(int pathCount) {
			Debug.Assert(pathCount > 0);
			PathCount = pathCount;
		}
	}

	
	
	/**A basic segment consisting of 1 path.*/
	public sealed class BasicRailSeg : RailSeg {
		public readonly RailPath Path;

		public BasicRailSeg(PathHV path) : base(1) {
			Path = new(path, this, 0);
		}

		public override string ToString() => $"BasicRailSeg({Id})";
	}
}