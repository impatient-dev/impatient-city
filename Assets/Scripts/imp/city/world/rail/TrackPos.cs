using imp.util;
using UnityEngine;

namespace imp.city.world.rail {
	
	/**Represents a position on some piece of track.*/
	public readonly struct TrackPos {
		public readonly World Chunk;
		public readonly RailPath Path;
		/**Distance from the start of the path.*/
		public readonly float DistanceFromStart;

		public TrackPos(World chunk, RailPath path, float distanceFromStart) {
			Chunk = chunk;
			Path = path;
			DistanceFromStart = distanceFromStart;
		}

		public override string ToString() => $"TrackPos({Path} dist={DistanceFromStart})";
		
		/**This method might return a position that is past the start/end of the path.*/
		public TrackPos PlusDistance(float distance) => new(Chunk, Path, DistanceFromStart + distance);
		public TrackPosOr Facing(Dir1E dir) => new(this, dir);
		public Vector3 Pos3d() => Path.Geom.PosAt(DistanceFromStart);
	}



	/**Represents a position and orientation on some piece of track.*/
	public readonly struct TrackPosOr {
		public readonly TrackPos Pos;
		/**Which way we're facing: whether the start or end of the path is in front of us.*/
		public readonly Dir1E Facing;

		public TrackPosOr(TrackPos pos, Dir1E facing) {
			Pos = pos;
			Facing = facing;
		}

		public override string ToString() => Pos + ":" + (Facing.IsStart ? "fwd" : "rev");

		public TrackPosOr Reversed() => new(Pos, !Facing);
		/**Returns a new position that is a certain distance behind this one. This method might return a position that is past the end of the path.*/
		public TrackPosOr Backward(float distance) => new(Pos.PlusDistance(distance.NegateIf(Facing.IsEnd)), Facing);
		/**Returns a new position that is a certain distance ahead of this one. This method might return a position that is past the end of the path.*/
		public TrackPosOr Forward(float distance) => Backward(-distance);
	}
}