using System;
using System.Collections.Generic;
using imp.city.u;
using imp.city.world.rail.ctrl;
using static imp.util.ImpatientUtil;

namespace imp.city.world.rail {
	
	/**An array of train parts rigidly linked together so they move at the same velocity.
	 * The front of the train is an arbitrary end and isn't special.*/
	public class Train : Disusable {
		public UIdxOpt Id = UIdxOpt.None;
		
		/**Positive is towards the front of the train.*/
		public float Velocity;
		/**Whether we're in the world's list of active trains that need to be updated every frame, because they're moving or accelerating.*/
		public bool Active = false;

		public TrainParts Parts;
		public TrainPositions Pos;
		public TrainPerformance Performance { get; private set; }
		public TrainControls Controls;


		/**The parts shouldn't be part of another train; this constructor will update the parts to reference this train.
		 * The front of the train will be set to the provided position, but the other positions are undefined and need to be fixed immediately (see TrainPositions).*/
		public Train(IReadOnlyList<TrainPart> parts, TrackPosOr front, TrainControls controls) {
			CheckArg(parts.Count > 0, "A train must have at least 1 part");
			Controls = controls;
			Parts = new(this, parts);
			Pos = TrainPositions.For(in Parts, front);
			Performance = TrainBasicsCalculator.Performance(Parts.PartsList);
		}

		/**Adds part(s) to the front or back of the train.
		 * The parts are added in their current order and orientation; there are some scenarios where you need to reverse the order and orientation before calling this.
		 * The parts shouldn't be part of another train; this function will update the parts to reference this train.
		 * Afterward, the positions will be invalid and need fixing (see TrainPositions).
		 * If the parts were added to the front of the train, the front will be adjusted to keep the existing parts in their current positions.*/
		public void Add(IReadOnlyList<TrainPart> parts, Dir1E end) {
			CheckArg(parts.Count > 0, "Cannot add 0 parts");
			var lenBefore = Performance.Length;

			if(end.IsEnd) {
				Parts.Append(parts);
				Pos.Append(prev: Parts.PartsList, add: parts);
			} else { // adding to front of train
				Parts.Prepend(parts);
				Pos.Prepend(prev: Parts.PartsList, add: parts);
			}

			Performance = TrainBasicsCalculator.Performance(Parts.PartsList);
		}

		/**Reverses the order and orientation of the parts of this train, plus the velocity and throttle.
		 * The positions should not need fixing after this.*/
		public void Reverse() {
			Parts.Reverse();
			Pos.Reverse();
			Velocity = -Velocity;
			Controls._onReverse();
		}

		/**Removes all parts after the provided one and returns all removed parts in a List. The parts will have their indices and reference to this train unset.
		 * The end-of-train position will become invalid and need fixing.
		 * If this train was being controlled from one of the removed parts, this method will not fix that.*/
		public List<TrainPart> SplitAfter(TrainPart part) {
			if(part.Train != this)
				throw new Exception($"Train part is part of train {part.Train.HexHash()}, not {this.HexHash()}");
			var removed = Parts.RemoveAfter(part.TrainIdx);
			Pos.RemoveAfter(part);
			Performance = TrainBasicsCalculator.Performance(Parts.PartsList);
			return removed;
		}


		public bool ShouldBeActive() => Velocity != 0 || Controls.Throttle != 0;

		public void Disuse() {
			Id = UIdxOpt.None;
			Velocity = float.NaN;
			Parts.Disuse();
			Pos.Disuse();
		}
	}
}