using System.Collections.Generic;
using imp.city.world.geom;
using UnityEngine;

namespace imp.city.world.rail {

	public static class TrainBasicsCalculator {
		public static TrainPerformance Performance(IReadOnlyList<TrainPart> parts) {
			double length = 0, mass = 0;
			float width = 0, height = 0;
			double tractiveForce = 0, tractivePower = 0;

			foreach(var part in parts) {
				length += part.Design.TotalLength;
				width = Mathf.Max(width, part.Design.Width);
				height = Mathf.Max(height, part.Design.Height);
				mass += part.Design.Mass;
				tractiveForce += part.Design.TractiveForce;
				tractivePower += part.Design.TractivePower;
			}
			
			return new TrainPerformance {
				Length = (float)length,
				Width = width,
				Height = height,
				Mass = (float)mass,
				TractiveForce = (float)tractiveForce,
				TractivePower = (float)tractivePower,
				FastBrakeForce = (float)tractiveForce,
				RollingResistance = (float)(mass * RailGeom.Gravity * 0.002),
			};
		}
	}
}