using static imp.util.ImpatientUtil;

namespace imp.city.world.rail {

	/**A part of a train that can contain passengers or cargo. This concept is currently unused.*/
	public class TrainCar {
		public readonly TrainPart Part;
		public readonly TrainCarDesign Design;
		public readonly int IdxInPart;

		public Train Train => Part.Train;

		public TrainCar(TrainPart part, TrainCarDesign design, int idxInPart) {
			Part = part;
			Design = design;
			IdxInPart = idxInPart;
		}

		public override string ToString() => $"TrainCar(#{IdxInTrain} {Part.Train?.Id}:{Part.TrainIdx}:{IdxInPart} design={Design})";

		public int IdxInTrain {
			get {
				if(Part.IsFwd)
					return Part.CarIdx1 + IdxInPart;
				return Part.CarIdx1 + (Part.Cars.Length - IdxInPart - 1);
			}
		}

		/**Returns the next or previous car in the train (in train order, not part order), or null if this is the end of the train.*/
		public TrainCar? NextInTrain(Dir1E dir) {
			var newIdxInPart = IdxInPart + Positive(dir.IsEnd == Part.IsFwd);
			if(newIdxInPart >= 0 && newIdxInPart < Part.Cars.Length)
				return Part.Cars[newIdxInPart];
			var newPartIdx = Part.TrainIdx + Positive(dir.IsEnd);
			if(newPartIdx >= 0 && newPartIdx < Train.Parts.Parts) {
				var newPart = Train.Parts.Part(newPartIdx);
				return newPart.Cars.End(dir.IsEnd);
			}
			return null;
		}

		/**Returns the next or previous car in the train, where end.IsEnd means the next car closer to the end of the part, not necessarily closer to the end of the train.*/
		public TrainCar? NextInPartOrder(Dir1E dir) => NextInTrain(Dir1E.End(dir.IsEnd == Part.IsFwd));
	}


	public interface TrainCarDesign {
		public string TypeStr { get; }
	}
}