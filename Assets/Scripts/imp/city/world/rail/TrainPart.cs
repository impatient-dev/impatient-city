using System;
using imp.city.world.geom;
using imp.city.world.rail.cars;
using static imp.util.ImpatientUtil;

namespace imp.city.world.rail {

	/**A section of a train that can decouple from the rest and roll on its own.
	 * Normally each train car is a separate part, but other arrangements are possible (shared "Jacob's" bogies, articulation).
	 * Each part has at least one rail attachment position (bogie).*/
	public class TrainPart {
		/**Null if not attached to a train.*/
		public Train Train { get; private set; }
		/**The index of this part in its train; -1 when not part of a train.*/
		public int TrainIdx { get; private set; } = -1;
		/**The number of cars in the train before the first one in this part; -1 when not part of a train.*/
		public int CarIdx1 { get; private set; } = -1;
		/**Our train has a list of positions where parts of the train attach to the tracks.
		 * This is the first index in that array that belongs to this part, excluding the front-of-train and rear-of-train positions.
		 * The order of entries in that list is from the front to the back of the train; if this part/car is backwards, that's the opposite of "our" order.
		 * -1 when not part of a train.*/
		public int PositionsBefore { get; private set; } = -1;
		
		public readonly TrainPartDesign Design;
		public readonly TrainCar[] Cars;
		/**Front means that the front of this car is closer to the front of the train than the back of the car is.
		 * Back means the opposite - the car is pointed backwards in the train.*/
		public Dir1S Facing;

		public TrainPart(TrainPartDesign design, Dir1S facing) {
			Design = design;
			Facing = facing;

			Cars = new TrainCar[design.Cars];
			for(int c = 0; c < design.Cars; c++)
				Cars[c] = new TrainCar(this, design.Car(c), c);

			if(design.AttachmentDistances.IsEmpty())
				throw new ArgumentException("A train part must have at least one rail attachment distance.");

			Train = null!;
		}

		public bool IsFwd => Facing.IsFront;
		public bool IsRev => Facing.IsBack;
		/**Returns the end of this part that is closer to the front of the train.*/
		public Dir1E FrontOfTrainEnd => Dir1E.Start(IsFwd);
		
		public int PositionCount => Design.AttachmentDistances.Length;
		public override string ToString() => $"TrainPart(train={Train?.Id} part {TrainIdx} car {CarIdx1}+ design={Design})";

		public int MaxRailAttachmentPointIndex => PositionsBefore + PositionCount - 1;

		/**Returns the position of one of our rail attachment points.
		 * The index specifies which point, taking into account whether this part is forward or backward in its train.*/
		public TrackPosOr Pos(int idx) {
			if(IsFwd)
				return Train!.Pos.Pos(PositionsBefore + idx);
			return Train!.Pos.Pos(PositionsBefore + PositionCount - idx - 1);
		}

		public TrackPosOr ExtrapolatedRailPosAt(float distanceFromStart) => IsFwd ?
			Pos(0).Forward(Design.AttachmentDistances[0] + RailGeom.CouplerLen()) :
			Pos(PositionCount - 1).Forward(Design.LengthExcludingCouplers - Design.AttachmentDistances.Last() + RailGeom.CouplerLen());
		/**Extrapolates the position of the front or rear of this part (in part order, not train order). The position may be invalid and need fixing.*/
		public TrackPosOr ExtrapolatedRailPosAt(Dir1E end) => ExtrapolatedRailPosAt(end.IsStart ? 0 : Design.TotalLength);
		/**Extrapolates the position of the front or rear of this part (in train order, not part order). The position may be invalid and need fixing.*/
		public TrackPosOr ExtrapolatedRailPosAtTrainOrder(Dir1E end) => ExtrapolatedRailPosAt(IsFwd ? end : !end);

		/**Returns the distance between the rail attachment point closest to start-of-train and the end of this car that is closest to start-of-train. The position may be invalid and need fixing.*/
		public float DistanceBetweenFirstAttachmentPointAndEndInTrainOrder() {
			if(IsFwd)
				return Design.AttachmentDistances[0];
			else
				return Design.LengthExcludingCouplers - Design.AttachmentDistances[^1];
		}
		/**Returns the distance between the rail attachment point closest to end-of-train and the end of this car that is closest to end-of-train.*/
		public float DistanceBetweenLastAttachmentPointAndEndInTrainOrder() {
			if(IsFwd)
				return Design.LengthExcludingCouplers - Design.AttachmentDistances[^1];
			else
				return Design.AttachmentDistances[0];
		}
		
		
		public void OnRemoveFromTrain() {
			if(Train == null)
				throw new Exception("This already wasn't part of a train.");
			Train = null!;
			TrainIdx = -1;
			PositionsBefore = -1;
		}

		public void OnAddToTrain(Train train, int index, int carsBefore, int attachmentPositionsBefore) {
			if(Train != null)
				throw new Exception("This is already part of a train.");
			Train = train;
			TrainIdx = index;
			CarIdx1 = carsBefore;
			PositionsBefore = attachmentPositionsBefore;
		}

		public void UpdateIndexes(int trainIndex, int carsBefore, int attachmentPositionsBefore) {
			if(Train == null)
				throw new Exception("It doesn't make sense to set indexes when this isn't part of a train.");
			TrainIdx = trainIndex;
			CarIdx1 = carsBefore;
			PositionsBefore = attachmentPositionsBefore;
		}
	}
}