using System;
using System.Collections.Generic;
using imp.city.u;
using UnityEngine;

namespace imp.city.world.rail {
	/**Contains the different parts and cars of a train.
	 * The constructor will notify all parts that they belong to this train, setting indexes as appropriate.*/
	public struct TrainParts: Disusable {
		private List<TrainPart> _parts;
		public int Cars { get; private set; }
		/**The number of attachment points on this train, for use elsewhere when initializing the list of distances.
		 * This does not include the front and back of the train.*/
		public int _railAttachmentPoints { get; private set; }

		public readonly int Parts => _parts.Count;
		public readonly TrainPart Part(int idx) => _parts[idx];
		public IReadOnlyList<TrainPart> PartsList => _parts;

		public void Disuse() {
			_parts = null!;
			Cars = -1;
		}


		public TrainParts(Train train, IReadOnlyList<TrainPart> parts) {
			_parts = new(parts.Count);

			int p = 0, c = 0, a = 0;
			foreach(var part in parts) {
				_parts.Add(part);
				part.OnAddToTrain(train, p++, c, a);
				c += part.Design.Cars;
				a += part.Design.AttachmentDistances.Length;
			}
			Cars = c;
			_railAttachmentPoints = a;
		}


		/**Adds parts to the rear of the train.*/
		public void Append(IReadOnlyList<TrainPart> parts) {
			var train = _parts[0].Train!;
			foreach(var part in parts) {
				part.OnAddToTrain(train, _parts.Count, Cars, _railAttachmentPoints);
				_parts.Add(part);
				Cars += part.Cars.Length;
				_railAttachmentPoints += part.Design.AttachmentDistances.Length;
			}
		}


		/**Adds parts to the front of the train.*/
		public void Prepend(IReadOnlyList<TrainPart> parts) {
			var train = _parts[0].Train!;
			int p = 0, c = 0, a = 0;

			for(; p < parts.Count; p++) {
				var part = parts[p];
				part.OnAddToTrain(train, p, c, a);
				c += part.Design.Cars;
				a += part.Design.AttachmentDistances.Length;
			}

			for(; p < _parts.Count; p++) {
				var part = _parts[p];
				part.UpdateIndexes(p, c, a);
				c += part.Design.Cars;
				a += part.Design.AttachmentDistances.Length;
			}

			Cars = c;
			_railAttachmentPoints = a;
		}


		public void Reverse() {
			int p = 0, c = 0, a = 0; // part, car, rail attachment point
			int stop = Mathf.FloorToInt(_parts.Count / (float)2);
			for(; p < stop; p++) {
				var pp = _parts.Count - p - 1;
				var part = _parts[pp];
				_parts[pp] = _parts[p];
				_parts[p] = part;
				part.Facing = !part.Facing;
				part.UpdateIndexes(trainIndex: p, carsBefore: c, attachmentPositionsBefore: a);
				c += part.Cars.Length;
				a += part.PositionCount;
			}
			for(; p < _parts.Count; p++) {
				var part = _parts[p];
				part.Facing = !part.Facing;
				part.UpdateIndexes(trainIndex: p, carsBefore: c, attachmentPositionsBefore: a);
				c += part.Cars.Length;
				a += part.PositionCount;
			}
		}


		/**Removes all parts after the provided index, and returns a list of the parts removed.*/
		public List<TrainPart> RemoveAfter(int idx) {
			if(idx < 0 || idx >= Parts - 1)
				throw new Exception($"For a train of {Parts} parts, cannot split after part {idx}.");
			var toRemove = Parts - idx - 1;
			var removed = new List<TrainPart>(toRemove);

			Cars = _parts[idx + 1].CarIdx1;
			for(int i = 0; i < toRemove; i++) {
				var part = _parts[idx + i + 1];
				part.OnRemoveFromTrain();
				removed.Add(part);
			}
			_parts.RemoveRange(idx + 1, toRemove);

			return removed;
		}
	}
}