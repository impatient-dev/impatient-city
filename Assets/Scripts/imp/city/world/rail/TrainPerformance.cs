using UnityEngine;

namespace imp.city.world.rail {
	
	/**Describes the performance-related characteristics of a train. This is a big mutable struct.*/
	public struct TrainPerformance {
		/**Including couplers at both ends.*/
		public float Length;
		public float Width;
		public float Height;
		public float Mass;

		/**How much force opposes the train's motion.*/
		public float RollingResistance;

		/**How much force the active engines can apply to move the train.
		 * We use either this or the tractive power, whichever provides less acceleration at the current speed.*/
		public float TractiveForce;
		/**How much power the active engines can apply to move the train, excluding HEP and losses in the engines.
		 * We use either this or the tractive force, whichever provides less acceleration at the current speed.*/
		public float TractivePower;

		/**How much force can be applied to stop the train by the brakes that are slowly triggered by air moving through the brakepipe.*/
		public float SlowBrakeForce; // TODO
		/**How much force can be applied by brakes that can be activated and deactivated quickly: locomotive independent brakes, and electronically controlled brakes.*/
		public float FastBrakeForce;

		/*Calculates the engine force that is accelerating the train based on the throttle setting and current speed.
		 * The value will be computed based on the power and the starting tractive effort, whichever is less.
		 * Both inputs must be positive; the output is also positive.*/
		public float EngineForcePositiveOnly(float throttleAmount, float speed) {
			var a = throttleAmount * TractiveForce;
			if(speed < 1)
				return a;
			var b = _engineForceFromPower(throttle: throttleAmount, speed: speed);
			return Mathf.Min(a, b);
		}
		/**Calculates the engine force that can accelerate the train based on the tractive power.*/
		// Wikipedia says Hay (1978) says t = P E / v, where E is an efficiency accounting for losses in motors, lighting, etc. of around 82%.
		// But TractivePower should already have those losses subtracted out.
		private float _engineForceFromPower(float throttle, float speed) => throttle * TractivePower /speed;
	}
}