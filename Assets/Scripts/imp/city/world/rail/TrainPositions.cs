using System.Collections.Generic;
using imp.city.u;
using imp.city.world.geom;
using imp.util;

namespace imp.city.world.rail {
	/**Contains info about the rail attachment points for a train: where they are on the tracks, and how far apart they are.
	 * Normally, each bogie on the train is a rail attachment point, but some kinds of rolling stock don't have bogies and attach differently (e.g. steam locomotives).
	 * In addition to the positions of the rail attachment points, this struct also contains the start-of-train and end-of-train positions.
	 * This struct contains the positions, but not the logic for making them valid.
	 * When the positions are possibly invalid, we say that they "need to be fixed". Fixing them involves 2 steps:
	 * (1) if the front is past the start or end of its path, it needs to be moved to the correct one;
	 * (2) all other positions need to be adjusted to be the correct distance from the front.
	 * This fixing only applies to the positions though; the distances between the rail attachment points are kept valid by this struct.*/
	public struct TrainPositions : Disusable {
		private List<TrackPosOr> _pos;
		private List<float> _sep;


		private TrainPositions(List<TrackPosOr> pos, List<float> sep) {
			_pos = pos;
			_sep = sep;
		}

		public int RailAttachmentPoints => _sep.Count - 1;
		
		/**The position of the front of the train (the tip of the coupler - this is probably not the location of a rail attachment point).*/
		public TrackPosOr Front {
			get => _pos[0];
			set => _pos[0] = value;
		}
		
		/**The position of the rear of the train (the tip of the coupler - this is probably not the location of a rail attachment point).*/
		public TrackPosOr Rear {
			get => _pos[^1];
			set => _pos[^1] = value;
		}

		/**Returns a position where part of the train attaches to the rails.
		 * 0 returns the first position in the first part of the train.
		 * This method doesn't consider whether any train parts are oriented forwards or backwards.*/
		public TrackPosOr Pos(int idx) => _pos[idx + 1];
		/**Sets the position of a rail attachment point in the train.*/
		public void SetPos(int idx, TrackPosOr pos) => _pos[idx + 1] = pos;

		/**For index N, gives the distance between rail-attachment-point N and N-1 (basically the next bogie closer to the front of the train).
		 * For the first rail attachment point in the train, the previous attachment point is considered to be the front of the train.
		 * This list contains an extra entry at the end, that gives the distance between the end-of-train and the last rail attachment point.*/
		public IReadOnlyList<float> Separations => _sep;

		public void Disuse() {
			_pos = null!;
			_sep = null!;
		}



		/**Reverses the order of all parts. The positions will stay valid and don't need fixing.
		 * The orientation and indices must have already been updated on the provided part.*/
		public void Reverse() {
			_pos.Reverse();
			_sep.Reverse();
		}

		/**Adds positions for new parts added at the rear of the train. The added positions will be invalid and need to be fixed.
		 * This function doesn't care whether the indices on the parts have been updated yet.*/
		public void Append(IReadOnlyList<TrainPart> prev, IReadOnlyList<TrainPart> add) {
			_sep.RemoveLast(); // end-of-train
			var couplerLen = RailGeom.CouplerLen();
			var extraDistance = 2 * couplerLen + prev[^1].DistanceBetweenLastAttachmentPointAndEndInTrainOrder();
			
			for(int p = 0; ; ) {
				var part = add[p];
				_pos.RepeatedAdd(Front, part.PositionCount);
				AddSeparations(part, _sep, extraAtEnd: out extraDistance, extraDistanceBefore: extraDistance, sepIdx0: _sep.Count);
				if(++p == add.Count) {
					_sep.Add(extraDistance + couplerLen); // end-of-train
					break;
				}
				extraDistance += 2 * couplerLen;
			}
		}


		/**Adds positions for new parts added at the front of the train. 
		 * This function requires the indexes on the parts to have been updated beforehand.
		 * This function will move the front-of-train forward so the existing parts stay in their place.
		 * All positions will be invalid and need to be fixed.*/
		public void Prepend(IReadOnlyList<TrainPart> add, IReadOnlyList<TrainPart> prev) {
			// update the first position in the preexisting parts
			var couplerLen = RailGeom.CouplerLen();
			_sep[0] = _sep[0] + 2 * couplerLen + add[^1].DistanceBetweenLastAttachmentPointAndEndInTrainOrder();

			double totalAddedLength = 0;
			var extraDistance = couplerLen;
			for(int p = 0; ; ) {
				var part = add[p];
				totalAddedLength += part.Design.TotalLength;
				_pos.RepeatedInsert(part.PositionsBefore, Front, part.PositionCount);
				AddSeparations(part, _sep, extraAtEnd: out extraDistance, extraDistanceBefore: extraDistance, sepIdx0: part.PositionsBefore);
				if(++p == add.Count)
					break;
				extraDistance += 2 * couplerLen;
			}

			Front = Front.Forward((float)totalAddedLength);
		}


		/**Removes positions for parts after the provided one. The end-of-train position will become invalid and need fixing; other positions will stay valid.*/
		public void RemoveAfter(TrainPart part) {
			var removePositions = RailAttachmentPoints - part.MaxRailAttachmentPointIndex - 1;
			_pos.RemoveRange(part.MaxRailAttachmentPointIndex + 1, removePositions); // excluding end-of-train
			_sep.RemoveRange(part.MaxRailAttachmentPointIndex + 1, removePositions + 1); // including end-of-train
			_sep.Add(RailGeom.CouplerLen() + part.DistanceBetweenLastAttachmentPointAndEndInTrainOrder()); // end-of-train
		}
		


		/**All positions will be set to the front of the train; you need to fix them after creating this.*/
		public static TrainPositions For(in TrainParts parts, TrackPosOr front) {
			var positions = new List<TrackPosOr>(parts._railAttachmentPoints + 2); // plus front and rear
			positions.RepeatedAdd(front, positions.Capacity);
			var separations = new List<float>(parts._railAttachmentPoints + 1); // plus rear

			var couplerLen = RailGeom.CouplerLen();
			var extraDistance = couplerLen;
			for(int p = 0; ; ) {
				var part = parts.Part(p);
				AddSeparations(part, separations, extraAtEnd: out extraDistance, extraDistanceBefore: extraDistance, sepIdx0: separations.Count);
				if(++p == parts.Parts) {
					separations.Add(extraDistance + couplerLen); // distance between last attachment point and end-of-train
					break;
				}
				extraDistance += 2 * couplerLen;
			}

			return new(positions, separations);
		}


		/**For each rail attachment point in the train part, adds the distance between that point and the previous one (closer to the front of the train) to the list.
		 * For the first rail attachment point in this part (or the last one if it's backwards),
		 * the location of the previous rail attachment point is given by extraDistanceBefore, which should be 0 for the first part in the train,
		 * and otherwise the distance between the front of this part (or the end if it's backwards) and the previous rail attachment point in the train,
		 * taking into account coupler distance between cars.
		 * The calculated distances will be added to the List starting at index sepIdx0.
		 * The distance between the "last" (in train order) rail attachment point in this part and the end of the part will be returned in extraAtEnd.*/
		private static void AddSeparations(
			TrainPart part,
			List<float> separations,
			out float extraAtEnd,
			float extraDistanceBefore,
			int sepIdx0
		) {
			if(part.IsFwd) {
				var prevDistance = -extraDistanceBefore;
				for(var i = 0; i < part.PositionCount; i++) {
					var d = part.Design.AttachmentDistances[i];
					separations.Insert(sepIdx0 + i, d - prevDistance);
					prevDistance = d;
				}
				extraAtEnd = part.Design.LengthExcludingCouplers - prevDistance;
			} else { // part is backward
				var prevDistance = part.Design.LengthExcludingCouplers + extraDistanceBefore;
				for(var i = part.Design.AttachmentDistances.LastIdx(); i >= 0; i--) {
					var d = part.Design.AttachmentDistances[i];
					separations.Insert(sepIdx0 + i, prevDistance - d);
					prevDistance = d;
				}
				extraAtEnd = prevDistance;
			}
		}
	}
}