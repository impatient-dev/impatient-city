namespace imp.city.world.rail.cars {
	public static class StandardTraincarDesigns {
		public static readonly TrainCarPart Boxcar =
			new BoxcarDesign(15f, 3f, 3.5f, 30f).AsPart();
		public static readonly TrainCarPart Flatcar =
			new FlatcarDesign(12.5f, 3f, 30f, 0.5f).AsPart();
		public static readonly TrainCarPart Switcher =
			BasicLocomotiveDesign.WithTractiveForceAsFractionOfWeight(8, 3, 3.5f, 75, 0.3f, 1000).AsPart();
	}


	/**A convenience class for locomotives.*/
	public class BasicLocomotiveDesign : StandardTrainCarDesign {
		public float Length { get; }
		public float Width { get; }
		public float Height { get; }
		public float Mass { get; }
		public float TractiveForce { get; }
		public float TractivePower { get; }
		public string TypeStr => "Locomotive";

		public BasicLocomotiveDesign(float length, float width, float height, float mass, float tractiveForce, float tractivePower) {
			Length = length;
			Width = width;
			Height = height;
			Mass = mass;
			TractiveForce = tractiveForce;
			TractivePower = tractivePower;
		}

		public static BasicLocomotiveDesign WithTractiveForceAsFractionOfWeight(float length, float width, float height, float mass, float tractiveForceMultiplier, float tractivePower) =>
			new(length: length, width: width, height: height, mass: mass, tractiveForce: 9.81f * mass * tractiveForceMultiplier, tractivePower: tractivePower);
	}



	/**A convenience class for unpowered freight cars.*/
	public class BasicFreightCarDesign : StandardTrainCarDesign {
		public float Length { get; }
		public float Width { get; }
		public float Height { get; }
		public float Mass { get; }
		public float TractiveForce => 0;
		public float TractivePower => 0;
		public string TypeStr { get; }

		public BasicFreightCarDesign(float length, float width, float height, float mass, string typeStr) {
			Length = length;
			Width = width;
			Height = height;
			Mass = mass;
			TypeStr = typeStr;
		}
	}
	


	public class BoxcarDesign : BasicFreightCarDesign {
		public BoxcarDesign(float length, float width, float height, float mass)
			: base(length: length, width: width, height: height, mass: mass, "Boxcar") {}
	}
	
	public class FlatcarDesign : BasicFreightCarDesign {
		public FlatcarDesign(float length, float width, float height, float mass)
			: base(length: length, width: width, height: height, mass: mass, "Flatcar") {}
	}
}