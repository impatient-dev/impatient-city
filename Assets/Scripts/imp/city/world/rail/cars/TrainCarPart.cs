using System.Numerics;
using imp.city.world.geom;

namespace imp.city.world.rail.cars {
	
	/**A design for a TrainPart that just consists of a single car. This is the normal / standard kind of train part.*/
	public class TrainCarPart : TrainPartDesign {
		public readonly StandardTrainCarDesign CarDesign;
		public int Cars => 1;
		public TrainCarDesign Car(int i) => CarDesign;

		public TrainCarPart(StandardTrainCarDesign car) {
			CarDesign = car;
			var halfBogieLen = RailGeom.BogieLength() / 2;
			AttachmentDistances = new[] {halfBogieLen, LengthExcludingCouplers - halfBogieLen};
		}

		public override string ToString() => $"TrainCarPart({CarDesign})";
		public float[] AttachmentDistances { get; }
		public float LengthExcludingCouplers => CarDesign.Length;
		public float Width => CarDesign.Width;
		public float Height => CarDesign.Height;
		public float Mass => CarDesign.Mass;
		public float TractiveForce => CarDesign.TractiveForce;
		public float TractivePower => CarDesign.TractivePower;
	}



	/**A normal train car, which has a bogie at each end and can function either independently or with the bogies being shared with the adjacent cars (Jacobs bogies).
	 * Most train cars are of this type; exceptions are articulated steam engines, articulated light rail or subway cars, and specialized permanently coupled sets.*/
	public interface StandardTrainCarDesign : TrainCarDesign {
		/**Excluding couplers.*/
		public float Length { get; }
		public float Width { get; }
		/**The highest point of the car that can collide with a bridge, tunnel, or overhead wire. Measured from the top of the rail.*/
		public float Height { get; }
		/**Mass in kilograms, excluding cargo, fuel, passengers, etc.*/
		public float Mass { get; }
		
		public float TractiveForce { get; }
		public float TractivePower { get; }

		/**The dimensions of a box that approximately matches the car, used for detecting clicks.
		 * X is the car width; Y is the car height, relative to the top of the rails; Z is the car length.*/
		public Vector3 SelectionBox => new(Width, Height, Length);
	}


	public static class StandardTrainCarDesignExtensions {
		public static TrainCarPart AsPart(this StandardTrainCarDesign design) => new(design);
	}
}