using imp.city.world.geom;

namespace imp.city.world.rail.cars {
	/**A design for a part of a train.
	 * A train part is a section of a train that can be decoupled and moved independently; it may be a single car, or a permanently-coupled grouping.*/
	public interface TrainPartDesign {
		/**The locations on this part (car) where it attaches to the rails, given as absolute distances from the front of the part, excluding couplers.
		 * These attachments are normally the centers of the bogies, but may be elsewhere for e.g. steam engines.
		 * The position and orientation of the car are derived from the positions of the rail attachment points.
		 * Valid numbers are &gt;= 0 and &lt;= the length of the part (car), excluding the couplers.*/
		public float[] AttachmentDistances { get; }
		
		/**The number of cars this part is divided into.*/
		public int Cars { get; }
		public TrainCarDesign Car(int idx);

		/**Including the couplers at both ends.*/
		public float TotalLength => LengthExcludingCouplers + RailGeom.CarSep();
		
		/**The length of the part/car, excluding the couplers at both ends (but including any internal couplers if this part contains multiple cars).*/
		public float LengthExcludingCouplers { get; }
		/**The farthest any part of the car is from the centerline of the car.*/
		public float Width { get; }
		/**The farthest any part of the car is from the top of the rails.*/
		public float Height { get; }
		
		
		/**Excluding any cargo, fuel, etc.*/
		public float Mass { get; }
		/**How much force can be produced to move the train forward, if this part contains any locomotives or multiple-unit cars.*/
		public float TractiveForce { get; }
		/**How much power can be produced to move the train forward, if this part contains any locomotives or multiple-unit cars.*/
		public float TractivePower { get; }
	}
}