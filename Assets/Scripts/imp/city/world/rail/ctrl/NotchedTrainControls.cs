using JetBrains.Annotations;
using UnityEngine;
using static imp.util.ImpatientUtil;

namespace imp.city.world.rail.ctrl {

	/**The train is controlled with a reverser and a throttle that can be set to a limited number of notches (e.g. 8 in North America).*/
	public class NotchedTrainControls : TrainControls {
		[NotNull] public TrainCar FromCar { get; private set; }
		public readonly int MaxThrottleNotch;
		public float Throttle { get; private set; }

		/**The reverser determines whether the throttle is applied forward or backward relative to this car / train part, not relative to the train as a whole.*/
		private TrainReverserState _reverser;
		public TrainReverserState Reverser {
			get => _reverser;
			set {
				if(value == _reverser)
					return;
				if(value == TrainReverserState.Neutral)
					ThrottleNotch = 0;
				else
					Throttle = -Throttle;
				_reverser = value;
			}
		}

		private int _throttleNotch;
		public int ThrottleNotch {
			get => _throttleNotch;
			set {
				if(Reverser == TrainReverserState.Forward)
					Throttle = Positive(FromCar.Part.IsFwd) * value / (float)MaxThrottleNotch;
				else if(Reverser == TrainReverserState.Reverse)
					Throttle = Positive(FromCar.Part.IsRev) * value / (float)MaxThrottleNotch;
				else
					Throttle = 0;
				_throttleNotch = value;
			}
		}

		public float FastBrake { get; set; } = 0;

		public void _onReverse() {
			ThrottleNotch = ThrottleNotch;
		}

		public NotchedTrainControls(TrainCar fromCar, int maxThrottleNotch) :
			this(fromCar, maxThrottleNotch, throttle: fromCar.Train.Controls.Throttle, fastBrake: fromCar.Train.Controls.FastBrake) {}

		public NotchedTrainControls(TrainCar fromCar, int maxThrottleNotch, float throttle = 0, float fastBrake = 0) {
			FromCar = fromCar;
			MaxThrottleNotch = maxThrottleNotch;
			FastBrake = fastBrake;

			if(throttle == 0)
				ThrottleNotch = 0;
			else
				ThrottleNotch = Mathf.RoundToInt(Positive(fromCar.Part.IsFwd) * throttle * maxThrottleNotch);
		}

		public void Disuse() {
			FromCar = null!;
			_throttleNotch = -1;
			FastBrake = float.NaN;
		}
	}
}