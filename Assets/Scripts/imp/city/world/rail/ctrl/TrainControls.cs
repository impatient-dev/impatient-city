using imp.city.u;

namespace imp.city.world.rail.ctrl {

	/**Describes/contains the controls (e.g. throttle) for a train.
	 * Implementation note: if you want to check whether a change would be redundant, put that check in WUTrainControls.*/
	public interface TrainControls : Disusable {
		/**The car that is controlling this train, if any. (E.g. the locomotive that has a crew driving.)*/
		public TrainCar? FromCar { get; }
		/**1 is max forward throttle (relative to the train); -1 is max reverse.*/
		public float Throttle { get; }
		/**0-1.*/
		public float FastBrake { get; }
		/**Notifies this controls object that the orientation of its TrainPart may have changed.*/
		public void _onReverse() {}
	}


	/**The state of the reverser control on a locomotive.*/
	public enum TrainReverserState {
		Reverse = -1,
		Neutral = 0,
		Forward = 1,
	}




	/**The train isn't currently being controlled, but the brakes may have been left on.*/
	public class NoTrainControls : TrainControls {
		public TrainCar? FromCar => null;
		public float Throttle => 0;
		public float FastBrake { get; private set; }

		public NoTrainControls(TrainControls copy) : this(fastBrake: copy.FastBrake) {}
		public NoTrainControls(float fastBrake = 0) { FastBrake = fastBrake; }

		public void Disuse() => FastBrake = float.NaN;
	}
}