using System;
using imp.city.world;
using imp.city.world.rail;

namespace imp.city.wu {
	/**Makes changes to train tracks and other rail infrastructure.*/
	public static class WURail {
		public static void CreateRailSeg(World chunk, RailSeg seg) {
			chunk.Add(seg);
		}

		/**Connect the end of one path (part of a rail segment) to the end of another path.
		 * Both ends must not be connected to anything currently.*/
		public static void ConnectRailSegEnds(World w, RailPath path1, Dir1E end1, RailPath path2, Dir1E end2) {
			// ensure both segments have IDs, and ensure the ends of these paths exist and aren't set to anything
			var conn1 = RailPathConn.Valid(w, path2, end2);
			var conn2 = RailPathConn.Valid(w, path1, end1);
			if(path1.Conn(end1).IsValid)
				throw new Exception($"Rail path {path1}, end {end1} is already connected to {path1.Conn(end1)}");
			if(path2.Conn(end2).IsValid)
				throw new Exception($"Rail path {path2}, end {end2} is already connected to {path2.Conn(end2)}");

			path1.Conn(end1) = conn1;
			path2.Conn(end2) = conn2;
		}
	}
}