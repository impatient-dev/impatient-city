using imp.city.wl;
using imp.city.world;

namespace imp.city.wu {
	/**Handles very-high-level updates to the world.*/
	public static class WURoot {
		/**Called once per frame to update everything that needs it.
		 * deltaTime is the number of seconds between frames.*/
		public static void Tick(World w, WorldListeners l, float deltaTime) {
			w.TickActiveTrains(deltaTime, l);
		}
	}
}