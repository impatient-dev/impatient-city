﻿using System;
using imp.city.error;
using imp.city.wl;
using imp.city.world;
using imp.city.world.rail;
using imp.city.world.rail.cars;
using imp.city.world.rail.ctrl;
using UnityEngine;
using static imp.util.ImpatientUtil;

namespace imp.city.wu {

	/**Makes changes to trains and train cars.*/
	public static class WUTrain {

		/**Spawns a new train.*/
		public static Train Spawn(World w, WorldListeners wl, TrainPartDesign partDesign, TrackPosOr front) {
			var trainPart = new TrainPart(partDesign, Dir1S.Front());
			var train = new Train(new[]{trainPart}, front, new NoTrainControls());
			WUTrainPos.Fix(w, train);
			w.Add(train);
			wl.Trains.OnAdd(train);
			return train;
		}

		/**Adds a part to the front or back of a train*/
		public static void SpawnAndAttach(World chunk, TrainPartDesign partDesign, Dir1S partFaces, Train train, Dir1E trainEnd) {
			var part = new TrainPart(partDesign, partFaces);
			part.Facing = partFaces;
			train.Add(new[]{part}, trainEnd);
			WUTrainPos.Fix(chunk, train);
		}

		/**Creates and returns a new train containing all parts after the provided one.*/
		public static Train SplitAfterPart(World w, WorldListeners wl, TrainPart part) {
			var train1 = part.Train!;
			CheckArg(part.TrainIdx + 1 < train1.Parts.Parts, "Cannot split a train after the last part.");
			
			var firstPartSplit = train1.Parts.Part(part.TrainIdx + 1);
			var front2 = firstPartSplit.ExtrapolatedRailPosAt(Dir1E.Start());
			TrainControls ctrl1 = train1.Controls, ctrl2 = new NoTrainControls(ctrl1);
			
			var splitParts = train1.SplitAfter(part);
			if(ctrl1.FromCar != null && splitParts.Contains(ctrl1.FromCar.Part)) {
				Swap(ref ctrl1, ref ctrl2);
				train1.Controls = ctrl1;
			}
			
			var train2 = new Train(splitParts, front2, ctrl2);
			WUTrainPos.Fix(w, train1);
			WUTrainPos.Fix(w, train2);
			w.Add(train2);
			wl.Trains.OnSplit(train1, train2);
			return train2;
		}

		/**A convenience method that splits apart a train at the boundary between 2 parts. The parts removed from this train will be in the returned train.*/
		public static Train SplitParts(World w, WorldListeners wl, TrainPart a, TrainPart b) {
			CheckArg(a != b, "Cannot split a train part from itself.");
			CheckArg(a.Train == b.Train, "Cannot split parts that are already in different trains.");
			if(a.TrainIdx + 1 == b.TrainIdx)
				return SplitAfterPart(w, wl, a);
			else if(a.TrainIdx - 1 == b.TrainIdx)
				return SplitAfterPart(w, wl, b);
			throw new Exception($"Cannot split non-adjacent parts {a.TrainIdx} and {b.TrainIdx} of {a.Train!.Parts.Parts}.");
		}
		
		/**Adjusts the velocity and position of an active train.*/
		public static void Tick(World chunk, WorldListeners wl, Train train, float deltaTime) {
			train.Velocity = NewVelocity(train, deltaTime);
			train.Pos.Front = train.Pos.Front.Forward(deltaTime * train.Velocity);
			try {
				WUTrainPos.Fix(chunk, train);
			} catch(PastEndOfTrackEx e) {
				HandleTrainReachedEndOfTrack(chunk, wl, train, e.Path, e.End);
			}
		}

		public static void SetVelocity(World world, WorldListeners l, Train train, float velocity) {
			train.Velocity = velocity;
			if(!train.Active && velocity != 0)
				world.MarkActive(train);
		}

		/**Call this after a change happens that should maybe result in the train being marked active.*/
		public static void MaybeMakeActive(World world, Train train) {
			if(!train.Active && train.ShouldBeActive())
				world.MarkActive(train);
		}


		private static float NewVelocity(Train train, float deltaTime) {
			var p = train.Performance;
			var c = train.Controls;
			var v0 = train.Velocity;
			var speed0 = v0.Abs();
			
			var engineForceAbs = p.EngineForcePositiveOnly(throttleAmount: c.Throttle.Abs(), speed: speed0) / p.Mass;
			var vAfterPower = v0 + deltaTime * Mathf.Sign(c.Throttle) * engineForceAbs;
			
			var dragForceAbs = p.RollingResistance + p.FastBrakeForce * c.FastBrake;
			var dragVChange = deltaTime * dragForceAbs / p.Mass;
			if(dragForceAbs > engineForceAbs) { // if the brakes are stronger than the engine, check whether we're coming to a stop
				if(dragVChange > vAfterPower.Abs())
					return 0;
			}
			return vAfterPower - Mathf.Sign(v0) * dragVChange;
		}



		/**Places the front of the train at the end-of-track (reversing the train if its rear reached the end-of-track) and reverses its velocity.*/
		private static void HandleTrainReachedEndOfTrack(World w, WorldListeners l, Train train, RailPath path, Dir1E endOfTrack) {
			// we want to place the front of the train at the end of the track; due to floating point imprecision, we can't place the back at exactly this point
			if(train.Velocity < 0) // if the rear went off track
				train.Reverse();
			train.Velocity = -train.Velocity / 2;
			train.Pos.Front = new TrackPosOr(new TrackPos(w, path, endOfTrack.IsStart ? 0 : path.Geom.Length), endOfTrack);
			WUTrainPos.Fix(w, train);
			l.Trains.OnReverse(train);
		}
	}
}