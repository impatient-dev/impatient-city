using imp.city.wl;
using imp.city.world;
using imp.city.world.rail;
using imp.city.world.rail.ctrl;

namespace imp.city.wu {
	/**Makes changes to the control inputs (throttle, brakes, etc.) for trains.*/
	public static class WUTrainControls {
		public static void Replace(World w, WorldListeners l, Train train, TrainControls newControls) {
			train.Controls.Disuse();
			train.Controls = newControls;
			l.Trains.OnControlsReplaced(train);
			WUTrain.MaybeMakeActive(w, train);
		}


		public static void SetReverser(World w, WorldListeners l, NotchedTrainControls control, TrainReverserState reverser) {
			control.Reverser = reverser;
			l.Trains.OnControlsInputChange(control.FromCar.Train!);
		}

		public static void SetThrottleNotch(World w, WorldListeners l, NotchedTrainControls control, int notch) {
			control.ThrottleNotch = notch;
			l.Trains.OnControlsInputChange(control.FromCar.Train!);
			WUTrain.MaybeMakeActive(w, control.FromCar.Train);
		}

		public static void SetFastBrake(World w, WorldListeners l, NotchedTrainControls control, float brake) {
			control.FastBrake = brake;
			l.Trains.OnControlsInputChange(control.FromCar.Train!);
		}
	}
}