using imp.city.error;
using imp.city.world;
using imp.city.world.rail;

namespace imp.city.wu {
	/**Handles updating the position of a train.*/
	public static class WUTrainPos {
		/**Updates all positions except the first one, making them the correct distances from each other.
		 * <exception cref="PastEndOfTrackEx">if some calculated position cannot be placed on a piece of track</exception>*/
		public static void Fix(World chunk, Train train) {
			var pos = train.Pos.Front;
			var separations = train.Pos.Separations;

			for(var i = 0; i < separations.Count; i++) {
				var d = separations[i];
				pos = pos.Backward(d).FixedPos(chunk);
				train.Pos.SetPos(i, pos);
			}
		}


		/** If the position is valid, it is returned with no changes.
		 *  But if it's beyond the start/end of its path, this will return the actual position, on the correct segment and path.
		 *  <exception cref="PastEndOfTrackEx">if the position is beyond the start/end of some segment and that end doesn't connect to anything</exception>*/
		private static TrackPosOr FixedPos(this TrackPosOr pos, World chunk) {
			var path = pos.Pos.Path!;
			while (true) {
				if (pos.Pos.DistanceFromStart < 0) { // went past the start of this path
					var conn = pos.Pos.Path.StartConn;
					if (!conn.IsValid)
						throw new PastEndOfTrackEx(pos.Pos.Path, Dir1E.Start(), -pos.Pos.DistanceFromStart);
					path = conn.Path!;
					var newDistance = conn.End.IsStart ? -pos.Pos.DistanceFromStart : path.Geom.Length + pos.Pos.DistanceFromStart;
					var newFacing = pos.Facing;
					if(conn.End.IsStart) // reverse direction at start-to-start connection
						newFacing = !newFacing;
					pos = new TrackPosOr(new TrackPos(chunk, conn.Path!, newDistance), newFacing);
				} else if (pos.Pos.DistanceFromStart > path.Geom.Length) { // went past the end of this path
					var distancePastEnd = pos.Pos.DistanceFromStart - path.Geom.Length;
					var conn = pos.Pos.Path.EndConn;
					if(!conn.IsValid)
						throw new PastEndOfTrackEx(pos.Pos.Path, Dir1E.End(), distancePastEnd);
					path = conn.Path!;
					var newDistance = conn.End.IsStart ? distancePastEnd : path.Geom.Length - distancePastEnd;
					var newFacing = pos.Facing;
					if(conn.End.IsEnd) // reverse direction at end-to-end connection
						newFacing = !newFacing;
					pos = new TrackPosOr(new TrackPos(chunk, conn.Path!, newDistance), newFacing);
				} else {
					return pos;
				}
			}
		}
	}
}