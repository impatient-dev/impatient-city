namespace imp.util {
	/**Basically a boolean, where the 2 options are stop/continue.
	 * (Continue is called "Go" because it can't be the same as the struct name.*/
	public readonly struct Continue {
		private readonly bool _stop;

		private Continue(bool stop) {
			_stop = stop;
		}

		public static readonly Continue CONTINUE = new Continue(false);
		public static readonly Continue STOP = new Continue(true);

		public static Continue GoIf(bool go) => go ? CONTINUE : STOP;
		public static Continue StopIf(bool stop) => stop ? STOP : CONTINUE;
		
		/**Checks whether this is the stop option.*/
		public bool Stop {
			get => _stop;
		}
		/**Checks whether this is the continue option.*/
		public bool Go {
			get => !_stop;
		}
	}
}