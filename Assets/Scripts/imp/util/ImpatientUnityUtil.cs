﻿using System;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static System.Runtime.CompilerServices.MethodImplOptions;
using Object = UnityEngine.Object;


namespace imp.util {

	/**Utilities for working with Unity.
	 * AR means AddRequired, and returns the created component. W means With, and returns this.
	 * Generally a component has either an AR or a W function, not both - whichever is the more typical usage.*/
	public static class ImpatientUnityUtil {
		
		
		
		// MATH & VECTORS

		[MethodImpl(AggressiveInlining)] public static Vector2 V2(float x, float y) => new Vector2(x, y);
		[MethodImpl(AggressiveInlining)] public static Vector2Int V2I(int x, int y) => new Vector2Int(x, y);
		[MethodImpl(AggressiveInlining)] public static Vector3 V3(float x, float y, float z = 0) => new Vector3(x, y, z);
		[MethodImpl(AggressiveInlining)] public static Vector3 V3xz(float x, float z) => new Vector3(x, 0, z);

		/**Adds a Z component.*/
		[MethodImpl(AggressiveInlining)] public static Vector3 Augment(this Vector2 vec, float z = 0) => new Vector3(vec.x, vec.y, z);
		/**Adds a Y component, and shifts the current Y to Z.*/
		[MethodImpl(AggressiveInlining)] public static Vector3 AugShift(this Vector2 vec, float y = 0) => new Vector3(vec.x, y, vec.y);
		
		/**Removes the Z component.*/
		[MethodImpl(AggressiveInlining)] public static Vector2 xy(this Vector3 vec) => new Vector2(vec.x, vec.y);
		/**Removes the Y component, turning Z into Y.*/
		[MethodImpl(AggressiveInlining)] public static Vector2 xz(this Vector3 vec) => new Vector2(vec.x, vec.z);
		
		[MethodImpl(AggressiveInlining)] public static Vector3 WithZ(this Vector3 vec, float z) => new Vector3(vec.x, vec.y, z);

		/**Takes the absolute value of each component.*/
		[MethodImpl(AggressiveInlining)] public static Vector2Int Abs(this Vector2Int vec) => new Vector2Int(Math.Abs(vec.x), Math.Abs(vec.y));

		[MethodImpl(AggressiveInlining)] public static float Dot(this Vector3 a, Vector3 b) => Vector3.Dot(a, b);

		/**Rotates 90 degrees counter-clockwise (left).*/
		public static Vector2 RotL(this Vector2 vec) => new(-vec.y, vec.x);
		/**Rotates 90 degrees clockwise (right).*/
		public static Vector2 RotR(this Vector2 vec) => new(vec.y, -vec.x);
		/**Rotates 90 degrees counter-clockwise (left) or clockwise (right).*/
		public static Vector2 RotL(this Vector2 vec, bool left) => left ? vec.RotL() : vec.RotR();

		/**Returns true if b is to the right of a and false if b is to the left.
		 * The result is undefined for vectors that are identical or opposite in orientation.*/
		public static bool VecIsRight(Vector2 a, Vector2 b) => Vector3.Cross(a.Augment(), b.Augment()).z > 0;
		
		
		
		// QUATERNIONS

		/**Creates a rotation for use in 2D space, causing the X-axis to point in the specified direction.
		 * The provided vector doesn't have to be a unit vector.*/
		public static Quaternion RotX(Vector2 xAxis) => Quaternion.LookRotation(Vector3.forward, xAxis.RotR());



		// UNITY GAME OBJECTS

		/**Creates a child GameObject with a specific name and a local transform of identity.
		 * The child's position and rotation will be relative to the parent (worldPositionStays = false).*/
		public static GameObject AddChild(this GameObject parent, string childName) {
			var child = new GameObject(childName);
			child.transform.SetParent(parent.transform, false);
			return child;
		}

		/**Finds a component. Throws an exception if the component is not found, instead of returning null.*/
		public static T Require<T>(this GameObject obj) where T : Component => obj.GetComponent<T>() ?? throw new Exception($"Component not found on object {obj.name}");

		/**Marks this GameObject, Component, or asset to be destroyed later this frame.
		 * If you need to add components that might conflict with this one on the same frame, this will not work (even if the component is also disabled).
		 * Unity has a DestroyImmediate function, but it's not recommended for use except in editor code.*/
		public static void Destroy(this Object obj) => Object.Destroy(obj);

		/**Sets the local position, relative to any parent transform. Returns this.*/
		public static GameObject LocalPos(this GameObject obj, Vector3 pos) {
			obj.transform.localPosition = pos;
			return obj;
		}
		/**Sets the local rotation, relative to any parent transform. Returns this.*/
		public static GameObject LocalRot(this GameObject obj, Quaternion rot) {
			obj.transform.localRotation = rot;
			return obj;
		}
		/**Sets the local position and rotation, relative to any parent transform. Returns this.*/
		public static GameObject LocalTransform(this GameObject obj, Vector3 pos, Quaternion rot) {
			var transform = obj.transform;
			transform.position = pos;
			transform.rotation = rot;
			return obj;
		}
		
		/**Sets the transform's local position to (0,0,0) and its rotation to identity.
		 * Does not change whether or not the transform has a parent.*/
		public static void Reset(this Transform transform) {
			transform.localPosition = Vector3.zero;
			transform.rotation = Quaternion.identity;
		}

		/**Sets the transform's position to (0,0,0), its rotation to identity, and its parent to nothing.*/
		public static void DetachAndReset(this Transform transform) {
			transform.parent = null;
			transform.localPosition = Vector3.zero;
			transform.rotation = Quaternion.identity;
		}

		
		
		// ADD COMPONENTS
		//alphabetical by component

		/**Adds and returns a component. The component is required - an exception will be thrown if the component is not added.*/
		public static T AR<T>(this GameObject obj) where T : Component => obj.AddComponent<T>() ?? throw new Exception("Component was  not added to game object " + obj.name);
		
		public static T GetOrCreateComponent<T>(this GameObject obj) where T : Component {
			var comp = obj.GetComponent<T>();
			if(comp == null)
				comp = obj.AR<T>();
			return comp;
		}

		public static void DestroyComponentIfExists<T>(this GameObject obj) where T : Component {
			var comp = obj.GetComponent<T>();
			if(comp != null)
				comp.Destroy();
		}

		/**Adds an Image component.
		 * Implementation note: if the GameObject has a RectTransform, adding an Image component causes Unity to stupidly set random offsets on the RectTransform
		 * and cause it to appear off the screen. This function undoes that.*/
		public static Image ARImage(this GameObject obj) {
			var rect = obj.GetComponent<RectTransform>();
			Image img;
			
			if(rect == null) {
				img = obj.AR<Image>();
			} else {
				Vector2 offsets0 = rect.offsetMin, offsets1 = rect.offsetMax;
				img = obj.AR<Image>();
				rect.offsetMin = offsets0;
				rect.offsetMax = offsets1;
			}
			
			return img;
		}
		/**Gets or creates an Image component. If it's disabled, this function will enable it.*/
		public static Image ProvideEnabledImage(this GameObject u) {
			var img = u.GetComponent<Image>();
			if(img != null)
				return img.Enabled();
			return u.ARImage();
		}
		/**Adds an Image to serve as a background.*/
		public static Image ARBackground(this GameObject obj, Color color) {
			var img = obj.ARImage();
			img.color = color;
			return img;
		}
		/**Adds an Image to serve as a background.*/
		public static GameObject WBackground(this GameObject obj, Color color) {
			obj.ARImage().color = color;
			return obj;
		}
		
		/**Adds a Canvas. Also adds a GraphicRaycaster, because otherwise the canvas doesn't work as a UI.*/
		public static GameObject WCanvas(this GameObject obj, RenderMode renderMode = RenderMode.ScreenSpaceOverlay) {
			obj.AR<Canvas>().renderMode = renderMode;
			obj.AR<GraphicRaycaster>(); 
			return obj;
		}

		/**Adds an EdgeCollide2D with the provided points.*/
		public static EdgeCollider2D AREdgeCollider2d(this GameObject obj, Vector2[] points, float width) {
			var ret = obj.AR<EdgeCollider2D>();
			ret.points = points;
			ret.edgeRadius = width;
			return ret;
		}

		/**Adds and returns a HorizontalLayoutGroup.*/
		public static HorizontalLayoutGroup ARHorizontal(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) =>
			obj.AR<HorizontalLayoutGroup>().Set(childAlign);
		/**Adds a HorizontalLayoutGroup.*/
		public static GameObject WHorizontal(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter, int spacing = 0) {
			var row = obj.AR<HorizontalLayoutGroup>().Set(childAlign);
			row.spacing = spacing;
			return obj;
		}
		/**Adds a HorizontalLayoutGroup with padding.*/
		public static GameObject WHorizontal(this GameObject obj, RectOffset padding, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<HorizontalLayoutGroup>().Set(childAlign).padding = padding;
			return obj;
		}
		
		/**Adds a HorizontalLayoutGroup with childForceExpandWidth.*/
		public static GameObject WHorizontalWide(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<HorizontalLayoutGroup>().Set(childAlign, childForceExpandWidth: true);
			return obj;
		}
		/**Adds a HorizontalLayoutGroup with childForceExpandHeight.*/
		public static GameObject WHorizontalTall(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<HorizontalLayoutGroup>().Set(childAlign, childForceExpandHeight: true);
			return obj;
		}
		/**Adds a HorizontalLayoutGroup with childForceExpandWidth and childForceExpandHeight.*/
		public static GameObject WHorizontalMax(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<HorizontalLayoutGroup>().Set(childAlign, childForceExpandWidth: true, childForceExpandHeight: true);
			return obj;
		}

		/**Creates a LineRenderer with all information necessary to display some lines.*/
		public static LineRenderer ARLine(this GameObject obj, float width, Material material, params Vector3[] positions) {
			var ret = obj.AR<LineRenderer>();
			ret.useWorldSpace = false;
			ret.startWidth = ret.endWidth = width;
			ret.material = material;
			ret.positionCount = positions.Length; // it only displays the first 2 positions without this
			ret.SetPositions(positions);
			return ret;
		}
		/**Creates a LineRenderer with the position count specified, but not any of the actual positions - you have to specify them later.*/
		public static LineRenderer ARLine(this GameObject obj, float width, Material material, int positionCount) {
			var ret = obj.AR<LineRenderer>();
			ret.useWorldSpace = false;
			ret.startWidth = ret.endWidth = width;
			ret.material = material;
			ret.positionCount = positionCount;
			return ret;
		}

		public static MeshRenderer ARMesh(this GameObject obj, Mesh mesh, Material? material = null) {
			obj.AR<MeshFilter>().mesh = mesh;
			var ret = obj.AR<MeshRenderer>();
			if(material != null)
				ret.material = material;
			return ret;
		}

		public static SpriteRenderer ARSprite(this GameObject obj, Sprite sprite, Material material, MaterialPropertyBlock? props) {
			var ret = obj.AR<SpriteRenderer>();
			ret.sprite = sprite;
			ret.material = material;
			ret.SetPropertyBlock(props);
			return ret;
		}

		/**Creates text that appears in the world (not in a Canvas). ("G" = general)
		 * fontSize should be the approximate desired height in world coordinates - we'll multiply this by 10 and send it to TextMeshPro.*/
		public static TextMeshPro ARGText(this GameObject obj, TMP_FontAsset font, Color color, float fontSize, string text = "",
			TextAlignmentOptions alignment = TextAlignmentOptions.Center, FontStyles style = FontStyles.Normal) {
			var ret = obj.AR<TextMeshPro>();
			ret.font = font;
			ret.color = color;
			ret.fontSize = fontSize * 10;
			ret.text = text;
			ret.alignment = alignment;
			ret.fontStyle = style;
			return ret;
		}

		/**Adds and returns a VerticalLayoutGroup.*/
		public static VerticalLayoutGroup ARVertical(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) =>
			obj.AR<VerticalLayoutGroup>().Set(childAlign);
		/**Adds a VerticalLayoutGroup.*/
		public static GameObject WVertical(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<VerticalLayoutGroup>().Set(childAlign);
			return obj;
		}
		/**Adds a VerticalLayoutGroup with padding.*/
		public static GameObject WVertical(this GameObject obj, RectOffset padding, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<VerticalLayoutGroup>().Set(childAlign).padding = padding;
			return obj;
		}
		
		/**Adds a VerticalLayoutGroup with childForceExpandWidth.*/
		public static GameObject WVerticalWide(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<HorizontalLayoutGroup>().Set(childAlign, childForceExpandWidth: true);
			return obj;
		}
		/**Adds a VerticalLayoutGroup with childForceExpandHeight.*/
		public static GameObject WVerticalTall(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<HorizontalLayoutGroup>().Set(childAlign, childForceExpandHeight: true);
			return obj;
		}
		/**Adds a VerticalLayoutGroup with childForceExpandWidth and childForceExpandHeight.*/
		public static GameObject WVerticalMax(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<HorizontalLayoutGroup>().Set(childAlign, childForceExpandWidth: true, childForceExpandHeight: true);
			return obj;
		}
		
		
		
		// CUSTOMIZE COMPONENTS
		//alphabetical by component

		public static C AsTrigger<C>(this C collider) where C : Collider {
			collider.isTrigger = true;
			return collider;
		}
		public static C AsTrigger2<C>(this C collider) where C : Collider2D {
			collider.isTrigger = true;
			return collider;
		}

		public static BoxCollider AsTrigger(this BoxCollider collider, Vector3 size) {
			collider.isTrigger = true;
			collider.size = size;
			return collider;
		}

		public static BoxCollider2D AsTrigger(this BoxCollider2D collider, Vector2 size) {
			collider.isTrigger = true;
			collider.size = size;
			return collider;
		}

		/**Enables or disables the component.*/
		public static C Enabled<C>(this C component, bool enabled = true) where C : Behaviour {
			component.enabled = enabled;
			return component;
		}

		/**Returns null if the component is disabled.*/
		public static C? DisabledToNull<C>(this C component) where C : Behaviour =>
			component.enabled ? component : null;

		public static ContentSizeFitter Set(this ContentSizeFitter fitter, ContentSizeFitter.FitMode horizontal, ContentSizeFitter.FitMode vertical) {
			fitter.horizontalFit = horizontal;
			fitter.verticalFit = vertical;
			return fitter;
		}
		
		/**Convenience function to set everything we normally care about in a layout component, with sensible defaults. (Some of the Unity defaults are not sensible.)*/
		public static LayoutGroup Set<LayoutGroup>(this LayoutGroup layout,
			TextAnchor childAlign = TextAnchor.MiddleCenter,
			bool childControlsWidth = true,
			bool childControlsHeight = true,
			bool childForceExpandWidth = false,
			bool childForceExpandHeight = false
		) where LayoutGroup : HorizontalOrVerticalLayoutGroup {
			layout.childAlignment = childAlign;
			layout.childControlWidth = childControlsWidth;
			layout.childControlHeight = childControlsHeight;
			layout.childForceExpandWidth = childForceExpandWidth;
			layout.childForceExpandHeight = childForceExpandHeight;
			return layout;
		}

		public static LineRenderer Positions(this LineRenderer lr, params Vector3[] positions) {
			lr.SetPositions(positions);
			lr.positionCount = positions.Length;
			return lr;
		}

		public static RectTransform Anchors(this RectTransform rect, float x0, float x1, float y0, float y1) {
			rect.anchorMin = new(x0, y0);
			rect.anchorMax = new(x1, y1);
			return rect;
		}
		
		public static RectTransform NoOffsets(this RectTransform rect) {
			rect.offsetMin = rect.offsetMax = new();
			return rect;
		}
		public static RectTransform Offsets(this RectTransform rect, float x0, float y0, float x1, float y1) {
			rect.offsetMin = new(x0, y0);
			rect.offsetMax = new(x1, y1);
			return rect;
		}

		public static RectTransform Pivot(this RectTransform rect, float x, float y) {
			rect.pivot = new(x, y);
			return rect;
		}

		public static RectTransform Size(this RectTransform rect, Vector2 size) {
			rect.sizeDelta = size;
			return rect;
		}
		public static RectTransform Size(this RectTransform rect, float x, float y) => rect.Size(new(x, y));
		
		
		// RESOURCES
		
		/**Creates a gray color, where red/green/blue are the same.*/
		public static Color Gray(float rgb, float a = 1) => new Color(rgb, rgb, rgb, a);

		/**Creates a color from a hexidecimal number. The alpha is always the max value.*/
		public static Color32 HexColor(int hex) {
			var r = (hex & 0xff0000) >> 16;
			var g = (hex & 0xff00) >> 8;
			var b = hex & 0xff;
			return new Color32((byte)r, (byte)g, (byte)b, byte.MaxValue);
		}

		public static Color Alpha(this Color color, float alpha) => new(color.r, color.g, color.b, alpha);
		
		/**Throws an exception if the resource isn't found.*/
		public static T LoadResource<T>(string path) where T : UnityEngine.Object => Resources.Load<T>(path) ?? throw new Exception($"Resource not found: {path}");
		/**If the string is null, this returns null; otherwise it loads the resource and ensures it is found.*/
		public static T? LoadResourceIf<T>(string? path) where T : UnityEngine.Object => path == null ? null : LoadResource<T>(path);
		
		/**Creates a TextMeshPro font.*/
		public static TMP_FontAsset Pro(this Font font) => TMP_FontAsset.CreateFontAsset(font);



		// UI

		/**Figures out whether the user is hovering over a canvas/UI element.
		 * Returns false if there is no current EventSystem, or if the current EventSystem says the mouse is not hovering over it.*/
		public static bool PointingAtUi() {
			var sys = EventSystem.current;
			if (sys == null)
				return false;
			return sys.IsPointerOverGameObject();
		}

		public static RectOffset SquareOffset(int side) => new(side, side, side, side);
		public static RectOffset SquareOffset(float side) => SquareOffset(Mathf.RoundToInt(side));
		/**Creates a RectOffset with different horizontal and vertical offsets.*/
		public static RectOffset RectOffsetHV(float h, float v) {
			var horizontal = Mathf.RoundToInt(h);
			var vertical = Mathf.RoundToInt(v);
			return new(left: horizontal, right: horizontal, top: vertical, bottom: vertical);
		}
	}

}