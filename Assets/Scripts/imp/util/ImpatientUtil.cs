﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using UnityEngine;
using static System.Runtime.CompilerServices.MethodImplOptions;
using Random = System.Random;

namespace imp.util {

	/**General C# utilities unrelated to Unity.*/
	public static class ImpatientUtil {

		public const char NDASH = '–', MDASH = '—';
	
		// GENERAL

		/**Returns the object, but calls your action on it first.*/
		[MethodImpl(AggressiveInlining)]
		public static T Also<T>(this T obj, Action<T> action) {
			action.Invoke(obj);
			return obj;
		}

		/**Throws ArgumentException with the provided message if the condition is not true.*/
		[MethodImpl(AggressiveInlining)]
		public static void CheckArg(bool condition, Func<string> message) {
			if(!condition)
				throw new ArgumentException(message.Invoke());
		}
		/**Throws ArgumentException with the provided message if the condition is not true.*/
		[MethodImpl(AggressiveInlining)]
		public static void CheckArg(bool condition, string message) {
			if(!condition)
				throw new ArgumentException(message);
		}

		public static void Swap<T>(ref T a, ref T b) {
			(a, b) = (b, a);
		}
		
		/**Throws an exception. Put this where you plan to write code later.*/
		public static T TODO<T>() => throw new NotImplementedException("This code has not been written yet.");
		/**Throws an exception because we reached some code it should not be possible to reach.*/
		public static void Unreachable() => throw new Exception("It should not be possible to reach this code.");
		/**Throws an exception because we reached some code it should not be possible to reach.*/
		public static T UnreachableR<T>() => throw new Exception("It should not be possible to reach this code.");
		/**Throws an exception because we reached some code it should not be possible to reach.*/
		public static void Unexpected(string message) => throw new Exception("Unexpected error: " + message);
		/**Throws an exception because we reached some code it should not be possible to reach.*/
		public static T UnexpectedR<T>(string message) => throw new Exception("Unexpected error: " + message);
		/**Throws an exception because we encountered a type we do not know how to handle.*/
		public static void UnexpectedType(object? thing) => throw new Exception("Unexpected type: " + thing?.GetType());
		/**Throws an exception because we encountered a type we do not know how to handle.*/
		public static R UnexpectedTypeR<R>(object? thing) {
			UnexpectedType(thing);
			return UnreachableR<R>();
		}



		// ARRAYS

		[MethodImpl(AggressiveInlining)] public static bool IsEmpty<T>(this T[] array) => array.Length == 0;
		[MethodImpl(AggressiveInlining)] public static bool IsNotEmpty<T>(this T[] array) => array.Length > 0;

		/**Returns the last index in the array. Returns -1 for an empty array.*/
		[MethodImpl(AggressiveInlining)]
		public static int LastIdx<T>(this T[] array) => array.Length - 1;
		/**Returns the last item in the array. The array must not be empty.*/
		[MethodImpl(AggressiveInlining)]
		public static ref T Last<T>(this T[] array) => ref array[^1];
		/**Returns the first index if false, or the last index if true. The array must not be empty.*/
		[MethodImpl(AggressiveInlining)]
		public static int EndIdx<T>(this T[] array, bool last) => last ? array.Length - 1 : 0;
		/**Returns the first item if false, or last if true. The array must not be empty.*/
		[MethodImpl(AggressiveInlining)]
		public static ref T End<T>(this T[] array, bool last) => ref array[last ? array.Length - 1 : 0];

		/**Finds and returns the index of the value, or else throws an exception.*/
		public static int RequireIdx<T>(this T[] array, T value) {
			for(var c = 0; c < array.Length; c++)
				if (Equals(array[c], value))
					return c;
			throw new Exception($"Value not found in array (len={array.Length}): {value}");
		}

		/**Returns whether this is a valid index in the array.*/
		public static bool Valid<T>(this T[] array, int index) => index >= 0 && index < array.Length;

		public static void SetAll<T>(this T[] array, T value) {
			for(var i = 0; i < array.Length; i++)
				array[i] = value;
		}
	
		/**Returns the provided array if its length matches desired.
		 * Otherwise, returns a new array of the correct size, with entries added or removed using the provided functions.*/
		public static T[] Resize<T>(this T[] array, int desiredLength, Func<T> factory, Action<T>? onDelete = null) {
			if (array.Length == desiredLength)
				return array;
			var ret = new T[desiredLength];
			Array.Copy(array, ret, Math.Min(array.Length, desiredLength));
			if (array.Length < desiredLength)
				for (var c = array.Length; c < desiredLength; c++)
					ret[c] = factory.Invoke();
			else
				for(var c = desiredLength; c < array.Length; c++)
					onDelete?.Invoke(array[c]);
			return ret;
		}

		/**Returns another array of the same length, produced by applying the provided function to each entry in the source array.*/
		public static T[] Map<S, T>(this S[] source, Func<S, T> transform) {
			var ret = new T[source.Length];
			for (var c = 0; c < source.Length; c++)
				ret[c] = transform.Invoke(source[c]);
			return ret;
		}

		/**Returns a list of up to the same size, or less. Populates the list by applying the transformation to each item in the array,
		 * skipping any items where the result is null.*/
		public static List<U> MapOpt<T, U>(this T[] array, Func<T, U> transform) where U : class {
			var ret = new List<U>(array.Length);
			foreach(var item in array) {
				var result = transform.Invoke(item);
				if(result != null)
					ret.Add(result);
			}
			return ret;
		}
		
		
		
		// LISTS

		public static List<T> ListOf<T>(params T[] array) {
			var ret = new List<T>(array.Length);
			foreach(var item in array)
				ret.Add(item);
			return ret;
		}

		public static void AddAll<T>(this List<T> list, IReadOnlyCollection<T> all) {
			foreach(var item in all)
				list.Add(item);
		}
		public static void RepeatedAdd<T>(this List<T> list, T item, int times) {
			for(var i = 0; i < times; i++)
				list.Add(item);
		}
		public static void RepeatedInsert<T>(this List<T> list, int index, T item, int times) {
			for(var i = 0; i < times; i++)
				list.Insert(index, item);
		}

		[MethodImpl(AggressiveInlining)] public static bool IsEmpty<T>(this IReadOnlyList<T> list) => list.Count == 0;
		[MethodImpl(AggressiveInlining)] public static bool IsNotEmpty<T>(this IReadOnlyList<T> list) => list.Count > 0;

		/**Throws if the index is invalid or the item is null.*/
		public static T Req<T>(this IReadOnlyList<T> list, int idx) where T : class {
			var ret = list[idx];
			if(ret == null)
				throw new Exception($"Null item at index {idx}.");
			return ret;
		}
		
		
		[MethodImpl(AggressiveInlining)] public static T? LastOpt<T>(this IReadOnlyList<T> list) where T : class => list.IsEmpty() ? null : list[^1];
		/**Returns the last item in the list. The list must not be empty.*/
		[MethodImpl(AggressiveInlining)] public static T LastReq<T>(this IReadOnlyList<T> list) => list[^1];
		/**Returns the last index in the list. Returns -1 for an empty list.*/
		[MethodImpl(AggressiveInlining)] public static int LastIdx<T>(this IReadOnlyList<T> list) => list.Count - 1;
		
		/**Returns the first index if false, or the last index if true. The list must not be empty.*/
		[MethodImpl(AggressiveInlining)] public static int EndIdx<T>(this IReadOnlyList<T> list, bool last) => last ? list.Count - 1 : 0;
		/**Returns the first item if false, or last if true. The list must not be empty.*/
		[MethodImpl(AggressiveInlining)] public static T End<T>(this IReadOnlyList<T> list, bool last) => list[last ? list.Count - 1 : 0];
		/**Returns true if all capacity is currently in use (so the next Add will require the capacity to expand).*/
		[MethodImpl(AggressiveInlining)] public static bool AtCapacity<T>(this List<T> list) => list.Count == list.Capacity;
		
		public static void RemoveLast<T>(this List<T> list) => list.RemoveAt(list.LastIdx());
		
		public static List<T> GetAndRemoveRange<T>(this List<T> list, int index, int count) {
			var ret = list.GetRange(index, count);
			list.RemoveRange(index, count);
			return ret;
		}
		/**Removes all elements starting at the provided one. Returns a list of the removed elements.*/
		public static List<T> SplitFrom<T>(this List<T> list, int idx) => list.GetAndRemoveRange(idx, list.Count - idx);
		
		
		
		// SETS

		[MethodImpl(AggressiveInlining)] public static bool IsEmpty<T>(this HashSet<T> set) => set.Count == 0;
		[MethodImpl(AggressiveInlining)] public static bool IsNotEmpty<T>(this HashSet<T> set) => set.Count > 0;
		/**Adds or removes the item, depending on the bool argument. Returns whether the set changed.*/
		public static bool Set<T>(this HashSet<T> set, T item, bool add) => add ? set.Add(item) : set.Remove(item);
		
		
		
		// DICTIONARIES

		/**Returns the value with this key. If there is none, uses the creator to create a value and add it to the dictionary first.*/
		public static V GetOrCreate<K, V>(this Dictionary<K, V> map, K key, Func<V> valueCreator) {
			V value;
			if (!map.TryGetValue(key, out value)) {
				value = valueCreator.Invoke();
				map[key] = value;
			}
			return value;
		}

		/**Returns null if the key isn't present.*/
		public static V? Opt<K, V>(this Dictionary<K, V> map, K key) where V : class {
			if (map.TryGetValue(key, out var value))
				return value;
			return null;
		}
		/**Returns the default value if the key isn't present.*/
		public static V? OrDefault<K, V>(this Dictionary<K, V> map, K key, V? defaultValue = default) {
			if (map.TryGetValue(key, out var value))
				return value;
			return defaultValue;
		}

		/**Throws an error if the key is already present.*/
		public static void PutNew<K, V>(this Dictionary<K, V> map, K key, [DisallowNull] V value) {
			if(map.TryGetValue(key, out var current))
				throw new Exception($"Dictionary already contains key {key}, with value {current}.");
			map[key] = value;
		}

		/**Throws an error if the key isn't found, and includes the key in the message (unlike the [] message).*/
		public static V Require<K, V>(this Dictionary<K, V> map, [DisallowNull] K key) {
			if (map.TryGetValue(key, out var value))
				return value;
			throw new Exception($"Key {key} not found.");
		}

		/**Generates a new random unsigned int that is not currently in the map, returns it, and stores the provided value under this key. TODO move to rand data gen*/
		public static uint PutRandKey<V>(this Dictionary<uint, V> map, V value) {
			for (var c = 0; c < 10; c++) {
				var key = ThreadLocalRandom.Current.NextUint(1);
				if (!map.ContainsKey(key)) {
					map[key] = value;
					return key;
				}
			}
			throw new Exception("Failed to find a unique uint to use as a key in a map.");
		}

		/**Creates a new map with different values. This variant uses a 1-argument function.*/
		public static Dictionary<K, V2> MapValues1<K, V, V2>(this Dictionary<K, V> map, Func<V, V2> transform) {
			var ret = new Dictionary<K, V2>(map.Count);
			foreach(var entry in map)
				ret[entry.Key] = transform.Invoke(entry.Value);
			return ret;
		}
		/**Creates a new map with different values. This variant provides the key to the function.*/
		public static Dictionary<K, V2> MapValuesK<K, V, V2>(this Dictionary<K, V> map, Func<K, V, V2> transform) {
			var ret = new Dictionary<K, V2>(map.Count);
			foreach(var entry in map)
				ret[entry.Key] = transform.Invoke(entry.Key, entry.Value);
			return ret;
		}



		// OTHER COLLECTIONS

		/**Returns the first item matching the predicate, or null if no items match.*/
		public static T? FirstOpt<T>(this IEnumerable<T> en, Predicate<T> func) where T : class {
			foreach(var item in en)
				if(func.Invoke(item))
					return item;
			return null;
		}

		/**Returns the first item, or null if this is empty.*/
		public static T? FirstOpt<T>(this IEnumerable<T> en) where T : class {
			var e = en.GetEnumerator();
			try {
				if(e.MoveNext())
					return e.Current;
				return null;
			} finally {
				e.Dispose();
			}
		}

		/**Returns the first item, or throws if this is empty.*/
		public static T FirstReq<T>(this IEnumerable<T> en) {
			var e = en.GetEnumerator();
			try {
				if(e.MoveNext())
					return e.Current;
				throw new Exception("IEnumerable is empty.");
			} finally {
				e.Dispose();
			}
		}
		
		
		
		// MATH

		[MethodImpl(AggressiveInlining)] public static float Abs(this float f) => Mathf.Abs(f);
		
		/**Both numbers should be positive. Returns difference / average.*/
		public static float RelativeDifference(float a, float b) => 2 * (b - a) / (a + b);
		
		/**Returns +1 if true, -1 if false.*/
		public static int Positive(bool positive) => positive ? 1 : -1;

		/**Returns null if the list is empty, or a random entry otherwise.*/
		public static T? NextInOpt<T>(this Random rand, IList<T> list) where T : class => list.Count == 0 ? null : list[rand.Next(list.Count)];

		public static uint NextUint(this Random rand) {
			var bytes = new byte[4];
			rand.NextBytes(bytes);
			return BitConverter.ToUInt32(bytes, 0);
		}

		/**The distribution isn't quite uniform, but it should be close enough.*/
		public static uint NextUint(this Random rand, uint min, uint max = UInt32.MaxValue) => min + rand.NextUint() % (max - min);

		public static float NegateIf(this float f, bool negate) => negate ? -f : f;
		
		
		
		// FORMATTING

		public static string ToHexStr(this byte i) => i.ToString("X");
		/**Padded to the max possible length.*/
		public static string ToHexPad(this byte i) => i.ToString("X2");

		public static string ToHexStr(this ushort i) => i.ToString("X");
		/**Padded to the max possible length.*/
		public static string ToHexPad(this ushort i) => i.ToString("X4");
		
		public static string ToHexStr(this uint i) => i.ToString("X");
		/**Padded to the max possible length.*/
		public static string ToHexPad(this uint i) => i.ToString("X8");
		
		public static string ToHexStr(this int i) => i.ToString("X");
		/**Padded to the max possible length.*/
		public static string ToHexPad(this int i) => i.ToString("X8");
		
		public static string ToHexStr(this ulong i) => i.ToString("X");
		/**Padded to the max possible length.*/
		public static string ToHexPad(this ulong i) => i.ToString("X16");

		/**Calculates the hash code and formats it as hexadecimal.*/
		public static string HexHash(this object o) => o.GetHashCode().ToHexStr();
		
		/**Returns "null" or "non-null".*/
		public static string IsNullStr(object? obj) => obj == null ? "null" : "non-null";

		public static string FmtPercent(float amount) => Mathf.RoundToInt(amount * 100).ToString() + '%';
		/**Formats a percent that always contains at least as many characters as 100% would.*/
		public static string FmtPercentWide(float amount) => Mathf.RoundToInt(amount * 100).ToString().PadLeft(3) + '%';
	}
}
