﻿using System;
using System.Collections.Generic;

namespace imp.util {

	public class ThreadLocalRandom {
		private static readonly Random _global = new Random();
		[ThreadStatic] private static Random? _local;

		public static Random Current {
			get {
				var ret = _local;
				if (ret != null)
					return ret;
				lock (_global) {
					ret = _local;
					if (ret != null)
						return ret;
					return _local = new Random(_global.Next());
				}
			}
		}
	}


	public static class RandomUtil {
		public static bool NextBool(this Random rand) => rand.Next(2) == 1;
		/**Picks a random entry from a non-empty array.*/
		public static ref T NextIn<T>(this Random rand, T[] array) => ref array[rand.Next(array.Length)];
		/**Returns a random index in a non-empty list.*/
		public static int NextIdx<T>(this Random rand, List<T> list) => rand.Next(list.Count);
		/**Finds (but does not remove) a random item from a non-empty list.*/
		public static T NextIn<T>(this Random rand, List<T> list) => list[rand.Next(list.Count)];

		/**Returns true with the provided probability (0-1).*/
		public static bool Chance(this Random rand, double chance) => rand.NextDouble(1.0f) < chance;

		public static float NextFloat(this Random rand, float min, float max) => (float) (min + rand.NextDouble() * (max - min));
		public static double NextDouble(this Random rand, double min, double max) => min + rand.NextDouble() * (max - min);
		public static float NextFloat(this Random rand, float max) => rand.NextFloat(0, max);
		public static double NextDouble(this Random rand, double max) => rand.NextDouble(0, max);
	}

}